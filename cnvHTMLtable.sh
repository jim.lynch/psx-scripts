##
##   cnvHTMLtable.sh
##
##   Will convert a CSV file to an enumerations Python dictionary used by cmpPSXprofiles.py
##   
##   Requires the Table Capture extension for Chrome.
##
ARGS=3
if [ $# -ne "$ARGS" ]
then
  echo Usage: $0 input-file output-file dict-name
  echo "      input-file is HTTP CSV"
  echo "      output-file is where the Python dictionary is written"
  echo "      dict-name is the dictionary name"
  exit 0
fi

INPUT_FILE=$1
OUTPUT_FILE=$2
DICT_NAME=$3

echo '    '${DICT_NAME}' = [' > $OUTPUT_FILE
cat $INPUT_FILE | sed '1,1d' | sed s/^/"        { 'mask': /"| sed "s/\t/, 'alias': '/g"|sed s/$/"' },"/g  >> $OUTPUT_FILE
echo '    ]' >> $OUTPUT_FILE

