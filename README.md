# psx-scripts




## Description
Script will compare two PSX profiles.  Profiles contain entities. The script compares each enum within the entity. If the 'diffs' option is specified, then for each enum that is different between the two entities, the alias and value will be logged.  Alternatively, without the 'diffs' option, the entity value will be logged for all entities along with a message indicating whether they contained the same value.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Script uses Python3 and requires the requests package which is not typically installed with Python3.  To install it, execute the following:

     pip3 install requests

## Usage

Here is the usage which is printed when no arguments are provided.

usage: cmpPSXprofiles.py [-h] [-d] [-diffs] [-prt] [-profs PROFILES] [-tgs TGS] [-gws GATEWAYS] -u PSXUSER -p PSXPASSWORD [-v PSXVERSION] -psx PSX -ip IP -t
                         {erp,feat,ipsp,rc,sig,tg}

Compare two PSX profiles

options:
  -h, --help            show this help message and exit

Required arguments:
  -u PSXUSER            PSX CLI user
  -p PSXPASSWORD        PSX CLI password
  -v PSXVERSION         PSX version
  -psx PSX              EMS server name
  -ip IP                EMS IP
  -t {erp,feat,ipsp,rc,sig,tg}
                        Profile type to retrieve)

Optional arguments:
  -d                    Enable debug logging (optional)
  -diffs                Only print the differences (optional)
  -prt                  Print the profiles. Overrides comparing two profiles.
  -profs PROFILES       The 2 profiles, comma separated, to be compared
  -tgs TGS              The 2 TGs (GW:TG), comma separated, for comparing TGs
  -gws GATEWAYS         The 2 gateways, comma separated, for comparing TGs

            Example to compare 2 IPSP profiles AUTO_CARRIER_IPSP and AUTO_CARRIER_IPSP_302:
                 ./cmpPSXprofiles.py -u psx-cli-user -p psx-cli-password -psx SCLC004PSX03 -t ipsp -ip 10.7.17.101 -profs AUTO_CARRIER_IPSP,AUTO_CARRIER_IPSP_302
            Example to compare 2 signaling profiles AUTO_SIG_PROF_PROXY and FIVN_SIG_PROF_PROXY:
                 ./cmpPSXprofiles.py -u psx-cli-user -p psx-cli-password -psx SCLC004PSX03 -t sig -ip 10.7.17.101 -profs AUTO_SIG_PROF_PROXY,FIVN_SIG_PROF_PROXY
            Example to print 2 signaling profiles AUTO_SIG_PROF_PROXY and FIVN_SIG_PROF_PROXY:
                 ./cmpPSXprofiles.py -u psx-cli-user -p psx-cli-password -psx SCLC004PSX03 -t sig -ip 10.7.17.101 -prt -profs AUTO_SIG_PROF_PROXY,FIVN_SIG_PROF_PROXY
            Example to compare 2 trunk group profiles AUTO_SIG_PROF_PROXY and FIVN_SIG_PROF_PROXY:
                 ./cmpPSXprofiles.py -u psx-cli-user -p psx-cli-passwor -psx SCLC004PSX03 -tgs SCLC007SBC11:AGENT_ACCESS,SCLC007SBC11:VERINT_CORE -t tg -ip 10.7.17.101  

Example on how to print each field and log whether they contain the same or different values.

./cmpPSXprofiles.py -u psx-cli-user -p psx-cli-password  -psx SCLC004PSX03 -t ipsp -ip 10.7.17.101 -profs  AUTO_PROXY_SIP302_US,PEER_SIP_IPSP01

  1. ipSigAttributes1              : AUTO_PROXY_SIP302_US:0x8000069, PEER_SIP_IPSP01:0x60 -----> Different
  2. ipSigAttributes2              : AUTO_PROXY_SIP302_US:0x1001002, PEER_SIP_IPSP01:0x1001002 -----> Same
  3. sipSignalingType              : AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x0 -----> Same
  4. sipSignalingTreatment         : AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x0 -----> Same
  5. sipHeaderPrivacyInfo          : AUTO_PROXY_SIP302_US:0x1, PEER_SIP_IPSP01:0x0 -----> Different
  6. sipSignalingTransportType     : AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x0 -----> Same
  7. sipOriginatingTg              : AUTO_PROXY_SIP302_US:0x1, PEER_SIP_IPSP01:0x0 -----> Different
  8. sipDestinationTg              : AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x0 -----> Same
  9. sipDcsChargeInfo              : AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x0 -----> Same
 10. ipSigAttributes3              : AUTO_PROXY_SIP302_US:0x1002500, PEER_SIP_IPSP01:0x2400 -----> Different
 11. ipSigAttributes4              : AUTO_PROXY_SIP302_US:0x4, PEER_SIP_IPSP01:0x0 -----> Different
 12. sipCallForwardingMapping      : AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x0 -----> Same
 13. sipToHeaderMapping            : AUTO_PROXY_SIP302_US:0x3, PEER_SIP_IPSP01:0x1 -----> Different
 14. mirroredTransparencies1       : AUTO_PROXY_SIP302_US:0x100, PEER_SIP_IPSP01:0x0 -----> Different
 15. mirroredTransparencies2       : AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x0 -----> Same
 16. nonMirroredTransparencies1    : AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x4 -----> Different
 17. ipSigAttributes5              : AUTO_PROXY_SIP302_US:0x8100, PEER_SIP_IPSP01:0x80 -----> Different
 18. ipSigAttributes6              : AUTO_PROXY_SIP302_US:0x4100401, PEER_SIP_IPSP01:0x0 -----> Different
 19. sipNpdiOption                 : AUTO_PROXY_SIP302_US:0x2, PEER_SIP_IPSP01:0x2 -----> Same
 20. sessionExpiresRefresherParm   : AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x0 -----> Same
 21. sipRelayFlags1                : AUTO_PROXY_SIP302_US:0x10010, PEER_SIP_IPSP01:0x0 -----> Different
 22. handleIpNotInNst              : AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x0 -----> Same
 23. ingHandleIpNotInNst           : AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x0 -----> Same
 24. sipSignalingTransportType2    : AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x0 -----> Same
 25. sipSignalingTransportType3    : AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x0 -----> Same
 26. sipSignalingTransportType4    : AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x0 -----> Same
 27. ipSigAttributes7              : AUTO_PROXY_SIP302_US:0x18021, PEER_SIP_IPSP01:0x20 -----> Different
 28. ipSigAttributes8              : AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x0 -----> Same
 29. sipOptionTagInSupported       : AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x0 -----> Same
 30. sipOptionTagInRequire         : AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x0 -----> Same
 31. action400RespWith417          : AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x0 -----> Same
 32. sipHeaderPChargeInfo          : AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x0 -----> Same
 33. ipSigAttributes9              : AUTO_PROXY_SIP302_US:0x1, PEER_SIP_IPSP01:0x1 -----> Same
 34. ipSigAttributes10             : AUTO_PROXY_SIP302_US:0x1400, PEER_SIP_IPSP01:0x1400 -----> Same
 35. ipSigAttributes11             : AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x0 -----> Same
 36. ipSigAttributes12             : AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x0 -----> Same
 37. ipSigAttributes13             : AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x0 -----> Same
 38. ipSigAttributes14             : AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x0 -----> Same
 39. sipVariantType                : AUTO_PROXY_SIP302_US:0x1, PEER_SIP_IPSP01:0x1 -----> Same

Now, to print which bits are different between pair of entities that are different, use the '-diffs' option.

./cmpPSXprofiles.py -u psx-cli-user -p psx-cli-password  -psx SCLC004PSX03 -t ipsp -ip 10.7.17.101 -profs  AUTO_PROXY_SIP302_US,PEER_SIP_IPSP01 -diffs

  1. ipSigAttributes1: AUTO_PROXY_SIP302_US:0x8000069, PEER_SIP_IPSP01:0x60
                                     Always Send SDP In Final 200 OK (Mask=00000001) : AUTO_PROXY_SIP302_US: setting: ON, PEER_SIP_IPSP01: setting: OFF
                    Only Selected Codec In Session Refresh (Ingress) (Mask=00000008) : AUTO_PROXY_SIP302_US: setting: ON, PEER_SIP_IPSP01: setting: OFF
                                          Send SDP In Subsequent 18x (Mask=08000000) : AUTO_PROXY_SIP302_US: setting: ON, PEER_SIP_IPSP01: setting: OFF
  2. sipHeaderPrivacyInfo: AUTO_PROXY_SIP302_US:0x1, PEER_SIP_IPSP01:0x0
                                                       P-Asserted ID (Mask=00000001) : AUTO_PROXY_SIP302_US: setting: ON, PEER_SIP_IPSP01: setting: OFF
  3. sipOriginatingTg: AUTO_PROXY_SIP302_US:0x1, PEER_SIP_IPSP01:0x0
                                                         Include OTG (Mask=00000001) : AUTO_PROXY_SIP302_US: setting: ON, PEER_SIP_IPSP01: setting: OFF
                                       Include Tgrp With Domain Name (Mask=00000003) : AUTO_PROXY_SIP302_US: setting: ON, PEER_SIP_IPSP01: setting: OFF
  4. ipSigAttributes3: AUTO_PROXY_SIP302_US:0x1002500, PEER_SIP_IPSP01:0x2400
                              Only Selected Codec In Session Refresh (Mask=00000100) : AUTO_PROXY_SIP302_US: setting: ON, PEER_SIP_IPSP01: setting: OFF
                                      Disable Reason Header (Egress) (Mask=01000000) : AUTO_PROXY_SIP302_US: setting: ON, PEER_SIP_IPSP01: setting: OFF
  5. ipSigAttributes4: AUTO_PROXY_SIP302_US:0x4, PEER_SIP_IPSP01:0x0
                                     Disable Reason Header (Ingress) (Mask=00000004) : AUTO_PROXY_SIP302_US: setting: ON, PEER_SIP_IPSP01: setting: OFF
  6. sipToHeaderMapping: AUTO_PROXY_SIP302_US:0x3, PEER_SIP_IPSP01:0x1
                                             Generic Number (Dialed) (Mask=00000002) : AUTO_PROXY_SIP302_US: setting: ON, PEER_SIP_IPSP01: setting: OFF
                                                                None (Mask=00000003) : AUTO_PROXY_SIP302_US: setting: ON, PEER_SIP_IPSP01: setting: ON
  7. mirroredTransparencies1: AUTO_PROXY_SIP302_US:0x100, PEER_SIP_IPSP01:0x0
                                                      Unknown Header (Mask=00000100) : AUTO_PROXY_SIP302_US: setting: ON, PEER_SIP_IPSP01: setting: OFF
  8. nonMirroredTransparencies1: AUTO_PROXY_SIP302_US:0x0, PEER_SIP_IPSP01:0x4
                                        Privacy Headers Transparency (Mask=00000004) : AUTO_PROXY_SIP302_US: setting: OFF, PEER_SIP_IPSP01: setting: ON
  9. ipSigAttributes5: AUTO_PROXY_SIP302_US:0x8100, PEER_SIP_IPSP01:0x80
                                                          Disable rn (Mask=00000080) : AUTO_PROXY_SIP302_US: setting: OFF, PEER_SIP_IPSP01: setting: ON
                                    Disable Media Lock Down (Egress) (Mask=00000100) : AUTO_PROXY_SIP302_US: setting: ON, PEER_SIP_IPSP01: setting: OFF
                   Include Transport Type In Contact Header (Egress) (Mask=00008000) : AUTO_PROXY_SIP302_US: setting: ON, PEER_SIP_IPSP01: setting: OFF
 10. ipSigAttributes6: AUTO_PROXY_SIP302_US:0x4100401, PEER_SIP_IPSP01:0x0
                                   Disable Media Lock Down (Ingress) (Mask=00000001) : AUTO_PROXY_SIP302_US: setting: ON, PEER_SIP_IPSP01: setting: OFF
                  Include Transport Type In Contact Header (Ingress) (Mask=00000400) : AUTO_PROXY_SIP302_US: setting: ON, PEER_SIP_IPSP01: setting: OFF
                    Include SS Attribute In Initial INVITE (Ingress) (Mask=00100000) : AUTO_PROXY_SIP302_US: setting: ON, PEER_SIP_IPSP01: setting: OFF
                               Use IP Signaling Peer Domain In R-URI (Mask=04000000) : AUTO_PROXY_SIP302_US: setting: ON, PEER_SIP_IPSP01: setting: OFF
 11. sipRelayFlags1: AUTO_PROXY_SIP302_US:0x10010, PEER_SIP_IPSP01:0x0
                                                     Status Code 3XX (Mask=00000010) : AUTO_PROXY_SIP302_US: setting: ON, PEER_SIP_IPSP01: setting: OFF
                       Reject the REFER request if no match is found (Mask=00010000) : AUTO_PROXY_SIP302_US: setting: ON, PEER_SIP_IPSP01: setting: OFF
 12. ipSigAttributes7: AUTO_PROXY_SIP302_US:0x18021, PEER_SIP_IPSP01:0x20
                     Include SS Attribute In Initial INVITE (Egress) (Mask=00000001) : AUTO_PROXY_SIP302_US: setting: ON, PEER_SIP_IPSP01: setting: OFF
                                      Send RTCP Port In SDP (Egress) (Mask=00008000) : AUTO_PROXY_SIP302_US: setting: ON, PEER_SIP_IPSP01: setting: OFF
                                     Send RTCP Port In SDP (Ingress) (Mask=00010000) : AUTO_PROXY_SIP302_US: setting: ON, PEER_SIP_IPSP01: setting: OFF 

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
