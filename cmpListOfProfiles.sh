#!/bin/bash

##########################################################
#
#  Compare a PSX profile with a list of profiles. 
#
##########################################################

ARGS=3
if [ $# -ne "$ARGS" ]
then
  echo "Usage: $0 Source-profile File-of-profile-names profile-type"
  echo "    Source-profile is to be compared against all profiles in the File-of-profile-names"
  echo "    File-of-profile-names contains one profile name per line"
  echo "    profile-type is one of the following:  erp, feat, ipsp, psp, rc, sig, tg"
  echo
  echo "  Example:  ./cmpListOfProfiles.sh AGENT_WEBRTC IPSP-profiles.txt ipsp"
  echo "    to compare the IPSP profile AGENT_WEBRTC with the list of profiles in IPSP-profiles.txt"
  exit 0
fi

SOURCE_PROFILE=$1
FILE_OF_PROFILE_NAMES=$2
PROFILE_TYPE=$3

##  Verify that file exists
if [ -f FILE_OF_PROFILE_NAME ]; then
  echo File $FILE_OF_PROFILE_NAMES does not exist
  exit 0
fi

##  Read the list of profiles 

Profiles=($(cat $FILE_OF_PROFILE_NAMES))

##  Now compare 
for p in ${Profiles[@]}
do
   echo -e "\nComparing $1 and $p\n"
#   ./cmpPSXprofiles.py -u jlynch -p G0lfing@2024  -psx SCLC004PSX03 -t $PROFILE_TYPE -ip 10.7.17.101  -profs $SOURCE_PROFILE,$p | grep Diff
   ./cmpPSXprofiles.py -u jlynch -p G0lfing@2024  -psx SCLC004PSX03 -t $PROFILE_TYPE -ip 10.7.17.101  -profs $SOURCE_PROFILE,$p
   echo status is $?
done
