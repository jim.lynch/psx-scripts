#!/usr/bin/env python3

#########################################################################################
#
#   PSXprofileTool.py
#
#   Compare various PSX profiles.  Use SOAP requests to retrieve the profiles
#   to be compared.
#
#########################################################################################

# cat ipSigAttributes2.csv| sed s/^/"    { 'mask': "/g | sed s/,/", 'alias': \""/g | sed 's/\r$/" },/g' | sed 's/[^[:print:]]//'

import argparse
import requests
import json
import subprocess
import urllib3
import base64
import syslog
import os
import sys
import xmltodict
import copy
import inspect

from pathlib import Path

import pdb 

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

pgmName = Path(sys.argv[0]).stem

class PsxConfig:
    user = ""
    password = ""
    name = ""
    version = ""
    IP = ""

    #############################################
    #
    #  Constructor
    #
    #############################################
    def __init__(self, user, password, name, version, IP):
        self.user = user
        self.password = password
        self.name = name
        self.version = version
        self.IP = IP

    ##  Helper (get/set) methods

    def setUser(self, user):
        self.user = user

    def getUser(self):
        return self.user

    def setPassword(self, password):
        self.password = password

    def getPassword(self):
        return self.password

    def setName(self, name):
        self.psxName = name

    def getName(self):
        return self.name

    def setVersion(self, version):
        self.version = version

    def getVersion(self):
        return self.version
    
    def setIP(self, IP):
        self.IP = IP

    def getIP(self):
        return self.IP

    def printCfg(self):
        print(', '.join("%s: %s" % item for item in vars(self).items()))        


###
###  Signaling Profile
###
class IPSPsignaling:       
    def __init__(self, profileName):
        self.profileName = profileName
        self.profileID = 'ipSignalingProfileId'
        self.keysToIgnore = ['ipSignalingProfileId']
        self.SOAPrequest = """
    <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:intf="http://www.sonusnet.com/ems/emsapi/psx/{psxVersion}/intf">
          <soapenv:Header>
          <USER soapenv:mustUnderstand="0" xsi:type="xsd:string">{user}</USER><PASSWORD soapenv:mustUnderstand="0" xsi:type="xsd:string">{password}</PASSWORD>
       </soapenv:Header>
       <soapenv:Body>
          <intf:retrieve soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
             <in0 xsi:type="xsd:string">{psxName}</in0>
             <in1 xsi:type="mod:IpSignalingProfileKey" xmlns:mod="http://www.sonusnet.com/ems/emsapi/psx/{psxVersion}/model">
             <ipSignalingProfileId xsi:type="xsd:string">{profName}</ipSignalingProfileId>
             </in1>
          </intf:retrieve>
       </soapenv:Body>
    </soapenv:Envelope>
    """              
        self.profAttributes = [
            { 'key': 'ipSignalingProfileId', 'value': '' },
            { 'key': 'ipSigAttributes1', 'value': '' },
            { 'key': 'ipSigAttributes2', 'value': '' },
            { 'key': 'sipSignalingType', 'value': '' },
            { 'key': 'sipSignalingTreatment', 'value': '' },
            { 'key': 'sipHeaderPrivacyInfo', 'value': '' },
            { 'key': 'sipSignalingTransportType', 'value': '' },
            { 'key': 'sipOriginatingTg', 'value': '' },
            { 'key': 'sipDestinationTg', 'value': '' },
            { 'key': 'sipDcsChargeInfo', 'value': '' },
            { 'key': 'ipSigAttributes3', 'value': '' },
            { 'key': 'ipSigAttributes4', 'value': '' },
            { 'key': 'sipCallForwardingMapping', 'value': '' },
            { 'key': 'sipToHeaderMapping', 'value': '' },
            { 'key': 'mirroredTransparencies1', 'value': '' },
            { 'key': 'mirroredTransparencies2', 'value': '' },
            { 'key': 'nonMirroredTransparencies1', 'value': '' },
            { 'key': 'ipSigAttributes5', 'value': '' },
            { 'key': 'ipSigAttributes6', 'value': '' },
            { 'key': 'sipNpdiOption', 'value': '' },
            { 'key': 'sessionExpiresRefresherParm', 'value': '' },
            { 'key': 'sipRelayFlags1', 'value': '' },
            { 'key': 'handleIpNotInNst', 'value': '' },
            { 'key': 'ingHandleIpNotInNst', 'value': '' },
            { 'key': 'sipSignalingTransportType2', 'value': '' },
            { 'key': 'sipSignalingTransportType3', 'value': '' },
            { 'key': 'sipSignalingTransportType4', 'value': '' },
            { 'key': 'ipSigAttributes7', 'value': '' },
            { 'key': 'ipSigAttributes8', 'value': '' },
            { 'key': 'sipOptionTagInSupported', 'value': '' },
            { 'key': 'sipOptionTagInRequire', 'value': '' },
            { 'key': 'action400RespWith417', 'value': '' },
            { 'key': 'sipHeaderPChargeInfo', 'value': '' },
            { 'key': 'ipSigAttributes9', 'value': '' },
            { 'key': 'ipSigAttributes10', 'value': '' },
            { 'key': 'ipSigAttributes11', 'value': '' },
            { 'key': 'ipSigAttributes12', 'value': '' },
            { 'key': 'ipSigAttributes13', 'value': '' },
            { 'key': 'ipSigAttributes14', 'value': '' },
            { 'key': 'sipVariantType', 'value': '' }
       ]

    def genRequest(self, psxCfg):
        theRequest = self.SOAPrequest.format(user=psxCfg.getUser(), password=psxCfg.getPassword(), psxName=psxCfg.getName(), psxVersion=psxCfg.getVersion(), profName=self.profileName)
        return theRequest

    ###  IPSP Signaling Enums

    sipSignalingType = [
        { 'mask': 0, 'alias': "ISUP" }
    ]

    sipSignalingTreatment = [
        { 'mask': 0, 'alias': "Interwork" },
        { 'mask': 1, 'alias': "Passthru" }
    ]

    sipSignalingTransportType = [
        { 'value': 0, 'alias': "None" },
        { 'mask': 1, 'alias': "TCP" },
        { 'mask': 2, 'alias': "UDP" },
        { 'mask': 4, 'alias': "SCTP" },
        { 'mask': 8, 'alias': "TLS Over TCP" }    
    ]

    mirroredTransparencies1 = [
        { 'mask': 0x00000001, 'alias': "Authcode Headers Transparency" },
        { 'mask': 0x00000002, 'alias': "Reason Header Supported Transparency" },
        { 'mask': 0x00000004, 'alias': "Alert Information Header Transparency" },
        { 'mask': 0x00000008, 'alias': "P-Charging-Vector Header" },
        { 'mask': 0x00000010, 'alias': "Route Header" },
        { 'mask': 0x00000020, 'alias': "Path Header" },
        { 'mask': 0x00000040, 'alias': "Service-Route Header" },
        { 'mask': 0x00000080, 'alias': "Maxforwards Header" },
        { 'mask': 0x00000100, 'alias': "Unknown Header" },
        { 'mask': 0x00000200, 'alias': "SIP Body" },
        { 'mask': 0x00000400, 'alias': "SIPFRAG Body" },
        { 'mask': 0x00000800, 'alias': "QSIG Body" },
        { 'mask': 0x00001000, 'alias': "MWI Body" },
        { 'mask': 0x00002000, 'alias': "Unknown Body" },
        { 'mask': 0x00004000, 'alias': "Tone Body" },
        { 'mask': 0x00008000, 'alias': "Referred-By Header" },
        { 'mask': 0x00010000, 'alias': "Error Info" },
        { 'mask': 0x00020000, 'alias': "Request URI" },
        { 'mask': 0x00040000, 'alias': "P-Called-Party-Id" },
        { 'mask': 0x00080000, 'alias': "Via Header" },
        { 'mask': 0x00100000, 'alias': "Allow Header" },
        { 'mask': 0x00200000, 'alias': "Geo Location Header" },
        { 'mask': 0x00400000, 'alias': "Geo Location Routing" },
        { 'mask': 0x00800000, 'alias': "Geo Location Error Header" }
    ]

    mirroredTransparencies2 = [
        { 'mask': 0x00000001, 'alias': "Accept-Contact Header" },
        { 'mask': 0x00000002, 'alias': "Accept-Language Header" },
        { 'mask': 0x00000004, 'alias': "Accept Header" },
        { 'mask': 0x00000008, 'alias': "Call-Info Header" },
        { 'mask': 0x00000010, 'alias': "External Body" },
        { 'mask': 0x00000020, 'alias': "Event Header" },
        { 'mask': 0x00000040, 'alias': "Image Body" },
        { 'mask': 0x00000080, 'alias': "Pidf Body" },
        { 'mask': 0x00000100, 'alias': "Pidf-Diff Body" },
        { 'mask': 0x00000200, 'alias': "Resource-Lists Body" },
        { 'mask': 0x00000400, 'alias': "RLMI Body" },
        { 'mask': 0x00000800, 'alias': "Server Header" },
        { 'mask': 0x00001000, 'alias': "Simple-Filter Body" },
        { 'mask': 0x00002000, 'alias': "User-Agent Header" },
        { 'mask': 0x00004000, 'alias': "Warning Header" },
        { 'mask': 0x00008000, 'alias': "Watcherinfo Body" },
        { 'mask': 0x00010000, 'alias': "X-APT Header Transparency" },
        { 'mask': 0x00040000, 'alias': "Resource Priority Option Tag" }    
    ]

    ipSigAttributes1 = [
        { 'mask': 0x00000001, 'alias': "Always Send SDP In Final 200 OK" },
        { 'mask': 0x00000002, 'alias': "Reason Header Precedence (Ingress)" },
        { 'mask': 0x00000004, 'alias': "Session Timer Refresh Update (Ingress)" },
        { 'mask': 0x00000008, 'alias': "Only Selected Codec In Session Refresh (Ingress)" },
        { 'mask': 0x00000010, 'alias': "SDP O-line Only Compares (Ingress)" },
        { 'mask': 0x00000020, 'alias': "Send PTIME In SDP (Ingress)" },
        { 'mask': 0x00000040, 'alias': "Minimize Relaying Of Media Changes From Other Call Leg (Ingress)" },
        { 'mask': 0x00000080, 'alias': "Relay Refer" },
        { 'mask': 0x00000100, 'alias': "Relay Info" },
        { 'mask': 0x00000200, 'alias': "Relay Options" },
        { 'mask': 0x00000400, 'alias': "Relay Notify" },
        { 'mask': 0x00000800, 'alias': "Map Called Party Category In P-Sig-Info Header" },
        { 'mask': 0x00001000, 'alias': "Set Accept Header To Application SDP Only (Ingress)" },
        { 'mask': 0x00002000, 'alias': "Set Sline Dash (Ingress)" },
        { 'mask': 0x00004000, 'alias': "Set Oline Dash (Ingress)" },
        { 'mask': 0x00008000, 'alias': "Customized Session Timer Behavior (Ingress)" },
        { 'mask': 0x00010000, 'alias': "Reason Header Supported (Ingress)" },
        { 'mask': 0x00020000, 'alias': "Reject REFER (Ingress)" },
        { 'mask': 0x00040000, 'alias': "Skip PSX Query For REFER" },
        { 'mask': 0x00080000, 'alias': "181 Not Supported" },
        { 'mask': 0x00100000, 'alias': "182 Not Supported" },
        { 'mask': 0x00200000, 'alias': "No SDP In 180" },
        { 'mask': 0x00400000, 'alias': "Convert Progress To Alert Supported" },
        { 'mask': 0x00800000, 'alias': "Send Fast Start Response Supported" },
        { 'mask': 0x01000000, 'alias': "Don't Send Facility Message Supported" },
        { 'mask': 0x04000000, 'alias': "Send Empty TCS (Ingress)" },
        { 'mask': 0x08000000, 'alias': "Send SDP In Subsequent 18x" }    
    ]

    ipSigAttributes2 = [
        { 'mask': 0x00000001, 'alias': "TTC-ISUP Mapping" },
        { 'mask': 0x00000002, 'alias': "SIP Signaling Disposition Handling" },
        { 'mask': 0x00000004, 'alias': "SIP Signaling BCI Interwork" },
        { 'mask': 0x00000008, 'alias': "SIP Signaling BCI ISDN Access" },
        { 'mask': 0x00000010, 'alias': "SIP Header Extension Trusted" },
        { 'mask': 0x00000020, 'alias': "SIP Header Extension PSTN Param" },
        { 'mask': 0x00000040, 'alias': "SIP Header Extension CIC" },
        { 'mask': 0x00000080, 'alias': "SIP Header Extension NPI" },
        { 'mask': 0x00000100, 'alias': "Do Not Include Tel URI in PAI Header" },
        { 'mask': 0x00000200, 'alias': "SIP Header Extension OLIP" },
        { 'mask': 0x00000400, 'alias': "SIP Header Extension TSWID TTGID" },
        { 'mask': 0x00000800, 'alias': "SIP Signaling Skip Redirect CB Profile" },
        { 'mask': 0x00001000, 'alias': "IP Signaling BCI Echo Control" },
        { 'mask': 0x00002000, 'alias': "SIP Signaling Precondition" },
        { 'mask': 0x00004000, 'alias': "SIP Header Extension CPC Info" },
        { 'mask': 0x00020000, 'alias': "SIP Header Extension Privacy Req" },
        { 'mask': 0x00040000, 'alias': "SIP Signaling Use OGI" },
        { 'mask': 0x00080000, 'alias': "Insert In Band Indication" },
        { 'mask': 0x00100000, 'alias': "Globalize Numbers" },
        { 'mask': 0x00200000, 'alias': "Reject REFER (Egress)" },
        { 'mask': 0x00400000, 'alias': "Skip PSX Query For REFER" },
        { 'mask': 0x00800000, 'alias': "Reason Header Supported (Egress)" },
        { 'mask': 0x01000000, 'alias': "Disable 2806 Compliance" },
        { 'mask': 0x02000000, 'alias': "Disable 3xx Progress" },
        { 'mask': 0x04000000, 'alias': "Wait Till Connect Before Abandon FastStart" },
        { 'mask': 0x08000000, 'alias': "Reason Header Precedence (Egress)" }
    ]

    ipSigAttributes3 = [
        { 'mask': 0x00000001, 'alias': "Add/Modify ETS Resource Priority Header" },
        { 'mask': 0x00000002, 'alias': "Send Empty TCS (Egress)" },
        { 'mask': 0x00000004, 'alias': "Use Terminating Domain Name" },
        { 'mask': 0x00000008, 'alias': "Preserve Ingress R-URI Domain Name" },
        { 'mask': 0x00000010, 'alias': "Preserve Ingress FROM Domain Name" },
        { 'mask': 0x00000020, 'alias': "Session Timer Refresh Update" },
        { 'mask': 0x00000040, 'alias': "Use Colon In SDP Media Type Parameter" },
        { 'mask': 0x00000080, 'alias': "Include Called Party ISUB" },
        { 'mask': 0x00000100, 'alias': "Only Selected Codec In Session Refresh" },
        { 'mask': 0x00000200, 'alias': "SDP O-line Only Compares" },
        { 'mask': 0x00000400, 'alias': "Send PTIME In SDP (Egress)" },
        { 'mask': 0x00000800, 'alias': "Validate ISUB address" },
        { 'mask': 0x00001000, 'alias': "Map Contractor Number In P-Sig-Info Header" },
        { 'mask': 0x00002000, 'alias': "Minimize Relaying Of Media Changes From Other Call Leg" },
        { 'mask': 0x00004000, 'alias': "No Userinfo In Contact Header (Egress)" },
        { 'mask': 0x00008000, 'alias': "Relay REFER (Egress)" },
        { 'mask': 0x00010000, 'alias': "Relay INFO (Egress)" },
        { 'mask': 0x00020000, 'alias': "Relay OPTIONS (Egress)" },
        { 'mask': 0x00040000, 'alias': "Relay NOTIFY (Egress)" },
        { 'mask': 0x00080000, 'alias': "Set Accept Header To Application SDP Only (Egress)" },
        { 'mask': 0x00100000, 'alias': "Set Sline Dash (Egress)" },
        { 'mask': 0x00200000, 'alias': "Set Oline Dash (Egress)" },
        { 'mask': 0x00400000, 'alias': "Customized Session Timer Behavior (Egress)" },
        { 'mask': 0x00800000, 'alias': "No Content Disposition (Egress)" },
        { 'mask': 0x01000000, 'alias': "Disable Reason Header (Egress)" },
        { 'mask': 0x02000000, 'alias': "Disable Also Header (Egress)" },
        { 'mask': 0x04000000, 'alias': "Do Not Include SS Attribute In re-INVITE (Egress)" },
        { 'mask': 0x08000000, 'alias': "No Port Number 5060 (Egress)" }
    ]

    ipSigAttributes4 = [
        { 'mask': 0x00000001, 'alias': "No Userinfo In Contact Header (Ingress)" },
        { 'mask': 0x00000002, 'alias': "No Content Disposition (Ingress)" },
        { 'mask': 0x00000004, 'alias': "Disable Reason Header (Ingress)" },
        { 'mask': 0x00000008, 'alias': "Disable Also Header (Ingress)" },
        { 'mask': 0x00000010, 'alias': "Do Not Include SS Attribute in re-INVITE (Ingress)" },
        { 'mask': 0x00000020, 'alias': "No Port Number 5060 (Ingress)" },
        { 'mask': 0x00000040, 'alias': "Map UUI In P-Sig-Info Header (Ingress)" },
        { 'mask': 0x00000080, 'alias': "Clearmode For Data Calls (Ingress)" },
        { 'mask': 0x00000100, 'alias': "Registration Support 3xx" },
        { 'mask': 0x00000200, 'alias': "Store Path Header (Ingress)" },
        { 'mask': 0x00000400, 'alias': "Create Path Header (Ingress)" },
        { 'mask': 0x00000800, 'alias': "Store Service-Route Header (Ingress)" },
        { 'mask': 0x00001000, 'alias': "Create Service-Route Header (Ingress)" },
        { 'mask': 0x00002000, 'alias': "Include IP Ports In FROM And TO Headers (Ingress)" },
        { 'mask': 0x00004000, 'alias': "Generate Terminating CA" },
        { 'mask': 0x00008000, 'alias': "Generate Terminating CIC" },
        { 'mask': 0x00010000, 'alias': "Map UUI In P-Sig-Info Header (Egress)" },
        { 'mask': 0x00020000, 'alias': "Include Calling Party ISUB" },
        { 'mask': 0x00040000, 'alias': "Allow NSAP ISUB" },
        { 'mask': 0x00080000, 'alias': "Allow User Specified ISUB" },
        { 'mask': 0x00100000, 'alias': "Include Qvalue" },
        { 'mask': 0x00200000, 'alias': "Create P-Charging-Vector (Egress)" },
        { 'mask': 0x00400000, 'alias': "Clearmode For Data Calls (Egress)" },
        { 'mask': 0x00800000, 'alias': "Store Path Header (Egress)" },
        { 'mask': 0x01000000, 'alias': "Create Path Header (Egress)" },
        { 'mask': 0x02000000, 'alias': "Store Service-Route Header (Egress)" },
        { 'mask': 0x04000000, 'alias': "Create Service-Route Header (Egress)" }
    ]

    ipSigAttributes5 = [
        { 'mask': 0x00000001, 'alias': "Treat 1xx Message Except 100 As 183" },
        { 'mask': 0x00000002, 'alias': "Include P-K-Adn" },
        { 'mask': 0x00000004, 'alias': "Include IP Ports In FROM And TO Headers (Egress)" },
        { 'mask': 0x00000008, 'alias': "Use Lower Case Domain Names" },
        { 'mask': 0x00000010, 'alias': "Use Terminating CA From SIP" },
        { 'mask': 0x00000020, 'alias': "Use Terminating CIC From SIP" },
        { 'mask': 0x00000040, 'alias': "Disconnect If Neither Terminating CA Nor CIC Received" },
        { 'mask': 0x00000080, 'alias': "Disable rn" },
        { 'mask': 0x00000100, 'alias': "Disable Media Lock Down (Egress)" },
        { 'mask': 0x00000200, 'alias': "Map Suspend/Resume Event In P-Svc-Info Header (Egress)" },
        { 'mask': 0x00000400, 'alias': "Convert Inactive To Sendrecv" },
        { 'mask': 0x00000800, 'alias': "Force Re-Route Via PSX Query (Egress)" },
        { 'mask': 0x00001000, 'alias': "Send All Allowed Codecs For Late Media Invite Or re-INVITE (Egress)" },
        { 'mask': 0x00002000, 'alias': "Send Direct Media Info In SDP Attribute (Egress)" },
        { 'mask': 0x00004000, 'alias': "Accept Any Cseq In Bye Message (Egress)" },
        { 'mask': 0x00008000, 'alias': "Include Transport Type In Contact Header (Egress)" },
        { 'mask': 0x00010000, 'alias': "Set Session Version Zero (Egress)" },
        { 'mask': 0x00020000, 'alias': "Disable Refer-To URI Parameters (Egress)" },
        { 'mask': 0x00040000, 'alias': "Calling Party Type Number If Present" },
        { 'mask': 0x00080000, 'alias': "BGCF Target Scheme Transparency" },
        { 'mask': 0x00100000, 'alias': "Use Called Party In Request URI" },
        { 'mask': 0x00200000, 'alias': "Disable Host Translation (Egress)" },
        { 'mask': 0x00400000, 'alias': "Same CallId For Required Authorization" },
        { 'mask': 0x00800000, 'alias': "P-ChgMsg-Info (Egress)" },
        { 'mask': 0x01000000, 'alias': "Map Cause Location (Egress)" },
        { 'mask': 0x02000000, 'alias': "Insert Peer Address As Top Route Header (Egress)" },
        { 'mask': 0x04000000, 'alias': "Call Hold Interworking (Egress)" },
        { 'mask': 0x08000000, 'alias': "Terminal Portability Interworking (Egress)" }
    ]

    ipSigAttributes6 = [
        { 'mask': 0x00000001, 'alias': "Disable Media Lock Down (Ingress)" },
        { 'mask': 0x00000002, 'alias': "Map Suspend/Resume Event In P-Svc-Info Header (Ingress)" },
        { 'mask': 0x00000004, 'alias': "Force Re-Route Via PSX Query (Ingress)" },
        { 'mask': 0x00000008, 'alias': "Send All Allowed Codecs For Late Media Invite Or re-INVITE (Ingress)" },
        { 'mask': 0x00000010, 'alias': "Send Direct Media Info In SDP Attribute (Ingress)" },
        { 'mask': 0x00000020, 'alias': "Accept Any Cseq In Bye Message (Ingress)" },
        { 'mask': 0x00000040, 'alias': "Set Session Version Zero (Ingress)" },
        { 'mask': 0x00000080, 'alias': "Send 183 On Initiating Disconnect Treatment" },
        { 'mask': 0x00000100, 'alias': "Disable Refer-To URI Parameters (Ingress)" },
        { 'mask': 0x00000200, 'alias': "Create P-Charging-Vector (Ingress)" },
        { 'mask': 0x00000400, 'alias': "Include Transport Type In Contact Header (Ingress)" },
        { 'mask': 0x00000800, 'alias': "Send RTCP Bandwidth Info in SDP" },
        { 'mask': 0x00001000, 'alias': "Disable Host Translation (Ingress)" },
        { 'mask': 0x00002000, 'alias': "Map Cause Location (Ingress)" },
        { 'mask': 0x00004000, 'alias': "P-ChgMsg-Info (Ingress)" },
        { 'mask': 0x00008000, 'alias': "Insert Peer Address As Top Route Header (Ingress)" },
        { 'mask': 0x00010000, 'alias': "Call Hold Interworking (Ingress)" },
        { 'mask': 0x00020000, 'alias': "Terminal Portability Interworking (Ingress)" },
        { 'mask': 0x00040000, 'alias': "Suppress UNREGISTER" },
        { 'mask': 0x00080000, 'alias': "Skip CSeq Check In Early Dialog" },
        { 'mask': 0x00100000, 'alias': "Include SS Attribute In Initial INVITE (Ingress)" },
        { 'mask': 0x00200000, 'alias': "End To End BYE (Ingress)" },
        { 'mask': 0x00400000, 'alias': "Add Path/Service Route Per TG (Ingress)" },
        { 'mask': 0x00800000, 'alias': "ReQuery PSX On REGISTER Refresh" },
        { 'mask': 0x01000000, 'alias': "Include G729 with G729A when offer PSP has G729A (Ingress)" },
        { 'mask': 0x02000000, 'alias': "Registration Expires in Expires Header" },
        { 'mask': 0x04000000, 'alias': "Use IP Signaling Peer Domain In R-URI" },
        { 'mask': 0x08000000, 'alias': "Replace Host On Via Header" }
    ]

    ipSigAttributes7 = [
        { 'mask': 0x00000001, 'alias': "Include SS Attribute In Initial INVITE (Egress)" },
        { 'mask': 0x00000002, 'alias': "End To End BYE (Egress)" },
        { 'mask': 0x00000004, 'alias': "Add Path/Service Route Per TG (Egress)" },
        { 'mask': 0x00000008, 'alias': "ReQuery PSX On REGISTER Refresh" },
        { 'mask': 0x00000010, 'alias': "Include G729 with G729A when offer PSP has G729A (Egress)" },
        { 'mask': 0x00000020, 'alias': "Transparency For Destination Trunk Group Parameter" },
        { 'mask': 0x00000040, 'alias': "H323 Delay Cut Through" },
        { 'mask': 0x00000200, 'alias': "Don't Send Fast Start Proposal" },
        { 'mask': 0x00000400, 'alias': "Refuse Fast Start Proposal" },
        { 'mask': 0x00000800, 'alias': "Disable Constrained Capacities (Egress)" },
        { 'mask': 0x00001000, 'alias': "Disable Constrained Capacities (Ingress)" },
        { 'mask': 0x00002000, 'alias': "Audio Codec Change through Empty TCS (Egress)" },
        { 'mask': 0x00004000, 'alias': "Audio Codec Change through Empty TCS (Ingress)" },
        { 'mask': 0x00008000, 'alias': "Send RTCP Port In SDP (Egress)" },
        { 'mask': 0x00010000, 'alias': "Send RTCP Port In SDP (Ingress)" },
        { 'mask': 0x00020000, 'alias': "P-Called-Party-Id-Support(Egress)" },
        { 'mask': 0x00040000, 'alias': "P-Called-Party-Id-Support (Ingress)" },
        { 'mask': 0x00080000, 'alias': "Relay Data Path Mode Changes To The Other Leg (Egress)" },
        { 'mask': 0x00100000, 'alias': "Relay Data Path Mode Changes To The Other Leg (Ingress)" },
        { 'mask': 0x00800000, 'alias': "Enable Default PUI Procedures (Egress)" },
        { 'mask': 0x01000000, 'alias': "Enable Default PUI Procedures (Ingress)" },
        { 'mask': 0x02000000, 'alias': "Enable Dial String Handling (Egress)" },
        { 'mask': 0x04000000, 'alias': "Enable Dial String Handling (Ingress)" },
        { 'mask': 0x08000000, 'alias': "Replace Host On Via Header (Ingress)" }
    ]

    ipSigAttributes8 = [
        { 'mask': 0x00000001, 'alias': "Reject Refer With IP (Egress)" },
        { 'mask': 0x00000002, 'alias': "Reject Refer With TN (Egress)" },
        { 'mask': 0x00000004, 'alias': "Don't Send Refer With IP (Egress)" },
        { 'mask': 0x00000008, 'alias': "Don't Send Refer With TN (Egress)" },
        { 'mask': 0x00000010, 'alias': "Reject 3XX With IP (Egress)" },
        { 'mask': 0x00000020, 'alias': "Reject 3XX With TN (Egress)" },
        { 'mask': 0x00000040, 'alias': "Override Relay For Non SIP Egrs Leg (Egress)" },
        { 'mask': 0x00000080, 'alias': "Reject Refer With IP (Ingress)" },
        { 'mask': 0x00000100, 'alias': "Reject Refer With TN (Ingress)" },
        { 'mask': 0x00000200, 'alias': "Don't Send Refer With IP (Ingress)" },
        { 'mask': 0x00000400, 'alias': "Don't Send Refer With TN (Ingress)" },
        { 'mask': 0x00000800, 'alias': "Don't Send 3XX With IP (Ingress)" },
        { 'mask': 0x00001000, 'alias': "Don't Send 3XX With TN (Ingress)" },
        { 'mask': 0x00002000, 'alias': "Override Relay For Non SIP Egrs Leg (Ingress)" },
        { 'mask': 0x00004000, 'alias': "Add P-Charging Func Addr (Egress)" },
        { 'mask': 0x00008000, 'alias': "Add P-Charging Func Addr (Ingress)" },
        { 'mask': 0x00010000, 'alias': "Restrict History Info Header (Egress)" },
        { 'mask': 0x00020000, 'alias': "Restrict History Info Header (Ingress)" },
        { 'mask': 0x00040000, 'alias': "Enable Hold On REFER" },
        { 'mask': 0x00080000, 'alias': "Enable Hold On REFER (Ingress)" },
        { 'mask': 0x00100000, 'alias': "Validate Access Nw Info Header (Egress)" },
        { 'mask': 0x00200000, 'alias': "Validate Access Nw Info Header (Ingress)" },
        { 'mask': 0x00400000, 'alias': "Force Re-query for Redirection" },
        { 'mask': 0x00800000, 'alias': "SIP Header Extension P-Charge-Info NOA" }
    ]

    ipSigAttributes9 = [
        { 'mask': 0x00000001, 'alias': "Do not Use Ingress Call-Id" },
        { 'mask': 0x00000002, 'alias': "Use Ingress Call-Id by adding Static string" },
        { 'mask': 0x00000004, 'alias': "Use Ingress Call-Id by Prepending" },
        { 'mask': 0x00000008, 'alias': "RESERVED" },
        { 'mask': 0x00000010, 'alias': "RESERVED" },
        { 'mask': 0x00000020, 'alias': "RESERVED" },
        { 'mask': 0x00000040, 'alias': "RESERVED" },
        { 'mask': 0x00000080, 'alias': "RESERVED" },
        { 'mask': 0x00000100, 'alias': "End To End ACK" },
        { 'mask': 0x00000200, 'alias': "Include HistoryInfo Header-Call Forwarding Data mapping" },
        { 'mask': 0x00000400, 'alias': "Map CPC if Presentation Indicator Restricted and cpc value priority" },
        { 'mask': 0x00000800, 'alias': "Map CPC if Presentation Indicator Restricted" },
        { 'mask': 0x00001000, 'alias': "Map CPC if Presentation Indicator Restricted and cpc value Any" },
        { 'mask': 0x00002000, 'alias': "Default" },
        { 'mask': 0x00004000, 'alias': "PAI" },
        { 'mask': 0x00008000, 'alias': "From" },
        { 'mask': 0x00010000, 'alias': "Both PAI and From" },
        { 'mask': 0x00020000, 'alias': "Accept 3xx with RN" },
        { 'mask': 0x00040000, 'alias': "MS Lync Privacy Support" },
        { 'mask': 0x00080000, 'alias': "Create P-Visited-Network Id (Egress)" },
        { 'mask': 0x00100000, 'alias': "Create P-Visited-Network Id (Ingress)" },
        { 'mask': 0x00200000, 'alias': "Suppress 183 for 3xx Redirect Response" },
        { 'mask': 0x00400000, 'alias': "Suppress 183 without SDP" },
        { 'mask': 0x00800000, 'alias': "Lockdown preferred codec (Ingress)" }
    ]

    ipSigAttributes10 = [
        { 'mask': 0x00000001, 'alias': "Map SGD In P-Sig-Info Header (Ingress)" },
        { 'mask': 0x00000002, 'alias': "Map SGD In P-Sig-Info Header (Egress)" },
        { 'mask': 0x00000004, 'alias': "Use DM/PM Manipulated Host Name In Request URI" },
        { 'mask': 0x00000008, 'alias': "Reason With Cause Value As Per RFC 4244 (Egress)" },
        { 'mask': 0x00000010, 'alias': "Cause Parameter In RFC 4458 (Egress)" },
        { 'mask': 0x00000020, 'alias': "Reason With Cause Value As Per RFC 4244 (Ingress)" },
        { 'mask': 0x00000040, 'alias': "Cause Parameter In RFC 4458 (Ingress)" },
        { 'mask': 0x00000080, 'alias': "History Info (Ingress)" },
        { 'mask': 0x00000100, 'alias': "Override 3xx Relay (Ingress)" },
        { 'mask': 0x00000200, 'alias': "Store P-Charging Function Addr (Ingress)" },
        { 'mask': 0x00000400, 'alias': "Store P-Charging Vector (Ingress)" },
        { 'mask': 0x00000800, 'alias': "Store P-Charging Function Addr (Egress)" },
        { 'mask': 0x00001000, 'alias': "Store P-Charging Vector (Egress)" },
        { 'mask': 0x00002000, 'alias': "Qos Based Routing" },
        { 'mask': 0x00004000, 'alias': "Call Preservation Time Out (Egress)" },
        { 'mask': 0x00010000, 'alias': "Call Preservation Time Out (Ingress)" },
        { 'mask': 0x00020000, 'alias': "Send TLS Connection Failure Response" },
        { 'mask': 0x00040000, 'alias': "Enable 3261Cancel Handling" },
        { 'mask': 0x00080000, 'alias': "Use SIP in Core" },
        { 'mask': 0x00100000, 'alias': "Restrict User Equals to Phone" },
        { 'mask': 0x00200000, 'alias': "Honor Embedded Headers in 3xx" },
        { 'mask': 0x00400000, 'alias': "End To End Relay Of re-INVITE(egress)" },
        { 'mask': 0x00800000, 'alias': "End To End Relay Of re-INVITE(Ingress)" },
        { 'mask': 0x01000000, 'alias': "End To End Relay Of Update(egress)" }
    ]

    ipSigAttributes11 = [
        { 'mask': 0x00000001, 'alias': "Route Using Received FQDN (Egress)" },
        { 'mask': 0x00000002, 'alias': "Route Using Received FQDN (Ingress)" },
        { 'mask': 0x00000004, 'alias': "Diversion History Info Header Interworking" },
        { 'mask': 0x00000008, 'alias': "Send BIT-H Of BCI In Outgoing 18x" },
        { 'mask': 0x00000010, 'alias': "Use Zone Level Domain Name In Contact (Egress)" },
        { 'mask': 0x00000020, 'alias': "Use Zone Level Domain Name In Contact (Ingress)" },
        { 'mask': 0x00000040, 'alias': "Send a valid IP when data path mode is inactive" },
        { 'mask': 0x00000080, 'alias': "Cause Parameter In RFC 4458 (Ingress)" },
        { 'mask': 0x00000100, 'alias': "Prefix RN to Dialed Digits" },
        { 'mask': 0x00000200, 'alias': "History Info (Ingress)" },
        { 'mask': 0x00000400, 'alias': "Suppress Min-SE if not received" },
        { 'mask': 0x00000800, 'alias': "Ignore SDP After Offer Answer Completed" },
        { 'mask': 0x00001000, 'alias': "ISUP MIME in SIP X-header" },
        { 'mask': 0x00002000, 'alias': "Use Psx Route for Registered Invite" },
        { 'mask': 0x00004000, 'alias': "From Header Anonymisation" },
        { 'mask': 0x00008000, 'alias': "Encrypt Path Header(Egress)" },
        { 'mask': 0x00010000, 'alias': "Encrypt ServiceRoute Header(Egress)" },
        { 'mask': 0x00020000, 'alias': "No Service Route Hdr For Emergency Registration" },
        { 'mask': 0x00040000, 'alias': "Transit PAI From Unregistered Peer" },
        { 'mask': 0x00080000, 'alias': "Map 3XX Contact URI To Route Header" },
        { 'mask': 0x00100000, 'alias': "Add Loop Back Route Header" },
        { 'mask': 0x00200000, 'alias': "Encrypt Path Header(Ingress)" },
        { 'mask': 0x00400000, 'alias': "Encrypt ServiceRoute Header(Ingress)" },
        { 'mask': 0x00800000, 'alias': "Include ENUM Parameters" },
        { 'mask': 0x01000000, 'alias': "Insert PAccees Network Info(Egress)" },
        { 'mask': 0x02000000, 'alias': "Insert PAccees Network Info(Ingress)" },
        { 'mask': 0x04000000, 'alias': "Use Zone Level Domain Name In Path Header(Egress)" },
        { 'mask': 0x08000000, 'alias': "No CDR Change In End To End Ack" }    
    ]

    ipSigAttributes12 = [
        { 'mask': 0x00000001, 'alias': "Support Reg Event(Egress)" },
        { 'mask': 0x00000002, 'alias': "Support Reg Event(Ingress)" },
        { 'mask': 0x00000004, 'alias': "Use PSX Route For SBC Initiated Subscribe(Egress)" },
        { 'mask': 0x00000008, 'alias': "Use PSX Route For SBC Initiated Subscribe(Ingress)" },
        { 'mask': 0x00000010, 'alias': "Contact Transparency For Isfocus Media Tag(Egress)" },
        { 'mask': 0x00000020, 'alias': "Contact Transparency For Isfocus Media Tag(Ingress)" },
        { 'mask': 0x00000040, 'alias': "Support S-CSCF Restoration Procedures(Egress)" },
        { 'mask': 0x00000080, 'alias': "Support S-CSCF Restoration Procedures(Ingress)" },
        { 'mask': 0x00000100, 'alias': "Insert UE Flow Info(Egress)" },
        { 'mask': 0x00000200, 'alias': "Insert UE Flow Info(Ingress)" },
        { 'mask': 0x00000400, 'alias': "Accept Alert Info(Ingress)" },
        { 'mask': 0x00000800, 'alias': "Accept Alert Info(Egress)" },
        { 'mask': 0x00002000, 'alias': "Register to Alternate on Primary Down" },
        { 'mask': 0x00004000, 'alias': "Revert to Primary on Recovery" },
        { 'mask': 0x00008000, 'alias': "Deregister Alternate On Primary Recovery" },
        { 'mask': 0x00010000, 'alias': "Override Internal Expires Timer" },
        { 'mask': 0x00020000, 'alias': "Register to Alternate on Primary Down(Ingress)" },
        { 'mask': 0x00040000, 'alias': "Revert to Primary on Recovery(Ingress)" },
        { 'mask': 0x00080000, 'alias': "Deregister Alternate On Primary Recovery(Ingress)" },
        { 'mask': 0x00100000, 'alias': "Override Internal Expires Timer(Ingress)" },
        { 'mask': 0x00001000, 'alias': "Send Updated SDP In 200OK" },
        { 'mask': 0x08000000, 'alias': "Use Configured Transport For Egress Leg" }    
    ]

    ipSigAttributes13 = [
        { 'mask': 0x00000001, 'alias': "Anonymize Host Ip Address(Egress)" },
        { 'mask': 0x00000002, 'alias': "Create ISUP Message Body" },
        { 'mask': 0x00000004, 'alias': "Create ISUP Message Body(Ingress)" },
        { 'mask': 0x00000008, 'alias': "Disable Transparently Passing ISUP Message Body" },
        { 'mask': 0x00000010, 'alias': "Disable Transparently Passing ISUP Message Body(Ingress)" },
        { 'mask': 0x00000020, 'alias': "Map Diversion Header To Charge Number" },
        { 'mask': 0x00000040, 'alias': "Map RN, OCN, RDI To Diversion Header" },
        { 'mask': 0x00000080, 'alias': "Use SIP Domain Name In PAI Header" },
        { 'mask': 0x00000100, 'alias': "Use Network Provided Screening Indicator For Calling Number" },
        { 'mask': 0x00000200, 'alias': "AIToPemInterworking" },
        { 'mask': 0x00000400, 'alias': "AIToPemInterworking (Ingress)" },
        { 'mask': 0x00000800, 'alias': "MonitorRtpPnEgressUpdate" },
        { 'mask': 0x00001000, 'alias': "SendSbcSupportedCodecForLateMediaReInvite" },
        { 'mask': 0x00002000, 'alias': "SendSbcSupportedCodecsForLateMediaReInvite(Ingress)" },
        { 'mask': 0x00004000, 'alias': "Skip Re-Route Via PSX Query(Ingress)" },
        { 'mask': 0x00008000, 'alias': "Skip Re-Route Via PSX Query(Egress)" },
        { 'mask': 0x00010000, 'alias': "Map Subsequent 180 to 183" },
        { 'mask': 0x00020000, 'alias': "Early Media Authorization" },
        { 'mask': 0x00040000, 'alias': "Convert Alert To Progress" },
        { 'mask': 0x00080000, 'alias': "Report Early Media Auth" },
        { 'mask': 0x00100000, 'alias': "Honor Subsequent SDP Answer" },
        { 'mask': 0x00200000, 'alias': "PI Allowed Send CPC In" },
        { 'mask': 0x00400000, 'alias': "Send CPC in DEFAULT" },
        { 'mask': 0x00800000, 'alias': "Send CPC in FROM" },
        { 'mask': 0x01000000, 'alias': "Send CPC in PAI" },
        { 'mask': 0x02000000, 'alias': "Send CPC in Both" },
        { 'mask': 0x04000000, 'alias': "Include SIP Reason Header" },
        { 'mask': 0x08000000, 'alias': "Include SIP Reason Header(Egress)" }    
    ]

    ipSigAttributes14 = [
        { 'mask': 0x00000001, 'alias': "Enhanced Local Redirection" },
        { 'mask': 0x00000002, 'alias': "Suppress End To End Session Refresh(egress)" },
        { 'mask': 0x00000004, 'alias': "Suppress End To End Session Refresh(ingress)" },
        { 'mask': 0x00000008, 'alias': "Process Qtype and Attach DPC/SSN info in 3xx" },
        { 'mask': 0x00000010, 'alias': "Use PSX Modified To URI Host Part" },
        { 'mask': 0x00000020, 'alias': "Do not use PSX Unmodified From URI Host Part" },
        { 'mask': 0x00000040, 'alias': "Do not use PSX Unmodified PAI URI Host Part" },
        { 'mask': 0x00000080, 'alias': "Enable Globalization of Numbers starting with Alphabet" },
        { 'mask': 0x00000100, 'alias': "Apply Transparency Profile Setting to SBC TG" },
        { 'mask': 0x00000200, 'alias': "Apply Transparency Profile Setting to" },
        { 'mask': 0x00000400, 'alias': "Apply Jurisdiction Support Settings to SBC TG" },
        { 'mask': 0x00000800, 'alias': "Apply Jurisdiction Support Settings to" },
        { 'mask': 0x00001000, 'alias': "Select Core Stream For Multi Stream" },
        { 'mask': 0x00002000, 'alias': "Select Core Stream For Multi Stream" },
        { 'mask': 0x00004000, 'alias': "Disable Non Core Audio And Image Streams(egress)" },
        { 'mask': 0x00008000, 'alias': "Disable Non Core Audio And Image Streams(ingress)" },
        { 'mask': 0x00010000, 'alias': "Skip DTG Lookup For 3XX Contact" },
        { 'mask': 0x00020000, 'alias': "Map DPM to Send RECV For Initial Dialog(egress)" },
        { 'mask': 0x00040000, 'alias': "Map DPM to Send RECV For Initial Dialog(ingress)" },
        { 'mask': 0x00080000, 'alias': "Suppress Refer Relay From Other Leg(egress)" },
        { 'mask': 0x00100000, 'alias': "Suppress Refer Relay From Other Leg(ingress)" },
        { 'mask': 0x00200000, 'alias': "Enable Jurisdiction Support" },
        { 'mask': 0x00400000, 'alias': "Apply SIP Variant Type Setting to SBC TG" },
        { 'mask': 0x00800000, 'alias': "Apply SIP Variant Type Setting to Use SIP In Core Egress TG if Applicable" },
        { 'mask': 0x01000000, 'alias': "Ignore Unmodified Called Userpart If Truncated" },
        { 'mask': 0x02000000, 'alias': "Ignore Unmodified Calling Userpart If Truncated" },
        { 'mask': 0x04000000, 'alias': "Support Call Info With SIP Cause 608 RFC 8688(egress)" },
        { 'mask': 0x08000000, 'alias': "Support Call Info With SIP Cause 608 RFC 8688(ingress)" }
    ]

    sipCallForwardingMapping = [
        { 'mask': 0x00000000, 'alias': "Diversion" },
        { 'mask': 0x00000001, 'alias': "PK Header" },
        { 'mask': 0x00000002, 'alias': "Call History" },
        { 'mask': 0x00000003, 'alias': "None" }
    ]

    nonMirroredTransparencies1 = [
        { 'mask': 0x00000001, 'alias': "Status Code Transparency" },
        { 'mask': 0x00000002, 'alias': "Diversion Header Transparency" },
        { 'mask': 0x00000004, 'alias': "Privacy Headers Transparency" },
        { 'mask': 0x00000008, 'alias': "To Header Transparency" },
        { 'mask': 0x00000010, 'alias': "From Header Transparency" },
        { 'mask': 0x00000020, 'alias': "P Early Media" },
        { 'mask': 0x00000040, 'alias': "History Info" },
        { 'mask': 0x00000080, 'alias': "Contact Header" },
        { 'mask': 0x00000100, 'alias': "Target-Dialog Header" },
        { 'mask': 0x00000400, 'alias': "Pass Complete Contact Header" },
        { 'mask': 0x00000800, 'alias': "P-Access-Network-Info Header" },
        { 'mask': 0x00002000, 'alias': "P-Visited-Network ID Header" },
        { 'mask': 0x00008000, 'alias': "P-Charge-Info Headers Transparency" },
        { 'mask': 0x00010000, 'alias': "P-DCS-Billing Header Transparency" },
        { 'mask': 0x00020000, 'alias': "User To User Header" }
    ]

    sipDcsChargeInfo = [
        { 'value': 0, 'alias': "Include None" },
        { 'value': 1, 'alias': "Include P-Charge-Info" },
        { 'value': 2, 'alias': "Include P-DCS-Billing-Info" }
    ]

    sipDestinationTg = [
        { 'value': 0, 'alias': "Include None" },
        { 'value': 1, 'alias': "Include DTG" },
        { 'value': 2, 'alias': "Include Tgrp With IP Address" },
        { 'value': 3, 'alias': "Include Tgrp With Domain Name" },
        { 'value': 8, 'alias': "TLS Over TCP" }
    ]

    sipHeaderPrivacyInfo = [
        { 'value': 0, 'alias': "P-Preferred ID" },
        { 'value': 1, 'alias': "P-Asserted ID" },
        { 'value': 2, 'alias': "Remote Party ID" }
    ]

    sipSignalingTransportType  = [
        { 'value': 0, 'alias': "None" },
        { 'value': 1, 'alias': "TCP" },
        { 'value': 2, 'alias': "UDP" },
        { 'value': 4, 'alias': "SCTP" },
        { 'value': 8, 'alias': "TLS Over TCP" }
    ]

    sipOriginatingTg = [
        { 'value': 0, 'alias': "Include None" },
        { 'value': 1, 'alias': "Include OTG" },
        { 'value': 2, 'alias': "Include Tgrp With IP Address" },
        { 'value': 3, 'alias': "Include Tgrp With Domain Name" },
        { 'value': 8, 'alias': "TLS Over TCP" }
    ]

    sipRelayFlags1 = [
        { 'mask': 0x00000000, 'alias': "Unknown" },
        { 'mask': 0x00000001, 'alias': "Refer" },
        { 'mask': 0x00000002, 'alias': "Info" },
        { 'mask': 0x00000004, 'alias': "Options" },
        { 'mask': 0x00000008, 'alias': "Notify" },
        { 'mask': 0x00000010, 'alias': "Status Code 3XX" },
        { 'mask': 0x00000020, 'alias': "Dtmf Body" },
        { 'mask': 0x00000040, 'alias': "Ribbon Media Body" },
        { 'mask': 0x00000080, 'alias': "Third Party Bodies" },
        { 'mask': 0x00000100, 'alias': "Status Code 4XX To 6XX" },
        { 'mask': 0x00000200, 'alias': "Message" },
        { 'mask': 0X00000400, 'alias': "Dialog Event Package" },
        { 'mask': 0X00000800, 'alias': "Reg Event Package" },
        { 'mask': 0X00001000, 'alias': "Conference Event Package" },
        { 'mask': 0x00002000, 'alias': "Force 503 To 500 Relay" },
        { 'mask': 0x00004000, 'alias': "Forwarding IN-Dialog PUBLISH" },
        { 'mask': 0x00008000, 'alias': "Update without SDP" },
        { 'mask': 0x00010000, 'alias': "Reject the REFER request if no match is found" },
        { 'mask': 0x00020000, 'alias': "relay the REFER request if no match is found" },
        { 'mask': 0x00040000, 'alias': "relay the REFER request without matching" },
        { 'mask': 0x00080000, 'alias': "Reason Phrase 4xx 6XX" },
    ]

    sipToHeaderMapping = [
        { 'value': 0, 'alias': "Original Called Number (OCN)" },
        { 'value': 1, 'alias': "Called Number (CPN)" },
        { 'value': 2, 'alias': "Generic Number (Dialed)" },
        { 'value': 3, 'alias': "None" }
    ]

    sipNpdiOption = [
        { 'value': 0, 'alias': "Include npdi=yes" },
        { 'value': 1, 'alias': "Include npdi" },
        { 'value': 2, 'alias': "Do Not Include npdi" }
    ]

    sessionExpiresRefresherParm =[
        { 'value': 0, 'alias': "Not Send" },
        { 'value': 1, 'alias': "UAC" },
        { 'value': 2, 'alias': "UAS" }
    ]

    handleIpNotInNst = [
        { 'value': 0, 'alias': "Route Via Transferring IPTG" },
        { 'value': 1, 'alias': "Reject Call" },
        { 'value': 2, 'alias': "Route Via Default Zone IPTG" },
        { 'value': 3, 'alias': "Route Via DEFAULTIPTG" }
    ]

    ingHandleIpNotInNst = [
        { 'value': 0, 'alias': "Route Via Transferring IPTG" },
        { 'value': 1, 'alias': "Reject Call" },
        { 'value': 2, 'alias': "Route Via Default Zone IPTG" },
        { 'value': 3, 'alias': "Route Via DEFAULTIPTG" }
    ]

    sipSignalingTransportType2 = [
        { 'value': 0, 'alias': "None" },
        { 'value': 1, 'alias': "TCP" },
        { 'value': 2, 'alias': "UDP" },
        { 'value': 4, 'alias': "SCTP" },
        { 'value': 8, 'alias': "TLS Over TCP" }
    ]

    sipSignalingTransportType3 = [
        { 'value': 0, 'alias': "None" },
        { 'value': 1, 'alias': "TCP" },
        { 'value': 2, 'alias': "UDP" },
        { 'value': 4, 'alias': "SCTP" },
        { 'value': 8, 'alias': "TLS Over TCP" }
    ]

    sipSignalingTransportType4 = [
        { 'value': 0, 'alias': "None" },
        { 'value': 1, 'alias': "TCP" },
        { 'value': 2, 'alias': "UDP" },
        { 'value': 4, 'alias': "SCTP" },
        { 'value': 8, 'alias': "TLS Over TCP" }
    ]

    sipOptionTagInSupported = [
        { 'mask': 0x00000001, 'alias': "Suppress Replace Tag In Supported Header" }
    ]

    sipOptionTagInRequire = [
        { 'mask': 0x00000001, 'alias': "Suppress Replace Tag In Require Header" }
    ]

    action400RespWith417 = [
        { 'value': 0, 'alias': "Retry Without ETS" },
        { 'value': 1, 'alias': "Do Not Retry" },
        { 'value': 2, 'alias': "Retry With Default ETS" }    
    ]

    sipHeaderPChargeInfo = [
        { 'value': 0, 'alias': "URI Parameter" },
        { 'value': 1, 'alias': "User Parameter" },
        { 'value': 2, 'alias': "Header Parameter" }
    ]

    sipVariantType = [
        { 'value': 0, 'alias': "Use SBC TG Setting" },
        { 'value': 1, 'alias': "sonus" },
        { 'value': 2, 'alias': "ttc" },
        { 'value': 3, 'alias': "q1912" },
        { 'value': 4, 'alias': "uk" },
        { 'value': 5, 'alias': "mgcf" }
    ]

###
###  IPSP ENums
###
class Signaling:
    def __init__(self, profileName):
        self.profileName = profileName
        self.profileID = 'signalingProfileId'
        self.keysToIgnore = ['signalingProfileId']
        self.SOAPrequest = """
    <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:intf="http://www.sonusnet.com/ems/emsapi/psx/{psxVersion}/intf">
          <soapenv:Header>
          <USER soapenv:mustUnderstand="0" xsi:type="xsd:string">{user}</USER><PASSWORD soapenv:mustUnderstand="0" xsi:type="xsd:string">{password}</PASSWORD>
       </soapenv:Header>
       <soapenv:Body>
          <intf:retrieve soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
             <in0 xsi:type="xsd:string">{psxName}</in0>
             <in1 xsi:type="mod:SignalingProfileKey" xmlns:mod="http://www.sonusnet.com/ems/emsapi/psx/{psxVersion}/model">
             <signalingProfileId xsi:type="xsd:string">{profName}</signalingProfileId>
             </in1>
          </intf:retrieve>
       </soapenv:Body>
    </soapenv:Envelope>
    """
        self.profAttributes = [
            { 'key': 'signalingProfileId', 'value': '' },
            { 'key': 'tnsInterLFlag', 'value': '' },
            { 'key': 'tnsIntraLFlag', 'value': '' },
            { 'key': 'tnsInterTFlag', 'value': '' },
            { 'key': 'tnsIntraTFlag', 'value': '' },
            { 'key': 'cipFlag', 'value': '' },
            { 'key': 'callingNumberFlag', 'value': '' },
            { 'key': 'callingNumber7dFlag', 'value': '' },
            { 'key': 'calledNumber7dFlag', 'value': '' },
            { 'key': 'chargeNumberFlag', 'value': '' },
            { 'key': 'olipFlag', 'value': '' },
            { 'key': 'cspFlag', 'value': '' },
            { 'key': 'jipFlag', 'value': '' },
            { 'key': 'inboundTnsAllowedFlag', 'value': '' },
            { 'key': 'natureOfAddress', 'value': '' },
            { 'key': 'transitCarrierIndicator', 'value': '' },
            { 'key': 'attributes', 'value': '' },
            { 'key': 'genericNumber', 'value': '' },
            { 'key': 'genericNumber2', 'value': '' },
            { 'key': 'locationNumber', 'value': '' },
            { 'key': 'redirectionNumber', 'value': '' },
            { 'key': 'originalNumber', 'value': '' },
            { 'key': 'redirectingNumber', 'value': '' },
            { 'key': 'genericDigitType', 'value': '' },
            { 'key': 'attributes2', 'value': '' },
            { 'key': 'attributes3', 'value': '' },
            { 'key': 'attributes4', 'value': '' },
            { 'key': 'attributes5', 'value': '' },
            { 'key': 'clip', 'value': '' },
            { 'key': 'mobileCdOcnFlag', 'value': '' },
            { 'key': 'mobileCdRedirectInfoFlag', 'value': '' },
            { 'key': 'ingressCftValue', 'value': '' },
            { 'key': 'egressCftValue', 'value': '' },
            { 'key': 'contractorNumber', 'value': '' },
            { 'key': 'fciIsupPreference', 'value': '' }
        ]

    def genRequest(self, psxCfg):
        theRequest = self.SOAPrequest.format(user=psxCfg.getUser(), password=psxCfg.getPassword(), psxName=psxCfg.getName(), psxVersion=psxCfg.getVersion(), profName=self.profileName)
        return theRequest
    
    ###  Signaling Enums

    tnsInterLFlag  = [ 
        { 'value': 0, 'alias': "No Input" },
        { 'value': 1, 'alias': "Send" },
        { 'value': 2, 'alias': "Do Not Send" }
    ]

    tnsIntraLFlag = [ 
        { 'value': 0, 'alias': "No Input" },
        { 'value': 1, 'alias': "Send" },
        { 'value': 2, 'alias': "Do Not Send" }
    ]

    tnsInterTFlag = [ 
        { 'value': 0, 'alias': "No Input" },
        { 'value': 1, 'alias': "Send" },
        { 'value': 2, 'alias': "Do Not Send" }
    ]

    tnsIntraTFlag = [ 
        { 'value': 0, 'alias': "No Input" },
        { 'value': 1, 'alias': "Send" },
        { 'value': 2, 'alias': "Do Not Send" }
    ]

    cipFlag = [ 
        { 'value': 0, 'alias': "No Input" },
        { 'value': 1, 'alias': "Send" },
        { 'value': 2, 'alias': "Do Not Send" }
    ]

    callingNumberFlag = [ 
        { 'value': 0, 'alias': "No Input" },
        { 'value': 1, 'alias': "Send" },
        { 'value': 2, 'alias': "Do Not Send" }
    ]

    callingNumber7dFlag = [ 
        { 'value': 0, 'alias': "FALSE" },
        { 'value': 1, 'alias': "TRUE" }
    ]

    calledNumber7dFlag = [ 
        { 'value': 0, 'alias': "FALSE" },
        { 'value': 1, 'alias': "TRUE" }
    ]

    chargeNumberFlag = [ 
        { 'value': 0, 'alias': "No Input" },
        { 'value': 1, 'alias': "Send" },
        { 'value': 2, 'alias': "Do Not Send" }
    ]

    olipFlag = [ 
        { 'mask': 0x0001, 'alias': "Send" },
        { 'mask': 0x0002, 'alias': "Do Not Send" },
        { 'mask': 0x1000, 'alias': "Reset" }
    ]

    cspFlag = [ 
        { 'value': 0, 'alias': "No Input" },
        { 'value': 1, 'alias': "Send" },
        { 'value': 2, 'alias': "Do Not Send" }
    ]

    jipFlag = [ 
        { 'value': 0, 'alias': "No Input" },
        { 'value': 1, 'alias': "Send" },
        { 'value': 2, 'alias': "Do Not Send" }
    ]

    inboundTnsAllowedFlag = [ 
        { 'value': 0, 'alias': "FALSE" },
        { 'value': 1, 'alias': "TRUE" }
    ]

    natureOfAddress = [ 
        { 'value': 0, 'alias': "None" },
        { 'value': 1, 'alias': "Pass Through" },
        { 'value': 2, 'alias': "Subscriber" },
        { 'value': 3, 'alias': "National" },
        { 'value': 4, 'alias': "International" },
        { 'value': 5, 'alias': "Network Specific" },
        { 'value': 6, 'alias': "Unknown" },
        { 'value': 7, 'alias': "Subscriber Operator" },
        { 'value': 8, 'alias': "National Operator" },
        { 'value': 9, 'alias': "International Operator" },
        { 'value': 10, 'alias': "Test Code" },
        { 'value': 11, 'alias': "No Number Operator Requested" },
        { 'value': 12, 'alias': "No Number Cut Through" },
        { 'value': 13, 'alias': "950" },
        { 'value': 14, 'alias': "ANI Of Calling Party Subscriber Number" },
        { 'value': 15, 'alias': "ANI Not Available Or Not Provided" },
        { 'value': 16, 'alias': "ANI Of Calling Party National Number" },
        { 'value': 17, 'alias': "ANI Of Called Party Subscriber Number" },
        { 'value': 18, 'alias': "ANI Of Called Party No Number Present" },
        { 'value': 19, 'alias': "ANI Of Called Party National Number" },
        { 'value': 20, 'alias': "SS7 Reserved" },
        { 'value': 21, 'alias': "National Not Restricted" },
        { 'value': 22, 'alias': "National Restricted" },
        { 'value': 23, 'alias': "International Not Restricted" },
        { 'value': 24, 'alias': "International Restricted" },
        { 'value': 25, 'alias': "Ignore" },
        { 'value': 26, 'alias': "APN Numbering Plan" },
        { 'value': 27, 'alias': "SID Number" },
        { 'value': 28, 'alias': "NRN ConCatenated With DN" },
        { 'value': 126, 'alias': "Partial Calling Line ID" },
        { 'value': 128, 'alias': "Spare 00" },
        { 'value': 129, 'alias': "Spare 01" },
        { 'value': 130, 'alias': "Spare 02" },
        { 'value': 131, 'alias': "Spare 03" },
        { 'value': 132, 'alias': "Spare 04" },
        { 'value': 133, 'alias': "Spare 05" },
        { 'value': 134, 'alias': "Spare 06" },
        { 'value': 135, 'alias': "Spare 07" },
        { 'value': 136, 'alias': "Spare 08" },
        { 'value': 137, 'alias': "Spare 09" },
        { 'value': 138, 'alias': "Spare 0A" },
        { 'value': 139, 'alias': "Spare 0B" },
        { 'value': 140, 'alias': "Spare 0C" },
        { 'value': 141, 'alias': "Spare 0D" },
        { 'value': 142, 'alias': "Spare 0E" },
        { 'value': 143, 'alias': "Spare 0F" },
        { 'value': 144, 'alias': "Spare 10" },
        { 'value': 145, 'alias': "Spare 11" },
        { 'value': 146, 'alias': "Spare 12" },
        { 'value': 147, 'alias': "Spare 13" },
        { 'value': 148, 'alias': "Spare 14" },
        { 'value': 149, 'alias': "Spare 15" },
        { 'value': 150, 'alias': "Spare 16" },
        { 'value': 151, 'alias': "Spare 17" },
        { 'value': 152, 'alias': "Spare 18" },
        { 'value': 153, 'alias': "Spare 19" },
        { 'value': 154, 'alias': "Spare 1A" },
        { 'value': 155, 'alias': "Spare 1B" },
        { 'value': 156, 'alias': "Spare 1C" },
        { 'value': 157, 'alias': "Spare 1D" },
        { 'value': 158, 'alias': "Spare 1E" },
        { 'value': 159, 'alias': "Spare 1F" },
        { 'value': 160, 'alias': "Spare 20" },
        { 'value': 161, 'alias': "Spare 21" },
        { 'value': 162, 'alias': "Spare 22" },
        { 'value': 163, 'alias': "Spare 23" },
        { 'value': 164, 'alias': "Spare 24" },
        { 'value': 165, 'alias': "Spare 25" },
        { 'value': 166, 'alias': "Spare 26" },
        { 'value': 167, 'alias': "Spare 27" },
        { 'value': 168, 'alias': "Spare 28" },
        { 'value': 169, 'alias': "Spare 29" },
        { 'value': 170, 'alias': "Spare 2A" },
        { 'value': 171, 'alias': "Spare 2B" },
        { 'value': 172, 'alias': "Spare 2C" },
        { 'value': 173, 'alias': "Spare 2D" },
        { 'value': 174, 'alias': "Spare 2E" },
        { 'value': 175, 'alias': "Spare 2F" },
        { 'value': 176, 'alias': "Spare 30" },
        { 'value': 177, 'alias': "Spare 31" },
        { 'value': 178, 'alias': "Spare 32" },
        { 'value': 179, 'alias': "Spare 33" },
        { 'value': 180, 'alias': "Spare 34" },
        { 'value': 181, 'alias': "Spare 35" },
        { 'value': 182, 'alias': "Spare 36" },
        { 'value': 183, 'alias': "Spare 37" },
        { 'value': 184, 'alias': "Spare 38" },
        { 'value': 185, 'alias': "Spare 39" },
        { 'value': 186, 'alias': "Spare 3A" },
        { 'value': 187, 'alias': "Spare 3B" },
        { 'value': 188, 'alias': "Spare 3C" },
        { 'value': 189, 'alias': "Spare 3D" },
        { 'value': 190, 'alias': "Spare 3E" },
        { 'value': 191, 'alias': "Spare 3F" },
        { 'value': 192, 'alias': "Spare 40" },
        { 'value': 193, 'alias': "Spare 41" },
        { 'value': 194, 'alias': "Spare 42" },
        { 'value': 195, 'alias': "Spare 43" },
        { 'value': 196, 'alias': "Spare 44" },
        { 'value': 197, 'alias': "Spare 45" },
        { 'value': 198, 'alias': "Spare 46" },
        { 'value': 199, 'alias': "Spare 47" },
        { 'value': 200, 'alias': "Spare 48" },
        { 'value': 201, 'alias': "Spare 49" },
        { 'value': 202, 'alias': "Spare 4A" },
        { 'value': 203, 'alias': "Spare 4B" },
        { 'value': 204, 'alias': "Spare 4C" },
        { 'value': 205, 'alias': "Spare 4D" },
        { 'value': 206, 'alias': "Spare 4E" },
        { 'value': 207, 'alias': "Spare 4F" },
        { 'value': 208, 'alias': "Spare 50" },
        { 'value': 209, 'alias': "Spare 51" },
        { 'value': 210, 'alias': "Spare 52" },
        { 'value': 211, 'alias': "Spare 53" },
        { 'value': 212, 'alias': "Spare 54" },
        { 'value': 213, 'alias': "Spare 55" },
        { 'value': 214, 'alias': "Spare 56" },
        { 'value': 215, 'alias': "Spare 57" },
        { 'value': 216, 'alias': "Spare 58" },
        { 'value': 217, 'alias': "Spare 59" },
        { 'value': 218, 'alias': "Spare 5A" },
        { 'value': 219, 'alias': "Spare 5B" },
        { 'value': 220, 'alias': "Spare 5C" },
        { 'value': 221, 'alias': "Spare 5D" },
        { 'value': 222, 'alias': "Spare 5E" },
        { 'value': 223, 'alias': "Spare 5F" },
        { 'value': 224, 'alias': "Spare 60" },
        { 'value': 225, 'alias': "Spare 61" },
        { 'value': 226, 'alias': "Spare 62" },
        { 'value': 227, 'alias': "Spare 63" },
        { 'value': 228, 'alias': "Spare 64" },
        { 'value': 229, 'alias': "Spare 65" },
        { 'value': 230, 'alias': "Spare 66" },
        { 'value': 231, 'alias': "Spare 67" },
        { 'value': 232, 'alias': "Spare 68" },
        { 'value': 233, 'alias': "Spare 69" },
        { 'value': 234, 'alias': "Spare 6A" },
        { 'value': 235, 'alias': "Spare 6B" },
        { 'value': 236, 'alias': "Spare 6C" },
        { 'value': 237, 'alias': "Spare 6D" },
        { 'value': 238, 'alias': "Spare 6E" },
        { 'value': 239, 'alias': "Spare 6F" },
        { 'value': 240, 'alias': "Spare 70" },
        { 'value': 241, 'alias': "Spare 71" },
        { 'value': 242, 'alias': "Spare 72" },
        { 'value': 243, 'alias': "Spare 73" },
        { 'value': 244, 'alias': "Spare 74" },
        { 'value': 245, 'alias': "Spare 75" },
        { 'value': 246, 'alias': "Spare 76" },
        { 'value': 247, 'alias': "Spare 77" },
        { 'value': 248, 'alias': "Spare 78" },
        { 'value': 249, 'alias': "Spare 79" },
        { 'value': 250, 'alias': "Spare 7A" },
        { 'value': 251, 'alias': "Spare 7B" },
        { 'value': 252, 'alias': "Spare 7C" },
        { 'value': 253, 'alias': "Spare 7D" },
        { 'value': 254, 'alias': "Spare 7E" },
        { 'value': 255, 'alias': "Spare 7F" },
        { 'value': 256, 'alias': "All" }
    ]

    transitCarrierIndicator = [ 
        { 'value': 0, 'alias': "Pass Through" },
        { 'value': 1, 'alias': "No Transmission" },
        { 'value': 2, 'alias': "Forward" },
        { 'value': 3, 'alias': "Backward" },
        { 'value': 4, 'alias': "Bi Direction" },
        { 'value': 5, 'alias': "Ored Backward" },
        { 'value': 6, 'alias': "None" },
        { 'value': 7, 'alias': "Ored Forward" }
    ]

    attributes = [ 
        { 'mask': 0x00000004, 'alias': "Restore FCI International Bit" },
        { 'mask': 0x000000FF, 'alias': "User To User" },
        { 'mask': 0x00000100, 'alias': "Don't Convert Called Number" },
        { 'mask': 0x00000200, 'alias': "Don't Convert Calling Number" },
        { 'mask': 0x00000400, 'alias': "Disallow Without Calling Number" },
        { 'mask': 0x00000800, 'alias': "Validate Gap Type Ported Number" },
        { 'mask': 0x00001000, 'alias': "Disallow Without OLIP" },
        { 'mask': 0x00002000, 'alias': "Disallow Without Billing Number" },
        { 'mask': 0x00004000, 'alias': "Generate FE Parameter" },
        { 'mask': 0x00008000, 'alias': "Propagate FE Parameter" },
        { 'mask': 0x00010000, 'alias': "Send Billing Number As Calling Number" },
        { 'mask': 0x00020000, 'alias': "Treat CIC As Numeric" },
        { 'mask': 0x00040000, 'alias': "Treat CIC 0000 As Blank" },
        { 'mask': 0x00080000, 'alias': "Propagate GD Parameter" },
        { 'mask': 0x00200000, 'alias': "Add Prefix 1 For Intra LATA" },
        { 'mask': 0x00100000, 'alias': "Add Prefix 1 For Inter LATA" },
        { 'mask': 0x00400000, 'alias': "Add Prefix 1 For International" },
        { 'mask': 0x00800000, 'alias': "Normalize Carrier Code" },
        { 'mask': 0x01000000, 'alias': "Don't Generate Exit Message" },
        { 'mask': 0x02000000, 'alias': "Send In OCN" },
        { 'mask': 0x04000000, 'alias': "Send In GAP" },
        { 'mask': 0x08000000, 'alias': "Send Calling Number When Not Derived From Input" }
    ]

    genericNumber = [ 
        { 'value': 9, 'alias': "GN: Dialed Digits" },
        { 'value': 10, 'alias': "GN: Destination" },
        { 'value': 11, 'alias': "GN: User Calling, alias:  Screened" },
        { 'value': 12, 'alias': "GN: User Calling, alias:  Not Screened" },
        { 'value': 13, 'alias': "GN: Redirecting Terminating" },
        { 'value': 14, 'alias': "GN: Ported Dialed" },
        { 'value': 15, 'alias': "GN: Called CES Id" },
        { 'value': 16, 'alias': "GN: Additional Called" },
        { 'value': 17, 'alias': "GN: Additional Connected" },
        { 'value': 18, 'alias': "GN: Additional Calling" },
        { 'value': 19, 'alias': "GN: Additional Original Called" },
        { 'value': 20, 'alias': "GN: Additional Redirecting" },
        { 'value': 21, 'alias': "GN: Additional Redirection" },
        { 'value': 22, 'alias': "GN: NQI = [ 252" },
        { 'value': 23, 'alias': "GN: NQI = [ 253" },
        { 'value': 24, 'alias': "GN: NQI = [ 254" },
        { 'value': 26, 'alias': "Contractor Number" },
        { 'value': 27, 'alias': "GN: Network Provided Number" },
        { 'value': 28, 'alias': "Presentation Number" },
        { 'value': 29, 'alias': "From URI" },
        { 'value': 30, 'alias': "Called Directory Number" },
        { 'value': 31, 'alias': "Dialed Number" },
        { 'value': 32, 'alias': "GN: Third Party Number" },
        { 'value': 33, 'alias': "GN: Collect Call Number" },
        { 'value': 34, 'alias': "GN: Local ANI" },
        { 'value': 35, 'alias': "Called IN Number" },
        { 'value': 36, 'alias': "GN: NQI Reserved For National Use" },
        { 'value': 37, 'alias': "GTT Digits Calling" },
        { 'value': 38, 'alias': "GTT Digits Called" },
        { 'value': 108, 'alias': "Jurisdiction Info" },
        { 'value': 129, 'alias': "Generic Digits (Spare 0D)" }
    ]

    genericNumber2 = [ 
        { 'value': 9, 'alias': "GN: Dialed Digits" },
        { 'value': 10, 'alias': "GN: Destination" },
        { 'value': 11, 'alias': "GN: User Calling, alias:  Screened" },
        { 'value': 12, 'alias': "GN: User Calling, alias:  Not Screened" },
        { 'value': 13, 'alias': "GN: Redirecting Terminating" },
        { 'value': 14, 'alias': "GN: Ported Dialed" },
        { 'value': 15, 'alias': "GN: Called CES Id" },
        { 'value': 16, 'alias': "GN: Additional Called" },
        { 'value': 17, 'alias': "GN: Additional Connected" },
        { 'value': 18, 'alias': "GN: Additional Calling" },
        { 'value': 19, 'alias': "GN: Additional Original Called" },
        { 'value': 20, 'alias': "GN: Additional Redirecting" },
        { 'value': 21, 'alias': "GN: Additional Redirection" },
        { 'value': 22, 'alias': "GN: NQI = [ 252" },
        { 'value': 23, 'alias': "GN: NQI = [ 253" },
        { 'value': 24, 'alias': "GN: NQI = [ 254" },
        { 'value': 26, 'alias': "Contractor Number" },
        { 'value': 27, 'alias': "GN: Network Provided Number" },
        { 'value': 28, 'alias': "Presentation Number" },
        { 'value': 29, 'alias': "From URI" },
        { 'value': 30, 'alias': "Called Directory Number" },
        { 'value': 31, 'alias': "Dialed Number" },
        { 'value': 32, 'alias': "GN: Third Party Number" },
        { 'value': 33, 'alias': "GN: Collect Call Number" },
        { 'value': 34, 'alias': "GN: Local ANI" },
        { 'value': 35, 'alias': "Called IN Number" },
        { 'value': 36, 'alias': "GN: NQI Reserved For National Use" },
        { 'value': 37, 'alias': "GTT Digits Calling" },
        { 'value': 38, 'alias': "GTT Digits Called" },
        { 'value': 108, 'alias': "Jurisdiction Info" },
        { 'value': 129, 'alias': "Generic Digits (Spare 0D)" },
    ]

    locationNumber = [ 
        { 'value': 0, 'alias': "Calling Number" },
        { 'value': 1, 'alias': "Called Number" },
        { 'value': 2, 'alias': "Pretranslated" },
        { 'value': 3, 'alias': "Redirection" },
        { 'value': 4, 'alias': "Original Called Number" },
        { 'value': 5, 'alias': "Location" },
        { 'value': 6, 'alias': "Redirecting" },
        { 'value': 7, 'alias': "Outpulse Number" },
        { 'value': 8, 'alias': "Billing Number" },
        { 'value': 9, 'alias': "GN: Dialed Digits" },
        { 'value': 10, 'alias': "GN: Destination" },
        { 'value': 11, 'alias': "GN: User Calling, alias:  Screened" },
        { 'value': 12, 'alias': "GN: User Calling, alias:  Not Screened" },
        { 'value': 13, 'alias': "GN: Redirecting Terminating" },
        { 'value': 14, 'alias': "GN: Ported Dialed" },
        { 'value': 15, 'alias': "GN: Called CES Id" },
        { 'value': 16, 'alias': "GN: Additional Called" },
        { 'value': 17, 'alias': "GN: Additional Connected" },
        { 'value': 18, 'alias': "GN: Additional Calling" },
        { 'value': 19, 'alias': "GN: Additional Original Called" },
        { 'value': 20, 'alias': "GN: Additional Redirecting" },
        { 'value': 21, 'alias': "GN: Additional Redirection" },
        { 'value': 22, 'alias': "GN: NQI = [ 252" },
        { 'value': 23, 'alias': "GN: NQI = [ 253" },
        { 'value': 24, 'alias': "GN: NQI = [ 254" },
        { 'value': 25, 'alias': "Two Stage Collected" },
        { 'value': 26, 'alias': "Contractor Number" },
        { 'value': 27, 'alias': "GN: Network Provided Number" },
        { 'value': 28, 'alias': "Presentation Number" },
        { 'value': 29, 'alias': "From URI" },
        { 'value': 30, 'alias': "Called Directory Number" },
        { 'value': 31, 'alias': "Dialed Number" },
        { 'value': 32, 'alias': "GN: Third Party Number" },
        { 'value': 33, 'alias': "GN: Collect Call Number" },
        { 'value': 34, 'alias': "GN: Local ANI" },
        { 'value': 35, 'alias': "Called IN Number" },
        { 'value': 36, 'alias': "GN: NQI Reserved For National Use" },
        { 'value': 37, 'alias': "GTT Digits Calling" },
        { 'value': 38, 'alias': "GTT Digits Called" },
        { 'value': 39, 'alias': "AD Telephone Number" },
        { 'value': 40, 'alias': "AD Mobile" },
        { 'value': 108, 'alias': "Jurisdiction Info" },
        { 'value': 129, 'alias': "Generic Digits (Spare 0D)" },
        { 'value': 333, 'alias': "Called URI User" },
        { 'value': 334, 'alias': "Calling URI User" },
        { 'value': 335, 'alias': "Diversion URI User" },
        { 'value': 336, 'alias': "History Info URI User" },
        { 'value': 337, 'alias': "Diversion URI 1 User" },
        { 'value': 338, 'alias': "Diversion URI 2 User" },
        { 'value': 339, 'alias': "Diversion URI 3 User" },
        { 'value': 340, 'alias': "Diversion URI 4 User" },
        { 'value': 341, 'alias': "Diversion URI 5 User" },
        { 'value': 342, 'alias': "History Info URI 1 User" },
        { 'value': 343, 'alias': "History Info URI 2 User" },
        { 'value': 344, 'alias': "History Info URI 3 User" },
        { 'value': 345, 'alias': "History Info URI 4 User" },
        { 'value': 346, 'alias': "History Info URI 5 User" },
        { 'value': 429, 'alias': "Enum Response Parameter P1" },
        { 'value': 430, 'alias': "Enum Response Parameter P2" },
        { 'value': 431, 'alias': "Enum Response Parameter P3" },
        { 'value': 432, 'alias': "Enum Response Parameter P4" },
        { 'value': 433, 'alias': "Enum Response Parameter P5" },
        { 'value': 434, 'alias': "Enum Response Parameter P6" },
        { 'value': 435, 'alias': "Enum Response Parameter P7" },
        { 'value': 436, 'alias': "Enum Response Parameter P8" },
        { 'value': 437, 'alias': "Enum Response Parameter P9" },
        { 'value': 438, 'alias': "Enum Response Parameter P10" },
        { 'value': 439, 'alias': "Enum Response Parameter P11" },
        { 'value': 440, 'alias': "Enum Response Parameter P12" },
        { 'value': 441, 'alias': "Enum Response Parameter P13" },
        { 'value': 442, 'alias': "Enum Response Parameter P14" },
        { 'value': 443, 'alias': "Enum Response Parameter P15" },
        { 'value': 444, 'alias': "Enum Response Parameter P16" }
    ]

    redirectionNumber = [ 
        { 'value': 0, 'alias': "Calling Number" },
        { 'value': 1, 'alias': "Called Number" },
        { 'value': 2, 'alias': "Pretranslated" },
        { 'value': 3, 'alias': "Redirection" },
        { 'value': 4, 'alias': "Original Called Number" },
        { 'value': 5, 'alias': "Location" },
        { 'value': 6, 'alias': "Redirecting" },
        { 'value': 7, 'alias': "Outpulse Number" },
        { 'value': 8, 'alias': "Billing Number" },
        { 'value': 9, 'alias': "GN: Dialed Digits" },
        { 'value': 10, 'alias': "GN: Destination" },
        { 'value': 11, 'alias': "GN: User Calling, alias:  Screened" },
        { 'value': 12, 'alias': "GN: User Calling, alias:  Not Screened" },
        { 'value': 13, 'alias': "GN: Redirecting Terminating" },
        { 'value': 14, 'alias': "GN: Ported Dialed" },
        { 'value': 15, 'alias': "GN: Called CES Id" },
        { 'value': 16, 'alias': "GN: Additional Called" },
        { 'value': 17, 'alias': "GN: Additional Connected" },
        { 'value': 18, 'alias': "GN: Additional Calling" },
        { 'value': 19, 'alias': "GN: Additional Original Called" },
        { 'value': 20, 'alias': "GN: Additional Redirecting" },
        { 'value': 21, 'alias': "GN: Additional Redirection" },
        { 'value': 22, 'alias': "GN: NQI = [ 252" },
        { 'value': 23, 'alias': "GN: NQI = [ 253" },
        { 'value': 24, 'alias': "GN: NQI = [ 254" },
        { 'value': 25, 'alias': "Two Stage Collected" },
        { 'value': 26, 'alias': "Contractor Number" },
        { 'value': 27, 'alias': "GN: Network Provided Number" },
        { 'value': 28, 'alias': "Presentation Number" },
        { 'value': 29, 'alias': "From URI" },
        { 'value': 30, 'alias': "Called Directory Number" },
        { 'value': 31, 'alias': "Dialed Number" },
        { 'value': 32, 'alias': "GN: Third Party Number" },
        { 'value': 33, 'alias': "GN: Collect Call Number" },
        { 'value': 34, 'alias': "GN: Local ANI" },
        { 'value': 35, 'alias': "Called IN Number" },
        { 'value': 36, 'alias': "GN: NQI Reserved For National Use" },
        { 'value': 37, 'alias': "GTT Digits Calling" },
        { 'value': 38, 'alias': "GTT Digits Called" },
        { 'value': 39, 'alias': "AD Telephone Number" },
        { 'value': 40, 'alias': "AD Mobile" },
        { 'value': 108, 'alias': "Jurisdiction Info" },
        { 'value': 129, 'alias': "Generic Digits (Spare 0D)" },
        { 'value': 333, 'alias': "Called URI User" },
        { 'value': 334, 'alias': "Calling URI User" },
        { 'value': 335, 'alias': "Diversion URI User" },
        { 'value': 336, 'alias': "History Info URI User" },
        { 'value': 337, 'alias': "Diversion URI 1 User" },
        { 'value': 338, 'alias': "Diversion URI 2 User" },
        { 'value': 339, 'alias': "Diversion URI 3 User" },
        { 'value': 340, 'alias': "Diversion URI 4 User" },
        { 'value': 341, 'alias': "Diversion URI 5 User" },
        { 'value': 342, 'alias': "History Info URI 1 User" },
        { 'value': 343, 'alias': "History Info URI 2 User" },
        { 'value': 344, 'alias': "History Info URI 3 User" },
        { 'value': 345, 'alias': "History Info URI 4 User" },
        { 'value': 346, 'alias': "History Info URI 5 User" },
        { 'value': 429, 'alias': "Enum Response Parameter P1" },
        { 'value': 430, 'alias': "Enum Response Parameter P2" },
        { 'value': 431, 'alias': "Enum Response Parameter P3" },
        { 'value': 432, 'alias': "Enum Response Parameter P4" },
        { 'value': 433, 'alias': "Enum Response Parameter P5" },
        { 'value': 434, 'alias': "Enum Response Parameter P6" },
        { 'value': 435, 'alias': "Enum Response Parameter P7" },
        { 'value': 436, 'alias': "Enum Response Parameter P8" },
        { 'value': 437, 'alias': "Enum Response Parameter P9" },
        { 'value': 438, 'alias': "Enum Response Parameter P10" },
        { 'value': 439, 'alias': "Enum Response Parameter P11" },
        { 'value': 440, 'alias': "Enum Response Parameter P12" },
        { 'value': 441, 'alias': "Enum Response Parameter P13" },
        { 'value': 442, 'alias': "Enum Response Parameter P14" },
        { 'value': 443, 'alias': "Enum Response Parameter P15" },
        { 'value': 444, 'alias': "Enum Response Parameter P16" }
    ]

    originalNumber = [ 
        { 'value': 0, 'alias': "No Input" },
        { 'value': 1, 'alias': "Send" },
        { 'value': 2, 'alias': "Do Not Send" }
    ]

    redirectingNumber = [ 
        { 'value': 0, 'alias': "No Input" },
        { 'value': 1, 'alias': "Send" },
        { 'value': 2, 'alias': "Do Not Send" }
    ]

    genericDigitType = [ 
        { 'value': 0, 'alias': "Unknown" },
        { 'value': 1, 'alias': "Account Code" },
        { 'value': 2, 'alias': "Authorization Code" },
        { 'value': 3, 'alias': "PN Travelling Classmark " },
        { 'value': 4, 'alias': "Bill To Number" },
        { 'value': 31, 'alias': "Reserve Offset  " },
        { 'value': 20, 'alias': "BCG ID  " },
        { 'value': 32, 'alias': "Spare00   " },
        { 'value': 33, 'alias': "Spare01" },
        { 'value': 34, 'alias': "Spare02" },
        { 'value': 35, 'alias': "Spare03" },
        { 'value': 36, 'alias': "Spare04" },
        { 'value': 37, 'alias': "Spare05" },
        { 'value': 38, 'alias': "Spare06" },
        { 'value': 39, 'alias': "Spare07" },
        { 'value': 40, 'alias': "Spare08" },
        { 'value': 41, 'alias': "Spare09" },
        { 'value': 48, 'alias': "Spare10" },
        { 'value': 42, 'alias': "Spare0A" },
        { 'value': 43, 'alias': "Spare0B" },
        { 'value': 44, 'alias': "Spare0C" },
        { 'value': 45, 'alias': "Spare0D" },
        { 'value': 46, 'alias': "Spare0E" },
        { 'value': 47, 'alias': "Spare0F" }
    ]

    attributes2 = [ 
        { 'mask': 0x00000001, 'alias': "ATP Supported" },
        { 'mask': 0x00000002, 'alias': "Eg Redirect Capability" },
        { 'mask': 0x00000004, 'alias': "Eg Redirect Counter" },
        { 'mask': 0x00000008, 'alias': "Eg Redirect Information" },
        { 'mask': 0x00000010, 'alias': "Use Output ANI For CDNIS" },
        { 'mask': 0x00000020, 'alias': "Send Billing Number As Calling Number If Calling Number Not , alias: Present" },
        { 'mask': 0x00000080, 'alias': "Disallow TNS And TG Originating Carrier Match" },
        { 'mask': 0x00000100, 'alias': "Discard TNS And TG Own Carrier Match" },
        { 'mask': 0x00000400, 'alias': "Generate Charge Message" },
        { 'mask': 0x00000800, 'alias': "Propagate Charge Message" },
        { 'mask': 0x00001000, 'alias': "ISUP Pref Reorder" },
        { 'mask': 0x00002000, 'alias': "Dialed Number As Called Number" },
        { 'mask': 0x00004000, 'alias': "Has SIP Parameters" },
        { 'mask': 0x00008000, 'alias': "Annex E Support" },
        { 'mask': 0x00010000, 'alias': "Restore Ingress Parameter" },
        { 'mask': 0x00020000, 'alias': "Skip Egress TG Processing" },
        { 'mask': 0x00040000, 'alias': "OLI Mapping" },
        { 'mask': 0x00080000, 'alias': "Convert Numbers To E164 Format" },
        { 'mask': 0x00100000, 'alias': "Undo LNP" },
        { 'mask': 0x00200000, 'alias': "Apply Switch Type CPC Profile" },
        { 'mask': 0x00400000, 'alias': "CPC Mapping" },
        { 'mask': 0x00800000, 'alias': "Reroute On Signaling Congestion" },
        { 'mask': 0x01000000, 'alias': "Restore Calling Number If Derived From Billing Number" },
        { 'mask': 0x02000000, 'alias': "Restore Calling Number If Derived From Redirecting Number" },
        { 'mask': 0x04000000, 'alias': "Restore Calling Number If Derived From Trunk Group" },
        { 'mask': 0x08000000, 'alias': "Restore Translated Numbers" }
    ]

    attributes3 = [ 
        { 'mask': 0x00000001, 'alias': "Don't Strip Calling Number For Restricted Presentation" },
        { 'mask': 0x00000002, 'alias': "UK ISUP Type: Trusted" },
        { 'mask': 0x00000004, 'alias': "UK ISUP Type: Untrusted" },
        { 'mask': 0x00000008, 'alias': "CLI Blocking Available" },
        { 'mask': 0x00000010, 'alias': "Alternate Route Allowed, alias:  Continuous Retry Allowed" },
        { 'mask': 0x00000020, 'alias': "Alternate Route Barred, alias:  Continuous Retry Allowed" },
        { 'mask': 0x00000030, 'alias': "Alternate Route Allowed, alias:  Continuous Retry Barred" },
        { 'mask': 0x00000040, 'alias': "Alternate Route Barred, alias:  Continuous Retry Barred" },
        { 'mask': 0x00000050, 'alias': "Alternate Route Once, alias:  Continuous Retry Allowed" },
        { 'mask': 0x00000060, 'alias': "Alternate Route Once, alias:  Continuous Retry Barred" },
        { 'mask': 0x00000080, 'alias': "Network Translated Address Occurred" },
        { 'mask': 0x00000100, 'alias': "Discard Calling And Contract Numbers" },
        { 'mask': 0x00000200, 'alias': "Overwrite Charge Information If Chosen IXC" },
        { 'mask': 0x00000400, 'alias': "Separate Alert From Progress For SIP Interworking" },
        { 'mask': 0x00000800, 'alias': "Disable Charging Authority For Chosen Carrier" },
        { 'mask': 0x00001000, 'alias': "Send Contract Number If Allowed By Ingress SIP" },
        { 'mask': 0x00002000, 'alias': "Discard GAP Additional Calling If Same As Calling Number" },
        { 'mask': 0x00004000, 'alias': "Use ISUP Immediate REL On SUS Timer" },
        { 'mask': 0x00008000, 'alias': "Change Bearer Cap From 3.1KHz To Speech" },
        { 'mask': 0x00010000, 'alias': "FE Parameter In Short Form" },
        { 'mask': 0x20020000, 'alias': "Forced Override OLIP Value." },
        { 'mask': 0x00080000, 'alias': "Generate CPG For Call Forward Notify" },
        { 'mask': 0x00100000, 'alias': "Enable Ingress Transfer Connect" },
        { 'mask': 0x00200000, 'alias': "Send DM/PM Manipulated Billing Number" },
        { 'mask': 0x00400000, 'alias': "Restore Calling Number If Derived From OCN" }
    ]

    attributes4 = [ 
        { 'mask': 0x00000001, 'alias': "Discard Redirection Backward Information" },
        { 'mask': 0x00000002, 'alias': "Discard Redirection Count for REL" },
        { 'mask': 0x00000004, 'alias': "Check Number Control Profile For Received Called Number" },
        { 'mask': 0x00000008, 'alias': "Perform Called Party Member Check (For JTI)" },
        { 'mask': 0x00000010, 'alias': "Perform Calling Party Screening" },
        { 'mask': 0x00000020, 'alias': "Perform CPC Screening" },
        { 'mask': 0x00000040, 'alias': "Transit All Redirection Number NOAs" },
        { 'mask': 0x00000080, 'alias': "Skip Redirection After Number Translations" },
        { 'mask': 0x00000100, 'alias': "Skip Redirection On RAC Indication Set" },
        { 'mask': 0x00000200, 'alias': "Trusted For COL" },
        { 'mask': 0x00000400, 'alias': "Don't Send Restricted" },
        { 'mask': 0x00000800, 'alias': "Don't Send Connected" },
        { 'mask': 0x00001000, 'alias': "Don't Send Unrequested" },
        { 'mask': 0x00002000, 'alias': "COLP/COLR Support" },
        { 'mask': 0x00004000, 'alias': "Suppress ONI" },
        { 'mask': 0x00010000, 'alias': "Prefix RN to Dialed Digits" }
    ]

    attributes5 = [ 
        { 'mask': 0x00000001, 'alias': "Propagate Ingress Channel Information" },
        { 'mask': 0x00000002, 'alias': "Propagate Egress Channel Information" },
        { 'mask': 0x00000004, 'alias': "Propagate Route Index GD" },
        { 'mask': 0x00000008, 'alias': "CFT Send" },
        { 'mask': 0x00000010, 'alias': "Calling Name Send" },
        { 'mask': 0x00000020, 'alias': "Calling Name Don't Send" },
        { 'mask': 0x00000030, 'alias': "Calling Name" },
        { 'mask': 0x000000C0, 'alias': "Visitor MA" },
        { 'mask': 0x00000040, 'alias': "Visitor MA Send" },
        { 'mask': 0x00000080, 'alias': "Visitor MA Don't Send" },
        { 'mask': 0x00000100, 'alias': "Call Back When Free Support" },
        { 'mask': 0x00000200, 'alias': "Ingress Trunkgroup Redirection Capability" },
        { 'mask': 0x00000400, 'alias': "Egress Redirection Capability" },
        { 'mask': 0x00000800, 'alias': "Check Ingress Trunk Group Redirection Capability" },
        { 'mask': 0x00001000, 'alias': "Check Received Redirection Parameters" },
        { 'mask': 0x00002000, 'alias': "Check OLEC And Chosen IXC" },
        { 'mask': 0x00004000, 'alias': "Check Redirection Capability Of Number Used For Routing In Number Control Profile" },
        { 'mask': 0x00008000, 'alias': "Check SIP Indirect DIP And Translation Source Number" },
        { 'mask': 0x00030000, 'alias': "Called Directory Number" },
        { 'mask': 0x00010000, 'alias': "Called Directory Number Send" },
        { 'mask': 0x00020000, 'alias': "Called Directory Number Don't Send" },
        { 'mask': 0x00040000, 'alias': "Create Called Directory Number In Egress Signal On Redirection" },
        { 'mask': 0x00080000, 'alias': "Replace Prefix 0 With +81 For Translation Source Number" },
        { 'mask': 0x00100000, 'alias': "Create Called Directory Number In Egress Signal" },
        { 'mask': 0x00200000, 'alias': "Undo Donor Switch Service" },
        { 'mask': 0x00400000, 'alias': "Generate PartitionId + NetId In NetworkData In IAM" },
        { 'mask': 0x00800000, 'alias': "Propagate PartitionId + NetId In NetworkData In IAM" },
        { 'mask': 0x01000000, 'alias': "Override PartitionId + NetId In NetworkData In IAM" },
        { 'mask': 0x02000000, 'alias': "Called Number From Alternate Called Number" },
        { 'mask': 0x04000000, 'alias': "Strip Rep Area Code Digits" },
        { 'mask': 0x08000000, 'alias': "Generate Redirection Count" }
    ]

    clip = [ 
        { 'value': 0, 'alias': "None" },
        { 'value': 1, 'alias': "Allowed" },
        { 'value': 2, 'alias': "Restricted" },
        { 'value': 3, 'alias': "Number Unavailable" },
        { 'value': 4, 'alias': "Spare" }
    ]

    mobileCdOcnFlag = [ 
        { 'value': 0, 'alias': "No Input" },
        { 'value': 1, 'alias': "Send" },
        { 'value': 2, 'alias': "Do Not Send" }
    ]

    mobileCdRedirectInfoFlag = [ 
        { 'value': 0, 'alias': "No Input" },
        { 'value': 1, 'alias': "Send" },
        { 'value': 2, 'alias': "Do Not Send" }
    ]

    ingressCftValue = [ 
        { 'value': 0, 'alias': "Off Net" },
        { 'value': 1, 'alias': "On Net" },
        { 'value': 2, 'alias': "Unknown" }
    ]

    egressCftValue = [ 
        { 'value': 0, 'alias': "Off Net" },
        { 'value': 1, 'alias': "On Net" },
        { 'value': 2, 'alias': "Unknown" }
    ]

    contractorNumber = [ 
        { 'value': 0, 'alias': "No Input" },
        { 'value': 1, 'alias': "Send" },
        { 'value': 2, 'alias': "Do Not Send" }
    ]

    fciIsupPreference = [ 
        { 'value': 0, 'alias': "None" },
        { 'value': 1, 'alias': "Not Required" },
        { 'value': 2, 'alias': "Required" },
        { 'value': 3, 'alias': "Preferred" }
    ]

###
###  Feature Control enums
###
class FeatureControl:
    def __init__(self, profileName):
        self.profileName = profileName
        self.profileID = 'featureControlProfileId'
        self.keysToIgnore = ['featureControlProfileId']
        self.SOAPrequest = """
    <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:intf="http://www.sonusnet.com/ems/emsapi/psx/{psxVersion}/intf">
          <soapenv:Header>
          <USER soapenv:mustUnderstand="0" xsi:type="xsd:string">{user}</USER><PASSWORD soapenv:mustUnderstand="0" xsi:type="xsd:string">{password}</PASSWORD>
       </soapenv:Header>
       <soapenv:Body>
          <intf:retrieve soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
             <in0 xsi:type="xsd:string">{psxName}</in0>
             <in1 xsi:type="mod:FeatureControlProfileKey" xmlns:mod="http://www.sonusnet.com/ems/emsapi/psx/{psxVersion}/model">
             <featureControlProfileId xsi:type="xsd:string">{profName}</featureControlProfileId>
             </in1>
          </intf:retrieve>
       </soapenv:Body>
    </soapenv:Envelope>
    """
        self.profAttributes = [
            { 'key': 'featureControlProfileId', 'value': '' },
            { 'key': 'featureList1', 'value': '' },
            { 'key': 'featureList2', 'value': '' },
            { 'key': 'featureList3', 'value': '' },
            { 'key': 'featureList4', 'value': '' },
            { 'key': 'featureList5', 'value': '' },
            { 'key': 'genericAttributes1', 'value': '' },
            { 'key': 'genericAttributes2', 'value': '' },
            { 'key': 'genericAttributes3', 'value': '' },
            { 'key': 'genericAttributes4', 'value': '' }    
        ]

    def genRequest(self, psxCfg):
        theRequest = self.SOAPrequest.format(user=psxCfg.getUser(), password=psxCfg.getPassword(), psxName=psxCfg.getName(), psxVersion=psxCfg.getVersion(), profName=self.profileName)
        return theRequest
        
    ###  Feature Contols Enums

    featureList1 = [
        { 'mask': 0x00000001, 'alias': "Apply OLIP Services" },
        { 'mask': 0x00000002, 'alias': "Apply Business Group Services" },
        { 'mask': 0x00000004, 'alias': "Process Generic Digits" },
        { 'mask': 0x00000008, 'alias': "Process Calling Number" },
        { 'mask': 0x00000010, 'alias': "Process Called Number" },
        { 'mask': 0x00000020, 'alias': "Apply Digit Length Enforcement" },
        { 'mask': 0x00000040, 'alias': "Normalize Digits" },
        { 'mask': 0x00000080, 'alias': "Use Trunk Group Country" },
        { 'mask': 0x00000100, 'alias': "Ignore Translation" },
        { 'mask': 0x00000200, 'alias': "Apply Calling Party Services" },
        { 'mask': 0x00000400, 'alias': "Use Billing Number For Subscriber" },
        { 'mask': 0x00000800, 'alias': "Apply Called Party Services" },
        { 'mask': 0x00001000, 'alias': "Apply Destination Services" },
        { 'mask': 0x00002000, 'alias': "Determine JIP" },
        { 'mask': 0x00004000, 'alias': "Determine Toll" },
        { 'mask': 0x00008000, 'alias': "Determine LATA, Region and MTA" },
        { 'mask': 0x00010000, 'alias': "Apply Dial Plan" },
        { 'mask': 0x00020000, 'alias': "Determine Carrier Homing" },
        { 'mask': 0x00040000, 'alias': "Skip SCP Services" },
        { 'mask': 0x00080000, 'alias': "Filter Routes Before Prioritization" },
        { 'mask': 0x00100000, 'alias': "Use Billing Number For Normalization" },
        { 'mask': 0x00200000, 'alias': "Process Presentation Setting" },
        { 'mask': 0x00400000, 'alias': "Process Screening Setting" },
        { 'mask': 0x00800000, 'alias': "Always Apply Default ANI From Trunk Group" },
        { 'mask': 0x01000000, 'alias': "Apply Default CPG If CPG Not Present" },
        { 'mask': 0x02000000, 'alias': "Apply Default CPG If CPG Not Valid" },
        { 'mask': 0x04000000, 'alias': "Apply Default CPG If CPG Not Subscriber" },
        { 'mask': 0x08000000, 'alias': "Always Apply Default Presentation Indicator From Trunk Group" } 
    ]

    featureList2 = [
        { 'mask': 0x10000001, 'alias': "Validate Originating Carrier Code" },
        { 'mask': 0x10000002, 'alias': "Process Called Party NOA" },
        { 'mask': 0x10000004, 'alias': "Process Called Party NPI" },
        { 'mask': 0x10000008, 'alias': "Process Calling Party NOA" },
        { 'mask': 0x10000010, 'alias': "Process Calling Party NPI" },
        { 'mask': 0x10000020, 'alias': "Use Redirecting Number For Subscriber" },
        { 'mask': 0x10000040, 'alias': "Apply Calling Party Category Services" },
        { 'mask': 0x10000080, 'alias': "Determine Charge Band" },
        { 'mask': 0x10000100, 'alias': "Has SIP Parameters" },
        { 'mask': 0x10000200, 'alias': "Skip LNP For Toll Calls" },
        { 'mask': 0x10000400, 'alias': "Error On Misrouted LRN" },
        { 'mask': 0x10000800, 'alias': "Do Not Error On LRN Non Subscriber" },
        { 'mask': 0x10001000, 'alias': "Apply All Countries Routing" },
        { 'mask': 0x10002000, 'alias': "Treat Presub Input Carrier As Not Casual" },
        { 'mask': 0x10004000, 'alias': "Treat Not Presub input Carrier As Not Casual" },
        { 'mask': 0x10008000, 'alias': "No Local Calls" },
        { 'mask': 0x10010000, 'alias': "Always Use Ingress CSP" },
        { 'mask': 0x10020000, 'alias': "Enable Route Index Feature" },
        { 'mask': 0x10040000, 'alias': "Always Use Provisioned Route Index" },
        { 'mask': 0x10080000, 'alias': "Always Use Trunk Group JIP" },
        { 'mask': 0x10100000, 'alias': "Trigger LNP For Operator Plus Calls" },
        { 'mask': 0x10200000, 'alias': "Use Carrier Destination Over Interworking For Charge Indicators" },
        { 'mask': 0x10400000, 'alias': "Use Billing number for Calling Party Number if Calling Party Number not received" },
        { 'mask': 0x10800000, 'alias': "Always Use Billing Number For Calling Party Number" },
        { 'mask': 0x11000000, 'alias': "Skip LRN Validation and Unporting after LNP" },
        { 'mask': 0x12000000, 'alias': "Skip Called Party Services For Misrouted LRN" },
        { 'mask': 0x14000000, 'alias': "Do Not Replace Calling Number For Emergency Calls" },
        { 'mask': 0x18000000, 'alias': "Always Use Redirecting Number For Calling Number" } 
    ]

    featureList3 = [
        { 'mask': 0x20000001, 'alias': "SSG Calling Party Use Signal-In Number" },
        { 'mask': 0x20000002, 'alias': "Use Redirecting BG" },
        { 'mask': 0x20000004, 'alias': "Allow CMT" },
        { 'mask': 0x20000008, 'alias': "Always Process Calling Number If NOA Unknown" },
        { 'mask': 0x20000010, 'alias': "Always Process Called Number If NOA Unknown" },
        { 'mask': 0x20000020, 'alias': "Fetch Subscriber With Country Code Prefixed" },
        { 'mask': 0x20000040, 'alias': "Process Redirection Number" },
        { 'mask': 0x20000080, 'alias': "Process Called Directory Number" },
        { 'mask': 0x20000100, 'alias': "Add Number Of Prefix Digits Stripped And Overlap Dial From Current Match" },
        { 'mask': 0x20000200, 'alias': "All Combinations Of Local Calling Areas Match Determination" },
        { 'mask': 0x20000400, 'alias': "Use Original Calling If Contractor Not Applied For Traffic Control" },
        { 'mask': 0x20000800, 'alias': "Use Redirecting Number Instead of CLI For DDI Screening" },
        { 'mask': 0x20001000, 'alias': "Use Redirecting Number For Called Number Normalization" },
        { 'mask': 0x20002000, 'alias': "Try Alternate Address For SIPE" },
        { 'mask': 0x20004000, 'alias': "Apply LATA from Trunk Group If Calling Number Not Present" },
        { 'mask': 0x20008000, 'alias': "Translated Emergency Number" },
        { 'mask': 0x20010000, 'alias': "Generate ECI" },
        { 'mask': 0x20020000, 'alias': "Translate Emergency Number to PSAP" },
        { 'mask': 0x20040000, 'alias': "Populate Subscriber Location Info" },
        { 'mask': 0x20080000, 'alias': "Use OCN For Calling Party Number If Redirecting Number Not Present" },
        { 'mask': 0x20200000, 'alias': "Perform Route Header Based Routing" },
        { 'mask': 0x20100000, 'alias': "Disable Fallback To 7 Digits Hosted LNP Lookup" },
        { 'mask': 0x20800000, 'alias': "Use TrunkGroup Configured Charge Band Profile" },
        { 'mask': 0x21000000, 'alias': "Determine Charge Band Profile from TG" },
        { 'mask': 0x22000000, 'alias': "Don't Send \\1 In ENUM Response" },
        { 'mask': 0x24000000, 'alias': "Use Redirecting Country Code For Subscriber Search" }
    ]

    featureList4 = [
        { 'mask': 0x30000001, 'alias': "Sip Proxy Mode" },
        { 'mask': 0x30000002, 'alias': "SIP Support PAI In Contact" },
        { 'mask': 0x30000004, 'alias': "Reject Calls To Non-Local Domains" },
        { 'mask': 0x30000008, 'alias': "Reject Calls To Non-Local IP Addresses" },
        { 'mask': 0x30000010, 'alias': "Default Calling User As A User Name" },
        { 'mask': 0x30000020, 'alias': "Support Domain Name In 300 Contact" },
        { 'mask': 0x30000040, 'alias': "Default Called User As A User Name" },
        { 'mask': 0x30000080, 'alias': "Disable Egress Check And Don't Send Contract Number" },
        { 'mask': 0x30000100, 'alias': "Don't Apply Called Party Services During LNP Transition" },
        { 'mask': 0x30000200, 'alias': "Use Called Directory Number For Services" },
        { 'mask': 0x30000400, 'alias': "Apply Network Traffic Management On Indirect Dip" },
        { 'mask': 0x30000800, 'alias': "Proxy/Redirector Force Route Calls With Non-Local IP Address" },
        { 'mask': 0x30001000, 'alias': "Set APRI For Network Provided Calling Party Number If Privacy Header Received" },
        { 'mask': 0x30002000, 'alias': "Skip Number Translations For Valid Route Label" },
        { 'mask': 0x30004000, 'alias': "Enable RPH ETS" },
        { 'mask': 0x30008000, 'alias': "Accept Calls With RPH If Dialed Number Is Non ETS" },
        { 'mask': 0x30010000, 'alias': "Process Originating Trunk Group And Trunk-Context Over OTG" },
        { 'mask': 0x30020000, 'alias': "Process Destination Trunk Group And Trunk-Context" },
        { 'mask': 0x30040000, 'alias': "Prefer BICC instead of ISUP routes for FCI preferred value" },
        { 'mask': 0x30080000, 'alias': "SIP Cause Code Mapping" },
        { 'mask': 0x30100000, 'alias': "Include Retry After for 503 Responses" },
        { 'mask': 0x30200000, 'alias': "Process Enumdi Parameter" },
        { 'mask': 0x30400000, 'alias': "Process Swid And Tgid From Sip Invite" },
        { 'mask': 0x30800000, 'alias': "Process Dialed Number" },
        { 'mask': 0x31000000, 'alias': "Restart Timer C on 1xx" },
        { 'mask': 0x32000000, 'alias': "Process State from NPA/NXX Table" },
        { 'mask': 0x34000000, 'alias': "Process Destination Trunk-Context" },
        { 'mask': 0x38000000, 'alias': "Override Trunkgroup With Subscriber End Point Profile" }  
    ]

    featureList5 = [
        { 'mask': 0x40000001, 'alias': "Honor Phone Context" },
        { 'mask': 0x40000002, 'alias': "Determine MTA For LRN in Ported Calls" },
        { 'mask': 0x40000008, 'alias': "Exclude LATA Sub-Zone Id For Determining Toll Indication" },
        { 'mask': 0x40000010, 'alias': "Enable Per Route Routing Label" },
        { 'mask': 0x40000040, 'alias': "Process ISUP MIME From SIP Message Body" },
        { 'mask': 0x40000080, 'alias': "Apply Number Translation To Called Directory Number If Received In IAM" },
        { 'mask': 0x40000020, 'alias': "Do Not Validate GAP" },
        { 'mask': 0x40000100, 'alias': "Use Flex Variable for Origination Jurisdiction Determination" },
        { 'mask': 0x40000200, 'alias': "Use Flex Variable for Destination Jurisdiction Determination" },
        { 'mask': 0x40000400, 'alias': "Support Number Portability Redirection for Donor Switch Service" },
        { 'mask': 0x40000800, 'alias': "Determine LATA, Region and MTA for LRN in Ported Calls" },
        { 'mask': 0x40001000, 'alias': "Enable STI Service for STIR/SHAKEN functionality" },
        { 'mask': 0x40002000, 'alias': "Process Screening For Call Origination" },
        { 'mask': 0x40004000, 'alias': "Process TO URI User" },
        { 'mask': 0x40008000, 'alias': "Process FROM URI User" },
        { 'mask': 0x40010000, 'alias': "Process PAI URI User" },
        { 'mask': 0x40020000, 'alias': "Process Diversion URI User" },
        { 'mask': 0x40040000, 'alias': "Process Calling URI User" },
        { 'mask': 0x40080000, 'alias': "Process Called URI User" },
        { 'mask': 0x40100000, 'alias': "Process History-Info URI User" },
        { 'mask': 0x40200000, 'alias': "Start Using Processed URI User Data" },
        { 'mask': 0x40400000, 'alias': "Continue DM/PM Service Modified: for 15.1.3" },
        { 'mask': 0x40800000, 'alias': "Skip Userinfo check in R-URI Modified: for 15.1.4" }
    ]

###
###  Element Routing Priority
###
class ElementRoutingPriority:
    def __init__(self, profileName):
        self.profileName = profileName
        self.profileID = 'erpProfileId'
        self.keysToIgnore = ['erpProfileId']
        self.SOAPrequest = """
    <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:intf="http://www.sonusnet.com/ems/emsapi/psx/{psxVersion}/intf">
          <soapenv:Header>
          <USER soapenv:mustUnderstand="0" xsi:type="xsd:string">{user}</USER><PASSWORD soapenv:mustUnderstand="0" xsi:type="xsd:string">{password}</PASSWORD>
       </soapenv:Header>
       <soapenv:Body>
          <intf:retrieve soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
             <in0 xsi:type="xsd:string">{psxName}</in0>
             <in1 xsi:type="mod:ElementRoutingPriorityKey" xmlns:mod="http://www.sonusnet.com/ems/emsapi/psx/{psxVersion}/model">             
             <erpProfileId xsi:type="xsd:string">{profName}</erpProfileId>
             <cpCalltype xsi:type="xsd:int">0</cpCalltype>
             <cpPriority xsi:type="xsd:short">1</cpPriority>
             <priority xsi:type="xsd:short">1</priority>
             </in1>
          </intf:retrieve>
       </soapenv:Body>
    </soapenv:Envelope>
    """
        self.profAttributes = [
            { 'key': 'erpProfileId', 'value': '' },
            { 'key': 'cpCalltype', 'value': '' },
            { 'key': 'cpPriority', 'value': '' },
            { 'key': 'priority', 'value': '' },
            { 'key': 'cpOnOffNet', 'value': '' },
            { 'key': 'cpTollIndication', 'value': '' },
            { 'key': 'callProcessingElementType', 'value': '' }    
        ]

    def genRequest(self, psxCfg):
        theRequest = self.SOAPrequest.format(user=psxCfg.getUser(), password=psxCfg.getPassword(), psxName=psxCfg.getName(), psxVersion=psxCfg.getVersion(), profName=self.profileName)
        return theRequest

    ###  Element Routing Priority Enums

    cpCalltype = [
        { 'value': 0, 'alias': "Private" },
        { 'value': 6, 'alias': "Toll Free" },
        { 'value': 8, 'alias': "National Operator" },
        { 'value': 9, 'alias': "Local Operator" },
        { 'value': 10, 'alias': "National" },
        { 'value': 11, 'alias': "International" },
        { 'value': 12, 'alias': "International Operator" },
        { 'value': 16, 'alias': "Internet Offload Premium" },
        { 'value': 17, 'alias': "Internet Offload Standard" },
        { 'value': 18, 'alias': "Internet Offload Economy" },
        { 'value': 19, 'alias': "Internet Offload Anonymous" },
        { 'value': 20, 'alias': "IP VPN Service" },
        { 'value': 21, 'alias': "Test" },
        { 'value': 22, 'alias': "Internet Reception Service" },
        { 'value': 23, 'alias': "Transit" },
        { 'value': 24, 'alias': "Other Carrier Chosen" },
        { 'value': 25, 'alias': "Carrier Cut Through" },
        { 'value': 26, 'alias': "User Name" },
        { 'value': 27, 'alias': "Switch ID Trunk Group ID" },
        { 'value': 28, 'alias': "Mobile" }
    ]

    cpOnOffNet = [
        { 'value': 0, 'alias': "All" },
        { 'value': 1, 'alias': "On Network" },
        { 'value': 2, 'alias': "Off Network" }
    ]

    cpTollIndication = [
        { 'mask': 0x01, 'alias': "Inter LATA Toll" },
        { 'mask': 0x02, 'alias': "Inter LATA Local" },
        { 'mask': 0x04, 'alias': "Intra LATA Local" },
        { 'mask': 0x08, 'alias': "Intra LATA Toll" },
        { 'mask': 0x10, 'alias': "International" },
        { 'mask': 0x20, 'alias': "Private" },
        { 'mask': 0x40, 'alias': "Unknown" },
        { 'mask': 0xff, 'alias': "All" }
    ]

    callProcessingElementType =[
        { 'value': 0, 'alias': "None" },
        { 'value': 1, 'alias': "Authcode" },
        { 'value': 2, 'alias': "Infodigit" },
        { 'value': 3, 'alias': "Subscriber" },
        { 'value': 4, 'alias': "Carrier" },
        { 'value': 5, 'alias': "Trunk Group" },
        { 'value': 6, 'alias': "Gateway" },
        { 'value': 7, 'alias': "Softswitch" },
        { 'value': 8, 'alias': "Calling Number" },
        { 'value': 9, 'alias': "Non Subscriber" },
        { 'value': 10, 'alias': "Toll Free Number" },
        { 'value': 11, 'alias': "All Element Types" },
        { 'value': 12, 'alias': "Trunk Group Domain" },
        { 'value': 13, 'alias': "Billing Number" },
        { 'value': 14, 'alias': "Origination LATA" },
        { 'value': 15, 'alias': "Dial Plan Treatment Profile" },
        { 'value': 16, 'alias': "Business Group" },
        { 'value': 17, 'alias': "Class Of Service" },
        { 'value': 18, 'alias': "Destination" },
        { 'value': 19, 'alias': "Switch ID Trunk Group" },
        { 'value': 20, 'alias': "Gateway Cluster" },
        { 'value': 21, 'alias': "Business Unit" },
        { 'value': 22, 'alias': "Business Group Subscriber" },
        { 'value': 23, 'alias': "CPC" },
        { 'value': 24, 'alias': "TNS Circuit Code" },
        { 'value': 25, 'alias': "Origination Entity Routing Profile" },
        { 'value': 26, 'alias': "Routing Label" },
        { 'value': 27, 'alias': "Traffic Control Application" },
        { 'value': 28, 'alias': "Original Called Number" },
        { 'value': 29, 'alias': "Gap Ported Number" },
        { 'value': 30, 'alias': "Additional Party Category" },
        { 'value': 31, 'alias': "Originating Local Exchange Carrier" },
        { 'value': 32, 'alias': "Originating Charge Area" },
        { 'value': 33, 'alias': "User To User Information" },
        { 'value': 34, 'alias': "Visitor MA" },
        { 'value': 35, 'alias': "SIP Message Type" },
        { 'value': 36, 'alias': "Contractor" },
        { 'value': 37, 'alias': "Redirecting Party" },
        { 'value': 38, 'alias': "Originating Local Exchange Carrier Group" },
        { 'value': 39, 'alias': "Originating Charge Area Group" },
        { 'value': 40, 'alias': "Originating Carrier" },
        { 'value': 41, 'alias': "Original Calling Number" },
        { 'value': 42, 'alias': "SCP Controlled CCG" },
        { 'value': 43, 'alias': "Routing Label TM" },
        { 'value': 44, 'alias': "Call Content Routing" },
        { 'value': 45, 'alias': "Called Number Length" },
        { 'value': 46, 'alias': "SCF ID" },
        { 'value': 47, 'alias': "Correlation ID" },
        { 'value': 48, 'alias': "DPC Info" },
        { 'value': 49, 'alias': "Prefix" },
        { 'value': 50, 'alias': "Member Status" },
        { 'value': 51, 'alias': "Chosen IXC" },
        { 'value': 52, 'alias': "Transmission Medium" },
        { 'value': 53, 'alias': "TNS Network Identifier" },
        { 'value': 54, 'alias': "Received Called Number NOA" },
        { 'value': 55, 'alias': "Trunk Group Type" },
        { 'value': 56, 'alias': "DPC Ingress TG Category" },
        { 'value': 57, 'alias': "Nature Of Address" },
        { 'value': 58, 'alias': "Received Called Prefix" },
        { 'value': 59, 'alias': "Received JTI" },
        { 'value': 60, 'alias': "SCP Carrier" },
        { 'value': 61, 'alias': "IP Peer" },
        { 'value': 62, 'alias': "MSRP Service" },
        { 'value': 63, 'alias': "Customer Identifier" },
        { 'value': 64, 'alias': "Origination Network Identifier" },
        { 'value': 65, 'alias': "Origination Region" },
        { 'value': 66, 'alias': "Connect To Service Node" },
        { 'value': 67, 'alias': "Trunkgroup Group" },
        { 'value': 68, 'alias': "Gateway Group Collection" },
        { 'value': 69, 'alias': "npdi" },
        { 'value': 70, 'alias': "Current Called Prefix" },
        { 'value': 71, 'alias': "Origination MTA" },
        { 'value': 72, 'alias': "AD" },
        { 'value': 73, 'alias': "Redirection Number" },
        { 'value': 74, 'alias': "SIP Header Type" },
        { 'value': 75, 'alias': "UE Dual Mode Compatibility" },
        { 'value': 76, 'alias': "LCR Customer" },
        { 'value': 77, 'alias': "Relay IP Port" },
        { 'value': 79, 'alias': "Flexible Variable Group" },
        { 'value': 80, 'alias': "Flex Variable V1" },
        { 'value': 81, 'alias': "Flex Variable V2" },
        { 'value': 82, 'alias': "Flex Variable V3" },
        { 'value': 83, 'alias': "Flex Variable V4" },
        { 'value': 84, 'alias': "Flex Variable V5" },
        { 'value': 85, 'alias': "Flex Variable V6" },
        { 'value': 86, 'alias': "Flex Variable V7" },
        { 'value': 87, 'alias': "Flex Variable V8" },
        { 'value': 88, 'alias': "Flex Variable V9" },
        { 'value': 89, 'alias': "Flex Variable V10" },
        { 'value': 90, 'alias': "Flex Variable V11" },
        { 'value': 91, 'alias': "Flex Variable V12" },
        { 'value': 92, 'alias': "Flex Variable V13" },
        { 'value': 93, 'alias': "Flex Variable V14" },
        { 'value': 94, 'alias': "Flex Variable V15" },
        { 'value': 95, 'alias': "Flex Variable V16" },
        { 'value': 96, 'alias': "Regex Group" },
        { 'value': 97, 'alias': "Route URI Host Part" },
        { 'value': 98, 'alias': "Carrier Type" },
        { 'value': 100, 'alias': "Egress Trunk Group" },
        { 'value': 103, 'alias': "Ingress Trunk Group and Route Set" },
        { 'value': 104, 'alias': "Stir-Shaken Type(Request)" },
        { 'value': 105, 'alias': "Screening" },
        { 'value': 106, 'alias': "Stir-Shaken Verification Type(Response)" },
        { 'value': 107, 'alias': "Request URI Host Part" },
        { 'value': 108, 'alias': "Pai URI Host Part" },
        { 'value': 109, 'alias': "Called URI Host Part" },
        { 'value': 110, 'alias': "Tel Pai URI Calling" },
        { 'value': 0x7FFF, 'alias': "Trigger Criteria" },
        { 'value': 111, 'alias': "Scores" },
        { 'value': 112, 'alias': "Flags" },
        { 'value': 113, 'alias': "SIP Errors" },
        { 'value': 114, 'alias': "Stir-Shaken Signing Type(Response)" },
        { 'value': 115, 'alias': "Stir-Shaken Tagging Type(Response)" },
        { 'value': 116, 'alias': "STI Errors" },
        { 'value': 117, 'alias': "CPE Group" },
        { 'value': 118, 'alias': "Call Type" },
        { 'value': 119, 'alias': "Partition" },
        { 'value': 120, 'alias': "Static Parameters" },
        { 'value': 121, 'alias': "Rest Variable" },
        { 'value': 122, 'alias': "Rest Response Variable 1" },
        { 'value': 123, 'alias': "Rest Response Variable 2" },
        { 'value': 124, 'alias': "Rest Response Variable 3" },
        { 'value': 125, 'alias': "Rest Response Variable 4" },
        { 'value': 126, 'alias': "Rest Response Variable 5" },
        { 'value': 127, 'alias': "Rest Response Variable 6" },
        { 'value': 128, 'alias': "Rest Response Variable 7" },
        { 'value': 129, 'alias': "Rest Response Variable 8" },
        { 'value': 130, 'alias': "Rest Response Variable 9" },
        { 'value': 131, 'alias': "Rest Response Variable 10" },
        { 'value': 132, 'alias': "Rest Response Variable 11" },
        { 'value': 133, 'alias': "Rest Response Variable 12" },
        { 'value': 134, 'alias': "Rest Response Variable 13" },
        { 'value': 135, 'alias': "Rest Response Variable 14" },
        { 'value': 136, 'alias': "Rest Response Variable 15" },
        { 'value': 137, 'alias': "Rest Response Variable 16" },
        { 'value': 138, 'alias': "Rest Response Variable 17" },
        { 'value': 139, 'alias': "Rest Response Variable 18" },
        { 'value': 140, 'alias': "Rest Response Variable 19" },
        { 'value': 141, 'alias': "Rest Response Variable 20" },
        { 'value': 142, 'alias': "Rest Response Variable 21" },
        { 'value': 143, 'alias': "Rest Response Variable 22" },
        { 'value': 144, 'alias': "Rest Response Variable 23" },
        { 'value': 145, 'alias': "Rest Response Variable 24" },
        { 'value': 146, 'alias': "Rest Response Variable 25" },
        { 'value': 147, 'alias': "Rest Response Variable 26" },
        { 'value': 148, 'alias': "Rest Response Variable 27" },
        { 'value': 149, 'alias': "Rest Response Variable 28" },
        { 'value': 150, 'alias': "Rest Response Variable 29" },
        { 'value': 151, 'alias': "Rest Response Variable 30" },
        { 'value': 152, 'alias': "Rest Response Variable 31" },
        { 'value': 153, 'alias': "Rest Response Variable 32" },
        { 'value': 154, 'alias': "Rest Service Errors" },
        { 'value': 155, 'alias': "Rest Application Errors" },
        { 'value': 156, 'alias': "Calling URI" },
        { 'value': 157, 'alias': "Called URI" },
        { 'value': 158, 'alias': "Sip PAI URI" },
        { 'value': 159, 'alias': "Sip From URI" },
        { 'value': 160, 'alias': "Sip To URI" },
        { 'value': 161, 'alias': "Request URI" },
        { 'value': 162, 'alias': "Sip Diversion URI" },
        { 'value': 163, 'alias': "Home Domain URI" },
        { 'value': 164, 'alias': "Route URI" },
        { 'value': 165, 'alias': "Sip Diversion Redirection URI" },
        { 'value': 166, 'alias': "Sip History Info Origination URI" },
        { 'value': 167, 'alias': "Sip History Info Redirection URI" },
        { 'value': 168, 'alias': "Emergency and HPC" },
        { 'value': 169, 'alias': "RPH Errors" },
        { 'value': 170, 'alias': "IP Protocol" },
        { 'value': 171, 'alias': "Use SIP in Core" },
        { 'value': 172, 'alias': "SIP Used in Core" },
        { 'value': 173, 'alias': "Enum Response Parameter Group" },
        { 'value': 174, 'alias': "Enum Response Parameter P1" },
        { 'value': 175, 'alias': "Enum Response Parameter P2" },
        { 'value': 176, 'alias': "Enum Response Parameter P3" },
        { 'value': 177, 'alias': "Enum Response Parameter P4" },
        { 'value': 178, 'alias': "Enum Response Parameter P5" },
        { 'value': 179, 'alias': "Enum Response Parameter P6" },
        { 'value': 180, 'alias': "Enum Response Parameter P7" },
        { 'value': 181, 'alias': "Enum Response Parameter P8" },
        { 'value': 182, 'alias': "Enum Response Parameter P9" },
        { 'value': 183, 'alias': "Enum Response Parameter P10" },
        { 'value': 184, 'alias': "Enum Response Parameter P11" },
        { 'value': 185, 'alias': "Enum Response Parameter P12" },
        { 'value': 186, 'alias': "Enum Response Parameter P13" },
        { 'value': 187, 'alias': "Enum Response Parameter P14" },
        { 'value': 188, 'alias': "Enum Response Parameter P15" },
        { 'value': 189, 'alias': "Enum Response Parameter P16" },
        { 'value': 190, 'alias': "Called Number WithOut CC" },
        { 'value': 191, 'alias': "Called Number" },
        { 'value': 192, 'alias': "Called Country Code" },
        { 'value': 193, 'alias': "Calling Number WithOut CC" },
        { 'value': 194, 'alias': "Calling Country Code" },
        { 'value': 195, 'alias': "Dialed Called Number WithOut CC" },
        { 'value': 196, 'alias': "Dialed Called Number" },
        { 'value': 197, 'alias': "Ingress TG Carrier Code" },
        { 'value': 198, 'alias': "Ingress Carrier's Context Info" },
        { 'value': 199, 'alias': "STI Service Type" },
        { 'value': 200, 'alias': "STI Service Status" },
        { 'value': 201, 'alias': "DTG" },
        { 'value': 202, 'alias': "Dest Trunkgroup" },
        { 'value': 203, 'alias': "Dest TrunkGroup Trunk-Context" },
        { 'value': 204, 'alias': "Ingress Call Service Type" },
        { 'value': 205, 'alias': "Ingress Call Service Tag" },
        { 'value': 206, 'alias': "Request URI User Part" },
        { 'value': 207, 'alias': "Trunk Group's Context Info" },
        { 'value': 208, 'alias': "STI Profile's Context Info" },
        { 'value': 209, 'alias': "Partition's Context Info" },
        { 'value': 210, 'alias': "Gateway's Context Info" },
        { 'value': 211, 'alias': "Trunk Group Domain's Context Info" },
        { 'value': 212, 'alias': "TG with Calling Number Modified: for 15.1.3" },
        { 'value': 213, 'alias': "TG with GN Additional Calling Modified: for 15.1.3" },
        { 'value': 214, 'alias': "GN Additional Calling Modified: for 15.1.3" },
        { 'value': 215, 'alias': "3ggpFCP" },
        { 'value': 216, 'alias': "Rest Response" },
        { 'value': 217, 'alias': "Identity Header (shaken)" },
        { 'value': 218, 'alias': "GETS Call" },
        { 'value': 219, 'alias': "Verstat" },
        { 'value': 220, 'alias': "P-Origination Id" },
        { 'value': 221, 'alias': "Attestation Indicator" },
        { 'value': 222, 'alias': "ppt" },
        { 'value': 223, 'alias': "Identity Header Type" },
        { 'value': 224, 'alias': "STI Display Name" },
        { 'value': 225, 'alias': "RPH Verstat" },
        { 'value': 226, 'alias': "Visitor MA" },
        { 'value': 227, 'alias': "Calling Number" },
        { 'value': 228, 'alias': "Customer Identifier" },
        { 'value': 229, 'alias': "Called Number WithOut CC" },
        { 'value': 230, 'alias': "Called Number" },
        { 'value': 231, 'alias': "Called Country Code" },
        { 'value': 232, 'alias': "Calling Number WithOut CC" },
        { 'value': 233, 'alias': "Calling Country Code" },
        { 'value': 234, 'alias': "Dialed Called Number WithOut CC" },
        { 'value': 235, 'alias': "Dialed Called Number" },
        { 'value': 236, 'alias': "Ingress TG Carrier Code" },
        { 'value': 237, 'alias': "Ingress Carrier's Context Info" },
        { 'value': 238, 'alias': "Trunk Group's Context Info" },
        { 'value': 239, 'alias': "STI Profile's Context Info" },
        { 'value': 240, 'alias': "Partition's Context Info" },
        { 'value': 241, 'alias': "e164 calling number" },
        { 'value': 242, 'alias': "e164 called number" },
        { 'value': 244, 'alias': "Logo" },
        { 'value': 245, 'alias': "Scores(Signing)" },
        { 'value': 246, 'alias': "Calling RN Modified: for 15.1.2" },
        { 'value': 247, 'alias': "Enum Service Status Modified: for 15.1.2" },
        { 'value': 248, 'alias': "Number Translation Service Status Modified: for 15.1.3" },
        { 'value': 249, 'alias': "Enterprise id Modified: for 15.1.2" },
        { 'value': 250, 'alias': "Sip PAI URI User Modified: for 15.1.2" },
        { 'value': 251, 'alias': "Sip From URI User Modified: for 15.1.2" },
        { 'value': 252, 'alias': "Ingress CallInfoHeader-ICN Modified: for 15.1.2" },
        { 'value': 253, 'alias': "Ingress CallInfoHeader-JCL Modified: for 15.1.2" },
        { 'value': 254, 'alias': "Ingress CallInfoHeader-JCD Modified: for 15.1.2" },
        { 'value': 255, 'alias': "Ingress CallInfoHeader-CRN Modified: for 15.1.2" },
        { 'value': 256, 'alias': "Ingress STI PAI DN Modified: for 15.1.2" },
        { 'value': 257, 'alias': "Ingress STI FROM DN Modified: for 15.1.2" },
        { 'value': 258, 'alias': "Ingress CallInfoHeader Modified: for 15.1.2" },
        { 'value': 259, 'alias': "ATI VLR Number Modified: for 15.1.3" },
        { 'value': 260, 'alias': "ATI Subscriber State Modified: for 15.1.3" },
        { 'value': 261, 'alias': "ATI Calling GT Number Modified: for 15.1.3" },
        { 'value': 262, 'alias': "ATI Errors Modified: for 15.1.3" }    
    ]

###
###  Packet Service Profile
###
class PSP:
    def __init__(self, profileName):
        self.profileName = profileName
        self.profileID = 'packetServiceProfileId'
        self.keysToIgnore = ['packetServiceProfileId']
        self.SOAPrequest = """
    <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:intf="http://www.sonusnet.com/ems/emsapi/psx/{psxVersion}/intf">
          <soapenv:Header>
          <USER soapenv:mustUnderstand="0" xsi:type="xsd:string">{user}</USER><PASSWORD soapenv:mustUnderstand="0" xsi:type="xsd:string">{password}</PASSWORD>
       </soapenv:Header>
       <soapenv:Body>
          <intf:retrieve soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
             <in0 xsi:type="xsd:string">{psxName}</in0>
             <in1 xsi:type="mod:PacketServiceProfileKey" xmlns:mod="http://www.sonusnet.com/ems/emsapi/psx/{psxVersion}/model">  
             <packetServiceProfileId xsi:type="xsd:string">{profName}</packetServiceProfileId>
             </in1>
          </intf:retrieve>
       </soapenv:Body>
    </soapenv:Envelope>
    """
        self.profAttributes = [
            { 'key': 'packetServiceProfileId', 'value': '' },
            { 'key': 'rtpDataPayloadType', 'value': '' },
            { 'key': 'silenceFactor', 'value': '' },
            { 'key': 'typeOfService', 'value': '' },
            { 'key': 'options', 'value': '' },
            { 'key': 'voiceInitPlayoutDelay', 'value': '' },
            { 'key': 'dataInitPlayoutDelay', 'value': '' },
            { 'key': 'packetLossThreshold', 'value': '' },
            { 'key': 'packetLossRrbandwidth', 'value': '' },
            { 'key': 'packetLossRsbandwidth', 'value': '' },
            { 'key': 'rtpOptions', 'value': '' },
            { 'key': 'g711SIDRTPPayloadType', 'value': '' },
            { 'key': 'aal1PayloadSize', 'value': '' },
            { 'key': 'codecEntry1Id', 'value': '' },
            { 'key': 'codecEntry2Id', 'value': '' },
            { 'key': 'codecEntry3Id', 'value': '' },
            { 'key': 'codecEntry4Id', 'value': '' },
            { 'key': 'attributes', 'value': '' },
            { 'key': 'attributes2', 'value': '' },
            { 'key': 'attributes3', 'value': '' },
            { 'key': 'attributes4', 'value': '' },
            { 'key': 'p2pCtrlOptionType', 'value': '' },
            { 'key': 'p2pCtrlOptionRtpFlow', 'value': '' },
            { 'key': 'dataCallPacketSize', 'value': '' },
            { 'key': 'rfc2833RtpPayloadType', 'value': '' },
            { 'key': 'securityCryptoSuiteId', 'value': '' },
            { 'key': 'videoMaxBw', 'value': '' },
            { 'key': 'videoBwReductionRate', 'value': '' },
            { 'key': 'videoFailAudioOnly', 'value': '' },
            { 'key': 'mediaPacketCos', 'value': '' },
            { 'key': 'packetMediaControlsh', 'value': '' },
            { 'key': 'codecEntry5Id', 'value': '' },
            { 'key': 'codecEntry6Id', 'value': '' },
            { 'key': 'codecEntry7Id', 'value': '' },
            { 'key': 'codecEntry8Id', 'value': '' },
            { 'key': 'codecEntry9Id', 'value': '' },
            { 'key': 'codecEntry10Id', 'value': '' },
            { 'key': 'codecEntry11Id', 'value': '' },
            { 'key': 'codecEntry12Id', 'value': '' },
            { 'key': 'ipv4Tos', 'value': '' },
            { 'key': 'ipv6TrafficClass', 'value': '' },
            { 'key': 'ieee8021QVlanCos', 'value': '' },
            { 'key': 'codecListProfile', 'value': '' },
            { 'key': 't38FaxExtAttributes', 'value': '' },
            { 'key': 'dtlsCryptoSuiteId', 'value': '' },
            { 'key': 'msrpDscp', 'value': '' },
            { 'key': 'genericAttributes1', 'value': '' },
            { 'key': 'genericAttributes2', 'value': '' },
            { 'key': 'genericAttributes3', 'value': '' },
            { 'key': 'genericAttributes4', 'value': '' },
            { 'key': 'pspPacketSize', 'value': '' },
            { 'key': 'pspBitRate', 'value': '' },
            { 'key': 'dtlsSctpDscp', 'value': '' },
            { 'key': 'voiceInitPlayoutDelay', 'value': '' },
            { 'key': 't140Dscp', 'value': '' },
            { 'key': 'monitoringProfileId', 'value': '' },
            { 'key': 'applicationDscp', 'value': '' },
            { 'key': 'maxNonRtpBandwidth', 'value': '' },
            { 'key': 'nonRtpTlsProfile', 'value': '' },
            { 'key': 'mediaPeerInactivityTimeout', 'value': '' }
        ]

    def genRequest(self, psxCfg):
        theRequest = self.SOAPrequest.format(user=psxCfg.getUser(), password=psxCfg.getPassword(), psxName=psxCfg.getName(), psxVersion=psxCfg.getVersion(), profName=self.profileName)
        return theRequest

    ###  PSP Enums

    options = [
        { 'mask': 0x0000, 'alias': 'None' },
        { 'mask': 0x0001, 'alias': 'Error Control Type - Redundancy' },
        { 'mask': 0x0002, 'alias': 'Error Control Type - Forward Error Correction' },
        { 'mask': 0x0004, 'alias': 'Low Speed Number of Redundant Packets - 1' },
        { 'mask': 0x0008, 'alias': 'Low Speed Number of Redundant Packets - 2' },
        { 'mask': 0x0020, 'alias': 'High Speed Number of Redundant Packets - 1' },
        { 'mask': 0x0040, 'alias': 'High Speed Number of Redundant Packets - 2' },
        { 'mask': 0x0100, 'alias': 'Maximum Bit Rate - 4.8 kbits/s' },
        { 'mask': 0x0200, 'alias': 'Maximum Bit Rate - 9.6 kbits/s' },
        { 'mask': 0x0300, 'alias': 'Maximum Bit Rate - 14.4 kbits/s' },
        { 'mask': 0x0800, 'alias': 'Data Rate Management Type 2 - Transfer of TCF' },
        { 'mask': 0x2000, 'alias': 'Send Silence Insertion Descriptor Heartbeat' },
        { 'mask': 0x4000, 'alias': 'Honor Remote Precedence Enabled' },
    ]

    rtpOptions = [
        { 'mask': 0x0000, 'alias': 'None' },
        { 'mask': 0x0001, 'alias': 'RTCP Peer Action - Trap' },
        { 'mask': 0x0003, 'alias': 'RTCP Peer Action - Trap And Disconnect' },
        { 'mask': 0x0004, 'alias': 'RTCP Packet Loss Action - Trap' },
        { 'mask': 0x0008, 'alias': 'RTCP Packet Loss Action - Trap And Adapt' },
        { 'mask': 0x000C, 'alias': 'RTCP Packet Loss Action - Trap And Disconnect' },
        { 'mask': 0x0010, 'alias': 'RTCP Enable' },
        { 'mask': 0x0040, 'alias': 'Enable RTCP Only For HELD Calls' },
        { 'mask': 0x0080, 'alias': 'RTCP Termination For Pass Through Calls' },
        { 'mask': 0x0200, 'alias': 'RTCP-MUX' },
        { 'mask': 0x1000, 'alias': 'Relay' },
        { 'mask': 0x2000, 'alias': 'Relay Or Terminate' },
        { 'mask': 0x4000, 'alias': 'generate RTCP for T140 media streams if not received from other leg' }
    ]

    attributes = [
        { 'mask': 0x00000000, 'alias': 'None' },
        { 'mask': 0x00000001, 'alias': 'Different Packet Size' },
        { 'mask': 0x00000002, 'alias': 'Different Silence Suppression' },
        { 'mask': 0x00000004, 'alias': 'Different DTMF Relay' },
        { 'mask': 0x00000008, 'alias': 'Send Enabled' },
        { 'mask': 0x00000010, 'alias': 'Receive Enabled' },
        { 'mask': 0x00000040, 'alias': 'Disallow Data Calls' },
        { 'mask': 0x00000080, 'alias': 'Use All Fields In Preferred Packet Service Profile' },
        { 'mask': 0x00000100, 'alias': 'Use Direct Media' },
        { 'mask': 0x00000200, 'alias': 'Honor Offer Preference' },
        { 'mask': 0x00000400, 'alias': 'Send Route PSP Precedence' },
        { 'mask': 0x00000800, 'alias': 'ECM Preferred' },
        { 'mask': 0x00001000, 'alias': 'Use Max Bit Rate Only' },
        { 'mask': 0x00002000, 'alias': 'Interworking DTMF OOB 2833 Without Transcoding' },
        { 'mask': 0x00004000, 'alias': 'Different 2833 Payload Type' },
        { 'mask': 0x00008000, 'alias': 'Honor Answer Preference' },
        { 'mask': 0x00010000, 'alias': 'FaxMaxDatagram Size without Redundancy' }
    ]

    attributes2 = [
        { 'mask': 0x0000, 'alias': 'None' },
        { 'mask': 0x0001, 'alias': 'Error Control Type - Redundancy' },
        { 'mask': 0x0002, 'alias': 'Error Control Type - Forward Error Correction' },
        { 'mask': 0x0004, 'alias': 'Low Speed Number of Redundant Packets - 1' },
        { 'mask': 0x0008, 'alias': 'Low Speed Number of Redundant Packets - 2' },
        { 'mask': 0x0020, 'alias': 'High Speed Number of Redundant Packets - 1' },
        { 'mask': 0x0040, 'alias': 'High Speed Number of Redundant Packets - 2' },
        { 'mask': 0x0100, 'alias': 'Maximum Bit Rate - 4.8 kbits/s' },
        { 'mask': 0x0200, 'alias': 'Maximum Bit Rate - 9.6 kbits/s' },
        { 'mask': 0x0300, 'alias': 'Maximum Bit Rate - 14.4 kbits/s' },
        { 'mask': 0x0800, 'alias': 'Data Rate Management Type 2 - Transfer of TCF' },
        { 'mask': 0x2000, 'alias': 'Send Silence Insertion Descriptor Heartbeat' },
        { 'mask': 0x4000, 'alias': 'Honor Remote Precedence Enabled' }
    ]

    attributes3 = [
        { 'mask': 0x00000000, 'alias': 'None' },
        { 'mask': 0x20000001, 'alias': 'Enable SRTP' },
        { 'mask': 0x20000002, 'alias': 'Allow Fallback' },
        { 'mask': 0x20000004, 'alias': 'Validate Peer Support for DTMF Events' },
        { 'mask': 0x20000008, 'alias': 'Apply Fax Tone Treatment' },
        { 'mask': 0x20000010, 'alias': 'SSRC Randomize' },
        { 'mask': 0x20000020, 'alias': 'DSCP Passthrough' },
        { 'mask': 0x20000040, 'alias': 'Update Crypto On Modify' },
        { 'mask': 0x20000080, 'alias': 'Reset the SRTP Roll Over Counter with respect to session key change' },
        { 'mask': 0x20000100, 'alias': 'Reset Roll Over Counter for both encryption and decryption when decryption key changes' },
        { 'mask': 0x20000200, 'alias': 'Media Lock Down For PassThrough' },
        { 'mask': 0x20000400, 'alias': 'DTLS Enable SRTP' },
        { 'mask': 0x20000800, 'alias': 'DTLS Allow Fallback' },
        { 'mask': 0x20001000, 'alias': 'HDCodec Preferred' },
        { 'mask': 0x20002000, 'alias': 'PreferNb Passthru Over HDTranscode' },
        { 'mask': 0x20004000, 'alias': 'Match Offered Codec Group IfNb Only' },
        { 'mask': 0x20008000, 'alias': 'Force Route PSP Order' },
        { 'mask': 0x20010000, 'alias': 'SRTP Relay' },
        { 'mask': 0x20020000, 'alias': 'SCTP Relay' },
        { 'mask': 0x20040000, 'alias': 'Allow Pass Through' },
        { 'mask': 0x20080000, 'alias': 'Reserve Bandwidth' },
        { 'mask': 0x20100000, 'alias': 'Police on heaviest codec' },
        { 'mask': 0x20200000, 'alias': 't140Call' },
        { 'mask': 0x20400000, 'alias': 'Own Codec EVS' },
        { 'mask': 0x20800000, 'alias': 'Peer Codec EVS' },
        { 'mask': 0x21000000, 'alias': 'Own Codec SILK8' },
        { 'mask': 0x22000000, 'alias': 'Peer Codec SILK8' },
        { 'mask': 0x24000000, 'alias': 'Own Codec SILK16' },
        { 'mask': 0x28000000, 'alias': 'Peer Codec SILK16' }
    ]

    attributes4 = [
        { 'mask': 0x00000000, 'alias': 'None' },
        { 'mask': 0x30000001, 'alias': 'Allow Audio Transcode For MultiStream Call' },
        { 'mask': 0x30000002, 'alias': 'Generate and Signal SSRC and CName' },
        { 'mask': 0x30000004, 'alias': 'Allow Mid Call SSRC Modification' },
        { 'mask': 0x30000008, 'alias': 'Vtp Support' },
        { 'mask': 0x30000010, 'alias': 'Always Send Timestamp' },
        { 'mask': 0x30000020, 'alias': 'Randomize the SSRC value for SRTP streams and applies to both pass-through and transcoded calls' },
        { 'mask': 0x30000100, 'alias': 'G711T140BaudotAdapt Modified: for 14.1.5' }
    ]

    p2pCtrlOptionType = [
        { 'mask': 0x0000, 'alias': 'Transcode Conditional' },
        { 'mask': 0x0001, 'alias': 'Transcode Always' },
        { 'mask': 0x0003, 'alias': 'Transcoder-Free-Transparency' }
    ]

    p2pCtrlOptionTypeRtpFlow = [
        { 'mask': 0x0000, 'alias': 'Pass Thru GSX' },
        { 'mask': 0x0001, 'alias': 'External Direct Media' }
    ]

    t38FaxExtAttributes = [
        { 'value': 0, 'alias': 'T.38(v0)' },
        { 'value': 1, 'alias': 'T.38(v3)' },
    ]

###
###  Routing Criteria 
###
class RoutingCriteria:
    def __init__(self, profileName):
        self.profileName = profileName
        self.profileID = 'rcProfileId'
        self.keysToIgnore = ['rcProfileId']
        self.SOAPrequest = """
    <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:intf="http://www.sonusnet.com/ems/emsapi/psx/{psxVersion}/intf">
          <soapenv:Header>
          <USER soapenv:mustUnderstand="0" xsi:type="xsd:string">{user}</USER><PASSWORD soapenv:mustUnderstand="0" xsi:type="xsd:string">{password}</PASSWORD>
       </soapenv:Header>
       <soapenv:Body>
          <intf:retrieve soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
             <in0 xsi:type="xsd:string">{psxName}</in0>
             <in1 xsi:type="mod:RoutingCriteriaKey" xmlns:mod="http://www.sonusnet.com/ems/emsapi/psx/{psxVersion}/model">             
             <rcProfileId xsi:type="xsd:string">{profName}</rcProfileId>
             <cpCalltype xsi:type="xsd:int">0</cpCalltype>
             <cpPriority xsi:type="xsd:short">1</cpPriority>
             </in1>
          </intf:retrieve>
       </soapenv:Body>
    </soapenv:Envelope>
    """
        self.profAttributes = [
            { 'key': 'rcProfileId', 'value': '' },
            { 'key': 'cpCalltype', 'value': '' },
            { 'key': 'cpPriority', 'value': '' },
            { 'key': 'cpOnOffNet', 'value': '' },
            { 'key': 'cpTollIndication', 'value': '' },
            { 'key': 'serviceCodeUseFlag', 'value': '' },
            { 'key': 'calltypeUseFlag', 'value': '' },
            { 'key': 'destinationUseFlag', 'value': '' },
            { 'key': 'partitionUseFlag', 'value': '' }
        ]

    def genRequest(self, psxCfg):
        theRequest = self.SOAPrequest.format(user=psxCfg.getUser(), password=psxCfg.getPassword(), psxName=psxCfg.getName(), psxVersion=psxCfg.getVersion(), profName=self.profileName)
        return theRequest

    ###  Routing Criteria Enums

    cpCallType = [
        { 'value': 0, 'alias': "Private" },
        { 'value': 1, 'alias': "200" },
        { 'value': 2, 'alias': "300" },
        { 'value': 3, 'alias': "400" },
        { 'value': 4, 'alias': "500" },
        { 'value': 5, 'alias': "700" },
        { 'value': 6, 'alias': "8XX" },
        { 'value': 7, 'alias': "900" },
        { 'value': 8, 'alias': "National Operator" },
        { 'value': 9, 'alias': "Local Operator" },
        { 'value': 10, 'alias': "National, All 1+ (Both Inter LATA And Intra LATA)" },
        { 'value': 11, 'alias': "International, 011" },
        { 'value': 12, 'alias': "International Operator, 01" },
        { 'value': 13, 'alias': "Long Distance Operator, 00" },
        { 'value': 14, 'alias': "Inter LATA 1+" },
        { 'value': 15, 'alias': "Intra LATA 1+" },
        { 'value': 16, 'alias': "Internet Offload Premium" },
        { 'value': 17, 'alias': "Internet Offload Standard" },
        { 'value': 18, 'alias': "Internet Offload Economy" },
        { 'value': 19, 'alias': "Internet Offload Anonymous" },
        { 'value': 20, 'alias': "IP VPN Service" },
        { 'value': 21, 'alias': "Test" },
        { 'value': 22, 'alias': "Internet Reception Service" },
        { 'value': 23, 'alias': "Transit" },
        { 'value': 24, 'alias': "Other Carrier Chosen" },
        { 'value': 25, 'alias': "Carrier Cut Through" },
        { 'value': 26, 'alias': "User Name" },
        { 'value': 27, 'alias': "Switch ID Trunk Group ID" },
        { 'value': 28, 'alias': "Mobile" },
        { 'value': 29, 'alias': "Inter LATA 0+" },
        { 'value': 30, 'alias': "Intra LATA 0+" },
        { 'value': 200, 'alias': "All" }
    ]

    cpOnOffNet = [
        { 'value': 0, 'alias': "All" },
        { 'value': 1, 'alias': "On Network" },
        { 'value': 2, 'alias': "Off Network" }
    ]

    cpTollIndication = [
        { 'mask': 0x01, 'alias': "Inter LATA Toll" },
        { 'mask': 0x02, 'alias': "Inter LATA Local" },
        { 'mask': 0x04, 'alias': "Intra LATA Local" },
        { 'mask': 0x08, 'alias': "Intra LATA Toll" },
        { 'mask': 0x10, 'alias': "International" },
        { 'mask': 0x20, 'alias': "Private" },
        { 'mask': 0x40, 'alias': "Unknown" },
        { 'mask': 0xff, 'alias': "All" }
    ]

    serviceCodeUseFlag = [
        { 'value': 1, 'alias': "True" },
        { 'value': 2, 'alias': "False" }
    ]

    destinationUseFlag = [
        { 'value': 1, 'alias': "True" },
        { 'value': 2, 'alias': "False" }
    ]

    partitionUseFlag = [
        { 'value': 1, 'alias': "True" },
        { 'value': 2, 'alias': "False" }
    ]


###
###  Trunk Group
###
class TrunkGroup:
    def __init__(self, profileName, gwName):
        self.profileName = profileName
        self.gwName = gwName
        self.profileID = 'trunkgroupId'
        self.keysToIgnore = ['trunkgroupId', 'trunkgroupId2', 'gatewayId']
        self.SOAPrequest = """
    <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:intf="http://www.sonusnet.com/ems/emsapi/psx/{psxVersion}/intf">
          <soapenv:Header>
          <USER soapenv:mustUnderstand="0" xsi:type="xsd:string">{user}</USER><PASSWORD soapenv:mustUnderstand="0" xsi:type="xsd:string">{password}</PASSWORD>
       </soapenv:Header>
       <soapenv:Body>
          <intf:retrieve soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
             <in0 xsi:type="xsd:string">{psxName}</in0>
             <in1 xsi:type="mod:TrunkgroupKey" xmlns:mod="http://www.sonusnet.com/ems/emsapi/psx/{psxVersion}/model">
                 <trunkgroupId xsi:type="xsd:string">{profileName}</trunkgroupId>
                 <gatewayId xsi:type="xsd:string">{gwName}</gatewayId>
             </in1>
          </intf:retrieve>
       </soapenv:Body>
    </soapenv:Envelope>
    """

        self.profAttributes = [
            { 'key': 'addressContext', 'value': '' },
            { 'key': 'autogenporig', 'value': '' },
            { 'key': 'autoRecallProfileId', 'value': '' },
            { 'key': 'beepToneProfileId', 'value': '' },
            { 'key': 'billingId', 'value': '' },
            { 'key': 'billingInfoId', 'value': '' },
            { 'key': 'billingNoa', 'value': '' },
            { 'key': 'billingNpi', 'value': '' },
            { 'key': 'billingPlanId', 'value': '' },
            { 'key': 'businessGroupId', 'value': '' },
            { 'key': 'businessLocationId', 'value': '' },
            { 'key': 'callingArea', 'value': '' },
            { 'key': 'callingPartyId', 'value': '' },
            { 'key': 'callingPartyNoa', 'value': '' },
            { 'key': 'callingPartyNpi', 'value': '' },
            { 'key': 'carrierCodeConvrId', 'value': '' },
            { 'key': 'carrierId', 'value': '' },
            { 'key': 'carrierSelectionPriorityId', 'value': '' },
            { 'key': 'carrierTypeProfileId', 'value': '' },
            { 'key': 'chargeBandProfileId', 'value': '' },
            { 'key': 'chargeIndicator', 'value': '' },
            { 'key': 'chargeInformationId', 'value': '' },
            { 'key': 'classOfServiceId', 'value': '' },
            { 'key': 'contextInfo', 'value': '' },
            { 'key': 'countryId', 'value': '' },
            { 'key': 'cpnPresentation', 'value': '' },
            { 'key': 'cpnScreening', 'value': '' },
            { 'key': 'ddiRangeProfileId', 'value': '' },
            { 'key': 'defaultCPC', 'value': '' },
            { 'key': 'defaultOlip', 'value': '' },
            { 'key': 'description', 'value': '' },
            { 'key': 'destinationSwitchType', 'value': '' },
            { 'key': 'deviceProfileId', 'value': '' },
            { 'key': 'dfltCpnPresentation', 'value': '' },
            { 'key': 'dialPlanProfileId', 'value': '' },
            { 'key': 'digitLenEnfProfileId', 'value': '' },
            { 'key': 'direction', 'value': '' },
            { 'key': 'egressChargeInd', 'value': '' },
            { 'key': 'egressPolicyProfileId', 'value': '' },
            { 'key': 'elementAttributes', 'value': '' },
            { 'key': 'enumDomainProfileId', 'value': '' },
            { 'key': 'erpProfileId', 'value': '' },
            { 'key': 'exceptionProfileId', 'value': '' },
            { 'key': 'featureControlProfileId', 'value': '' },
            { 'key': 'flexibleVariableRuleId', 'value': '' },
            { 'key': 'forcedOlip', 'value': '' },
            { 'key': 'gatewayId', 'value': '' },
            { 'key': 'genericRuleEg', 'value': '' },
            { 'key': 'genericRuleProfileId', 'value': '' },
            { 'key': 'infoTransCapProfileId', 'value': '' },
            { 'key': 'inPmRuleId', 'value': '' },
            { 'key': 'ipSignalingPeerGroupId', 'value': '' },
            { 'key': 'ipSignalingProfileId', 'value': '' },
            { 'key': 'ipspGenericProfilesId', 'value': '' },
            { 'key': 'iptgIpSignalingProfileId', 'value': '' },
            { 'key': 'iptgPacketServiceProfileId', 'value': '' },
            { 'key': 'isEscaped', 'value': '' },
            { 'key': 'jipId', 'value': '' },
            { 'key': 'lataId', 'value': '' },
            { 'key': 'localizationType', 'value': '' },
            { 'key': 'localRingBackToneId', 'value': '' },
            { 'key': 'maximumSatelliteHops', 'value': '' },
            { 'key': 'networkdataNet', 'value': '' },
            { 'key': 'networkdataPartition', 'value': '' },
            { 'key': 'nextHopDomainId', 'value': '' },
            { 'key': 'npaId', 'value': '' },
            { 'key': 'npiId', 'value': '' },
            { 'key': 'numberAnalysisProfileId', 'value': '' },
            { 'key': 'numberingPlanId', 'value': '' },
            { 'key': 'originatingCarrier', 'value': '' },
            { 'key': 'origNetworkId', 'value': '' },
            { 'key': 'outPmRuleId', 'value': '' },
            { 'key': 'overrideCarrierId', 'value': '' },
            { 'key': 'ownCarrierId', 'value': '' },
            { 'key': 'owningCarrierProfileId', 'value': '' },
            { 'key': 'poiChargeArea', 'value': '' },
            { 'key': 'poiLevel', 'value': '' },
            { 'key': 'policyProfileGroupId', 'value': '' },
            { 'key': 'pOriginationId', 'value': '' },
            { 'key': 'pprProfileId', 'value': '' },
            { 'key': 'prefixProfileId', 'value': '' },
            { 'key': 'prfdPacketServiceProfileId', 'value': '' },
            { 'key': 'pseudoCarrierId', 'value': '' },
            { 'key': 'rcProfileId', 'value': '' },
            { 'key': 'regionId', 'value': '' },
            { 'key': 'releaseToneTreatment', 'value': '' },
            { 'key': 'remoteSipPeerType', 'value': '' },
            { 'key': 'scpBusinessServiceGroup', 'value': '' },
            { 'key': 'serDetectPolicyProfileGroupId', 'value': '' },
            { 'key': 'signalingFlag', 'value': '' },
            { 'key': 'signalingProfileId', 'value': '' },
            { 'key': 'sipDomainId', 'value': '' },
            { 'key': 'sipLrProfileId', 'value': '' },
            { 'key': 'sipRcProfileId', 'value': '' },
            { 'key': 'sipUsedInCoreEgressIpspProfileId', 'value': '' },
            { 'key': 'sipUsedInCoreInterGatewayIpspProfileId', 'value': '' },
            { 'key': 'stiGenericProfileId', 'value': '' },
            { 'key': 'stiProfileId', 'value': '' },
            { 'key': 'tdmTrunkType', 'value': '' },
            { 'key': 'tgCos', 'value': '' },
            { 'key': 'tgCosProfileId', 'value': '' },
            { 'key': 'tgRuriHost', 'value': '' },
            { 'key': 'tgRuriHostPort', 'value': '' },
            { 'key': 'tgTrunkContext', 'value': '' },
            { 'key': 'trafficTrunkResvLevel1', 'value': '' },
            { 'key': 'trafficTrunkResvLevel2', 'value': '' },
            { 'key': 'trunkgroupDomainId', 'value': '' },
            { 'key': 'trunkgroupId', 'value': '' },
            { 'key': 'trunkgroupId2', 'value': '' },
            { 'key': 'trunkgroupVersionPreference', 'value': '' },
            { 'key': 'unrestrictedFlag', 'value': '' },
            { 'key': 'useSipInCoreEgressIpspProfileId', 'value': '' },
            { 'key': 'useSipInCoreInterGatewayIpspProfileId', 'value': '' },
            { 'key': 'zoneIndexProfileId', 'value': '' },
            { 'key': 'zzProfileId', 'value': '' }    
        ]

    def genRequest(self, psxCfg):
        theRequest = self.SOAPrequest.format(user=psxCfg.getUser(), password=psxCfg.getPassword(), psxName=psxCfg.getName(), psxVersion=psxCfg.getVersion(), profileName=self.profileName, gwName=self.gwName)
        return theRequest

    ###  Trunk Group Enums

    destinationSwitchType = [
        { 'value': 1, 'alias': "Access" },
        { 'value': 2, 'alias': "Tandem" },
        { 'value': 3, 'alias': "EAEO" },
        { 'value': 4, 'alias': "Non EAEO" },
        { 'value': 5, 'alias': "IXC" },
        { 'value': 6, 'alias': "International" } 
    ]

    direction = [
        { 'value': 0, 'alias': "Unknown" },
        { 'value': 1, 'alias': "Two Way" },
        { 'value': 2, 'alias': "One Way In" },
        { 'value': 3, 'alias': "One Way Out" }
    ]

    unrestrictedFlag = [
        { 'value': 0, 'alias': "Restricted (56 kbps)" },
        { 'value': 1, 'alias': "Unrestricted (64 kbps)" }        
    ]

    signalingFlag = [
        { 'value': 0, 'alias': "Unknown" },
        { 'value': 1, 'alias': "GR394 ISUP" },
        { 'value': 2, 'alias': "GR317 ISUP" },
        { 'value': 3, 'alias': "ISDN PRI" },
        { 'value': 4, 'alias': "CAS" },
        { 'value': 5, 'alias': "ITU" },
        { 'value': 6, 'alias': "Japan ISUP" },
        { 'value': 7, 'alias': "Other" },
        { 'value': 8, 'alias': "SIP" },
        { 'value': 9, 'alias': "H.323" },
        { 'value': 10, 'alias': "BT IUP" },
        { 'value': 11, 'alias': "R2" },
        { 'value': 12, 'alias': "China ISUP" },
        { 'value': 13, 'alias': "SIP-I" },
        { 'value': 14, 'alias': "Wireless" },
        { 'value': 15, 'alias': "BICC" }        
    ]

    callingPartyNoa = [
        { 'value': 0, 'alias': "None" },
        { 'value': 1, 'alias': "Pass Through" },
        { 'value': 2, 'alias': "Subscriber" },
        { 'value': 3, 'alias': "National" },
        { 'value': 4, 'alias': "International" },
        { 'value': 5, 'alias': "Network Specific" },
        { 'value': 6, 'alias': "Unknown" },
        { 'value': 7, 'alias': "Subscriber Operator" },
        { 'value': 8, 'alias': "National Operator" },
        { 'value': 9, 'alias': "International Operator" },
        { 'value': 10, 'alias': "Test Code" },
        { 'value': 11, 'alias': "No Number Operator Requested" },
        { 'value': 12, 'alias': "No Number Cut Through" },
        { 'value': 13, 'alias': "950" },
        { 'value': 20, 'alias': "SS7 Reserved" },
        { 'value': 28, 'alias': "NRN ConCatenated With DN" },
        { 'value': 126, 'alias': "Partial Calling Line ID" },
        { 'value': 128, 'alias': "Spare 00" },
        { 'value': 129, 'alias': "Spare 01" },
        { 'value': 130, 'alias': "Spare 02" },
        { 'value': 131, 'alias': "Spare 03" },
        { 'value': 132, 'alias': "Spare 04" },
        { 'value': 133, 'alias': "Spare 05" },
        { 'value': 134, 'alias': "Spare 06" },
        { 'value': 135, 'alias': "Spare 07" },
        { 'value': 136, 'alias': "Spare 08" },
        { 'value': 137, 'alias': "Spare 09" },
        { 'value': 138, 'alias': "Spare 0A" },
        { 'value': 139, 'alias': "Spare 0B" },
        { 'value': 140, 'alias': "Spare 0C" },
        { 'value': 141, 'alias': "Spare 0D" },
        { 'value': 142, 'alias': "Spare 0E" },
        { 'value': 143, 'alias': "Spare 0F" },
        { 'value': 144, 'alias': "Spare 10" },
        { 'value': 145, 'alias': "Spare 11" },
        { 'value': 146, 'alias': "Spare 12" },
        { 'value': 147, 'alias': "Spare 13" },
        { 'value': 148, 'alias': "Spare 14" },
        { 'value': 149, 'alias': "Spare 15" },
        { 'value': 150, 'alias': "Spare 16" },
        { 'value': 151, 'alias': "Spare 17" },
        { 'value': 152, 'alias': "Spare 18" },
        { 'value': 153, 'alias': "Spare 19" },
        { 'value': 154, 'alias': "Spare 1A" },
        { 'value': 155, 'alias': "Spare 1B" },
        { 'value': 156, 'alias': "Spare 1C" },
        { 'value': 157, 'alias': "Spare 1D" },
        { 'value': 158, 'alias': "Spare 1E" },
        { 'value': 159, 'alias': "Spare 1F" },
        { 'value': 160, 'alias': "Spare 20" },
        { 'value': 161, 'alias': "Spare 21" },
        { 'value': 162, 'alias': "Spare 22" },
        { 'value': 163, 'alias': "Spare 23" },
        { 'value': 164, 'alias': "Spare 24" },
        { 'value': 165, 'alias': "Spare 25" },
        { 'value': 166, 'alias': "Spare 26" },
        { 'value': 167, 'alias': "Spare 27" },
        { 'value': 168, 'alias': "Spare 28" },
        { 'value': 169, 'alias': "Spare 29" },
        { 'value': 170, 'alias': "Spare 2A" },
        { 'value': 171, 'alias': "Spare 2B" },
        { 'value': 172, 'alias': "Spare 2C" },
        { 'value': 173, 'alias': "Spare 2D" },
        { 'value': 174, 'alias': "Spare 2E" },
        { 'value': 175, 'alias': "Spare 2F" },
        { 'value': 176, 'alias': "Spare 30" },
        { 'value': 177, 'alias': "Spare 31" },
        { 'value': 178, 'alias': "Spare 32" },
        { 'value': 179, 'alias': "Spare 33" },
        { 'value': 180, 'alias': "Spare 34" },
        { 'value': 181, 'alias': "Spare 35" },
        { 'value': 182, 'alias': "Spare 36" },
        { 'value': 183, 'alias': "Spare 37" },
        { 'value': 184, 'alias': "Spare 38" },
        { 'value': 185, 'alias': "Spare 39" },
        { 'value': 186, 'alias': "Spare 3A" },
        { 'value': 187, 'alias': "Spare 3B" },
        { 'value': 188, 'alias': "Spare 3C" },
        { 'value': 189, 'alias': "Spare 3D" },
        { 'value': 190, 'alias': "Spare 3E" },
        { 'value': 191, 'alias': "Spare 3F" },
        { 'value': 192, 'alias': "Spare 40" },
        { 'value': 193, 'alias': "Spare 41" },
        { 'value': 194, 'alias': "Spare 42" },
        { 'value': 195, 'alias': "Spare 43" },
        { 'value': 196, 'alias': "Spare 44" },
        { 'value': 197, 'alias': "Spare 45" },
        { 'value': 198, 'alias': "Spare 46" },
        { 'value': 199, 'alias': "Spare 47" },
        { 'value': 200, 'alias': "Spare 48" },
        { 'value': 201, 'alias': "Spare 49" },
        { 'value': 202, 'alias': "Spare 4A" },
        { 'value': 203, 'alias': "Spare 4B" },
        { 'value': 204, 'alias': "Spare 4C" },
        { 'value': 205, 'alias': "Spare 4D" },
        { 'value': 206, 'alias': "Spare 4E" },
        { 'value': 207, 'alias': "Spare 4F" },
        { 'value': 208, 'alias': "Spare 50" },
        { 'value': 209, 'alias': "Spare 51" },
        { 'value': 210, 'alias': "Spare 52" },
        { 'value': 211, 'alias': "Spare 53" },
        { 'value': 212, 'alias': "Spare 54" },
        { 'value': 213, 'alias': "Spare 55" },
        { 'value': 214, 'alias': "Spare 56" },
        { 'value': 215, 'alias': "Spare 57" },
        { 'value': 216, 'alias': "Spare 58" },
        { 'value': 217, 'alias': "Spare 59" },
        { 'value': 218, 'alias': "Spare 5A" },
        { 'value': 219, 'alias': "Spare 5B" },
        { 'value': 220, 'alias': "Spare 5C" },
        { 'value': 221, 'alias': "Spare 5D" },
        { 'value': 222, 'alias': "Spare 5E" },
        { 'value': 223, 'alias': "Spare 5F" },
        { 'value': 224, 'alias': "Spare 60" },
        { 'value': 225, 'alias': "Spare 61" },
        { 'value': 226, 'alias': "Spare 62" },
        { 'value': 227, 'alias': "Spare 63" },
        { 'value': 228, 'alias': "Spare 64" },
        { 'value': 229, 'alias': "Spare 65" },
        { 'value': 230, 'alias': "Spare 66" },
        { 'value': 231, 'alias': "Spare 67" },
        { 'value': 232, 'alias': "Spare 68" },
        { 'value': 233, 'alias': "Spare 69" },
        { 'value': 234, 'alias': "Spare 6A" },
        { 'value': 235, 'alias': "Spare 6B" },
        { 'value': 236, 'alias': "Spare 6C" },
        { 'value': 237, 'alias': "Spare 6D" },
        { 'value': 238, 'alias': "Spare 6E" },
        { 'value': 239, 'alias': "Spare 6F" },
        { 'value': 240, 'alias': "Spare 70" },
        { 'value': 241, 'alias': "Spare 71" },
        { 'value': 242, 'alias': "Spare 72" },
        { 'value': 243, 'alias': "Spare 73" },
        { 'value': 244, 'alias': "Spare 74" },
        { 'value': 245, 'alias': "Spare 75" },
        { 'value': 246, 'alias': "Spare 76" },
        { 'value': 247, 'alias': "Spare 77" },
        { 'value': 248, 'alias': "Spare 78" },
        { 'value': 249, 'alias': "Spare 79" },
        { 'value': 250, 'alias': "Spare 7A" },
        { 'value': 251, 'alias': "Spare 7B" },
        { 'value': 252, 'alias': "Spare 7C" },
        { 'value': 253, 'alias': "Spare 7D" },
        { 'value': 254, 'alias': "Spare 7E" },
        { 'value': 255, 'alias': "Spare 7F" }        
    ]

    callingPartyNpi = [
        { 'value': 0, 'alias': "None" },
        { 'value': 1, 'alias': "ISDN" },
        { 'value': 2, 'alias': "Data" },
        { 'value': 3, 'alias': "Telex" },
        { 'value': 4, 'alias': "Private" },
        { 'value': 6, 'alias': "Spare 0/Unknown" },
        { 'value': 7, 'alias': "Spare 1" },
        { 'value': 8, 'alias': "Spare 2" },
        { 'value': 9, 'alias': "Spare 3" },
        { 'value': 10, 'alias': "Spare 4" },
        { 'value': 11, 'alias': "Spare 5" },
        { 'value': 12, 'alias': "Spare 6" },
        { 'value': 13, 'alias': "Spare 7" }        
    ]

    billingNoa = [
        { 'value': 0, 'alias': "None" },
        { 'value': 1, 'alias': "Pass Through" },
        { 'value': 2, 'alias': "Subscriber" },
        { 'value': 3, 'alias': "National" },
        { 'value': 4, 'alias': "International" },
        { 'value': 5, 'alias': "Network Specific" },
        { 'value': 6, 'alias': "Unknown" },
        { 'value': 7, 'alias': "Subscriber Operator" },
        { 'value': 8, 'alias': "National Operator" },
        { 'value': 9, 'alias': "International Operator" },
        { 'value': 10, 'alias': "Test Code" },
        { 'value': 11, 'alias': "No Number Operator Requested" },
        { 'value': 12, 'alias': "No Number Cut Through" },
        { 'value': 13, 'alias': "950" },
        { 'value': 14, 'alias': "ANI Of Calling Party Subscriber Number" },
        { 'value': 15, 'alias': "ANI Not Available Or Not Provided" },
        { 'value': 16, 'alias': "ANI Of Calling Party National Number" },
        { 'value': 17, 'alias': "ANI Of Called Party Subscriber Number" },
        { 'value': 18, 'alias': "ANI Of Called Party No Number Present" },
        { 'value': 19, 'alias': "ANI Of Called Party National Number" },
        { 'value': 20, 'alias': "SS7 Reserved" },
        { 'value': 28, 'alias': "NRN ConCatenated With DN" },
        { 'value': 126, 'alias': "Partial Calling Line ID" },
        { 'value': 128, 'alias': "Spare 00" },
        { 'value': 129, 'alias': "Spare 01" },
        { 'value': 130, 'alias': "Spare 02" },
        { 'value': 131, 'alias': "Spare 03" },
        { 'value': 132, 'alias': "Spare 04" },
        { 'value': 133, 'alias': "Spare 05" },
        { 'value': 134, 'alias': "Spare 06" },
        { 'value': 135, 'alias': "Spare 07" },
        { 'value': 136, 'alias': "Spare 08" },
        { 'value': 137, 'alias': "Spare 09" },
        { 'value': 138, 'alias': "Spare 0A" },
        { 'value': 139, 'alias': "Spare 0B" },
        { 'value': 140, 'alias': "Spare 0C" },
        { 'value': 141, 'alias': "Spare 0D" },
        { 'value': 142, 'alias': "Spare 0E" },
        { 'value': 143, 'alias': "Spare 0F" },
        { 'value': 144, 'alias': "Spare 10" },
        { 'value': 145, 'alias': "Spare 11" },
        { 'value': 146, 'alias': "Spare 12" },
        { 'value': 147, 'alias': "Spare 13" },
        { 'value': 148, 'alias': "Spare 14" },
        { 'value': 149, 'alias': "Spare 15" },
        { 'value': 150, 'alias': "Spare 16" },
        { 'value': 151, 'alias': "Spare 17" },
        { 'value': 152, 'alias': "Spare 18" },
        { 'value': 153, 'alias': "Spare 19" },
        { 'value': 154, 'alias': "Spare 1A" },
        { 'value': 155, 'alias': "Spare 1B" },
        { 'value': 156, 'alias': "Spare 1C" },
        { 'value': 157, 'alias': "Spare 1D" },
        { 'value': 158, 'alias': "Spare 1E" },
        { 'value': 159, 'alias': "Spare 1F" },
        { 'value': 160, 'alias': "Spare 20" },
        { 'value': 161, 'alias': "Spare 21" },
        { 'value': 162, 'alias': "Spare 22" },
        { 'value': 163, 'alias': "Spare 23" },
        { 'value': 164, 'alias': "Spare 24" },
        { 'value': 165, 'alias': "Spare 25" },
        { 'value': 166, 'alias': "Spare 26" },
        { 'value': 167, 'alias': "Spare 27" },
        { 'value': 168, 'alias': "Spare 28" },
        { 'value': 169, 'alias': "Spare 29" },
        { 'value': 170, 'alias': "Spare 2A" },
        { 'value': 171, 'alias': "Spare 2B" },
        { 'value': 172, 'alias': "Spare 2C" },
        { 'value': 173, 'alias': "Spare 2D" },
        { 'value': 174, 'alias': "Spare 2E" },
        { 'value': 175, 'alias': "Spare 2F" },
        { 'value': 176, 'alias': "Spare 30" },
        { 'value': 177, 'alias': "Spare 31" },
        { 'value': 178, 'alias': "Spare 32" },
        { 'value': 179, 'alias': "Spare 33" },
        { 'value': 180, 'alias': "Spare 34" },
        { 'value': 181, 'alias': "Spare 35" },
        { 'value': 182, 'alias': "Spare 36" },
        { 'value': 183, 'alias': "Spare 37" },
        { 'value': 184, 'alias': "Spare 38" },
        { 'value': 185, 'alias': "Spare 39" },
        { 'value': 186, 'alias': "Spare 3A" },
        { 'value': 187, 'alias': "Spare 3B" },
        { 'value': 188, 'alias': "Spare 3C" },
        { 'value': 189, 'alias': "Spare 3D" },
        { 'value': 190, 'alias': "Spare 3E" },
        { 'value': 191, 'alias': "Spare 3F" },
        { 'value': 192, 'alias': "Spare 40" },
        { 'value': 193, 'alias': "Spare 41" },
        { 'value': 194, 'alias': "Spare 42" },
        { 'value': 195, 'alias': "Spare 43" },
        { 'value': 196, 'alias': "Spare 44" },
        { 'value': 197, 'alias': "Spare 45" },
        { 'value': 198, 'alias': "Spare 46" },
        { 'value': 199, 'alias': "Spare 47" },
        { 'value': 200, 'alias': "Spare 48" },
        { 'value': 201, 'alias': "Spare 49" },
        { 'value': 202, 'alias': "Spare 4A" },
        { 'value': 203, 'alias': "Spare 4B" },
        { 'value': 204, 'alias': "Spare 4C" },
        { 'value': 205, 'alias': "Spare 4D" },
        { 'value': 206, 'alias': "Spare 4E" },
        { 'value': 207, 'alias': "Spare 4F" },
        { 'value': 208, 'alias': "Spare 50" },
        { 'value': 209, 'alias': "Spare 51" },
        { 'value': 210, 'alias': "Spare 52" },
        { 'value': 211, 'alias': "Spare 53" },
        { 'value': 212, 'alias': "Spare 54" },
        { 'value': 213, 'alias': "Spare 55" },
        { 'value': 214, 'alias': "Spare 56" },
        { 'value': 215, 'alias': "Spare 57" },
        { 'value': 216, 'alias': "Spare 58" },
        { 'value': 217, 'alias': "Spare 59" },
        { 'value': 218, 'alias': "Spare 5A" },
        { 'value': 219, 'alias': "Spare 5B" },
        { 'value': 220, 'alias': "Spare 5C" },
        { 'value': 221, 'alias': "Spare 5D" },
        { 'value': 222, 'alias': "Spare 5E" },
        { 'value': 223, 'alias': "Spare 5F" },
        { 'value': 224, 'alias': "Spare 60" },
        { 'value': 225, 'alias': "Spare 61" },
        { 'value': 226, 'alias': "Spare 62" },
        { 'value': 227, 'alias': "Spare 63" },
        { 'value': 228, 'alias': "Spare 64" },
        { 'value': 229, 'alias': "Spare 65" },
        { 'value': 230, 'alias': "Spare 66" },
        { 'value': 231, 'alias': "Spare 67" },
        { 'value': 232, 'alias': "Spare 68" },
        { 'value': 233, 'alias': "Spare 69" },
        { 'value': 234, 'alias': "Spare 6A" },
        { 'value': 235, 'alias': "Spare 6B" },
        { 'value': 236, 'alias': "Spare 6C" },
        { 'value': 237, 'alias': "Spare 6D" },
        { 'value': 238, 'alias': "Spare 6E" },
        { 'value': 239, 'alias': "Spare 6F" },
        { 'value': 240, 'alias': "Spare 70" },
        { 'value': 241, 'alias': "Spare 71" },
        { 'value': 242, 'alias': "Spare 72" },
        { 'value': 243, 'alias': "Spare 73" },
        { 'value': 244, 'alias': "Spare 74" },
        { 'value': 245, 'alias': "Spare 75" },
        { 'value': 246, 'alias': "Spare 76" },
        { 'value': 247, 'alias': "Spare 77" },
        { 'value': 248, 'alias': "Spare 78" },
        { 'value': 249, 'alias': "Spare 79" },
        { 'value': 250, 'alias': "Spare 7A" },
        { 'value': 251, 'alias': "Spare 7B" },
        { 'value': 252, 'alias': "Spare 7C" },
        { 'value': 253, 'alias': "Spare 7D" },
        { 'value': 254, 'alias': "Spare 7E" },
        { 'value': 255, 'alias': "Spare 7F" }        
    ]

    billingNpi = [
        { 'value': 0, 'alias': "None" },
        { 'value': 1, 'alias': "ISDN" },
        { 'value': 2, 'alias': "Data" },
        { 'value': 3, 'alias': "Telex" },
        { 'value': 4, 'alias': "Private" },
        { 'value': 6, 'alias': "Spare 0/Unknown" },
        { 'value': 7, 'alias': "Spare 1" },
        { 'value': 8, 'alias': "Spare 2" },
        { 'value': 9, 'alias': "Spare 3" },
        { 'value': 10, 'alias': "Spare 4" },
        { 'value': 11, 'alias': "Spare 5" },
        { 'value': 12, 'alias': "Spare 6" },
        { 'value': 13, 'alias': "Spare 7" }        
    ]

    elementAttributes = [
        { 'mask': 0x00000001, 'alias': "Access Screening" },
        { 'mask': 0x00000002, 'alias': "Fraud Screening" },
        { 'mask': 0x00000004, 'alias': "JTLS" },
        { 'mask': 0x00000008, 'alias': "Don't Overwrite Charge Indication" },
        { 'mask': 0x00000010, 'alias': "Get TCI From Ingress Trunk Group" },
        { 'mask': 0x00000020, 'alias': "Use Called Prefix For Ingress Trunk Group TCI Lookup" },
        { 'mask': 0x00000040, 'alias': "Use Called Prefix For Egress Trunk Group TCI Lookup" },
        { 'mask': 0x00000080, 'alias': "Always Send CIT Information" },
        { 'mask': 0x00000100, 'alias': "IP Peer Supported" },
        { 'mask': 0x00000200, 'alias': "Non-zero Video Bandwidth Based Routing for SIP" },
        { 'mask': 0x00000400, 'alias': "HD Preferred Routing" },
        { 'mask': 0x00000800, 'alias': "HD Supported Routing" },
        { 'mask': 0x00001000, 'alias': "Use Sac NonSac Call Types For ZZ Profile" },
        { 'mask': 0x00002000, 'alias': "Use IP Trunk Group Routing" },
        { 'mask': 0x00004000, 'alias': "Japan Blocking Service" },
        { 'mask': 0x00008000, 'alias': "Connect To Service Node" },
        { 'mask': 0x00010000, 'alias': "Discard NPDI" },
        { 'mask': 0x00020000, 'alias': "Discard RN" },
        { 'mask': 0x00040000, 'alias': "PSP Destination Override" },
        { 'mask': 0x00080000, 'alias': "Out Of Service" },
        { 'mask': 0x00100000, 'alias': "Do Not Use As Fallback Bearer Capability Route" },
        { 'mask': 0x00200000, 'alias': "Allow Hex Digits In Cdpn" },
        { 'mask': 0x00400000, 'alias': "TNS Circuit Code Routing" },
        { 'mask': 0x00800000, 'alias': "Non-Zero Video Bandwidth Based Routing for H.323" },
        { 'mask': 0x02000000, 'alias': "VPN CLI Based BG Determination" },
        { 'mask': 0x04000000, 'alias': "Satellite Trunk" },
        { 'mask': 0x10000000, 'alias': "Enable JIP Interwork" },
        { 'mask': 0x20000000, 'alias': "Overlap Dialing" },
        { 'mask': 0x40000000, 'alias': "Disable Crankback" },
        { 'mask': 0x01000000, 'alias': "Use Preferred Identity" },
        { 'mask': 0x08000000, 'alias': "Send STI Verified Display Name" }        
    ]

    tdmTrunkType = [
        { 'value': 0, 'alias': "Other" },
        { 'value': 1, 'alias': "IMT" },
        { 'value': 2, 'alias': "FGD" },
        { 'value': 3, 'alias': "DAL" },
        { 'value': 4, 'alias': "FGB" },
        { 'value': 5, 'alias': "CNAT" },
        { 'value': 6, 'alias': "CAMA" },
        { 'value': 7, 'alias': "2A" },
        { 'value': 8, 'alias': "NTT" },
        { 'value': 9, 'alias': "GC" },
        { 'value': 10, 'alias': "IMT1" },
        { 'value': 11, 'alias': "LS" },
        { 'value': 12, 'alias': "LS3" },
        { 'value': 13, 'alias': "CT" },
        { 'value': 14, 'alias': "INT1" },
        { 'value': 15, 'alias': "INT2" },
        { 'value': 16, 'alias': "INT3" },
        { 'value': 17, 'alias': "INT4" },
        { 'value': 18, 'alias': "INT5" },
        { 'value': 19, 'alias': "INT7" },
        { 'value': 20, 'alias': "SSP" },
        { 'value': 21, 'alias': "INTS" },
        { 'value': 22, 'alias': "TST" },
        { 'value': 23, 'alias': "SIP" },
        { 'value': 24, 'alias': "IMT2" },
        { 'value': 25, 'alias': "IMT3" }        
    ]

    chargeIndicator = [
        { 'value': 0, 'alias': "None" },
        { 'value': 1, 'alias': "No Charge" },
        { 'value': 2, 'alias': "Charge" },
        { 'value': 3, 'alias': "No Indication" }        
    ]

    cpnPresentation = [
        { 'value': 0, 'alias': "None" },
        { 'value': 1, 'alias': "Allowed" },
        { 'value': 2, 'alias': "Restricted" },
        { 'value': 3, 'alias': "Number Unavailable" },
        { 'value': 4, 'alias': "Spare" }        
    ]

    cpnScreening = [
        { 'value': 0, 'alias': "None" },
        { 'value': 1, 'alias': "User Provided Not Screened" },
        { 'value': 2, 'alias': "User Provided Passed Network Screening" },
        { 'value': 3, 'alias': "User Provided Failed Network Screening" },
        { 'value': 4, 'alias': "Network Provided Not Screened" }
    ]

    dfltCpnPresentation = [
        { 'value': 0, 'alias': "None" },
        { 'value': 1, 'alias': "Allowed" },
        { 'value': 2, 'alias': "Restricted" },
        { 'value': 3, 'alias': "Number Unavailable" },
        { 'value': 4, 'alias': "Spare" }        
    ]

    localizationType = [
        { 'value': 0, 'alias': "Unknown" },
        { 'value': 1, 'alias': "North America" },
        { 'value': 2, 'alias': "Japan" },
        { 'value': 3, 'alias': "Generic" }
    ]

    egressChargeInd = [
        { 'value': 0, 'alias': "None" },
        { 'value': 1, 'alias': "No Charge" },
        { 'value': 2, 'alias': "Charge" },
        { 'value': 3, 'alias': "No Indication" }
    ]

    isEscaped = [
        { 'value': 0, 'alias': "False" },
        { 'value': 1, 'alias': "True" }
    ]

    defaultCPC = [
        { 'value': 0, 'alias': "Unknown" },
        { 'value': 1, 'alias': "Operator French" },
        { 'value': 2, 'alias': "Operator English" },
        { 'value': 3, 'alias': "Operator German" },
        { 'value': 4, 'alias': "Operator Russian" },
        { 'value': 5, 'alias': "Operator Spanish" },
        { 'value': 6, 'alias': "Operator Available 1" },
        { 'value': 7, 'alias': "Operator Available 2" },
        { 'value': 8, 'alias': "Operator Available 3" },
        { 'value': 9, 'alias': "Reserved" },
        { 'value': 10, 'alias': "Ordinary Subscriber" },
        { 'value': 11, 'alias': "Priority Subscriber" },
        { 'value': 12, 'alias': "Data Call" },
        { 'value': 13, 'alias': "Test Call" },
        { 'value': 14, 'alias': "Spare" },
        { 'value': 15, 'alias': "Payphone" },
        { 'value': 16, 'alias': "Transit Call Via Voice Service" },
        { 'value': 17, 'alias': "Interception Operator" },
        { 'value': 18, 'alias': "Private Metering" },
        { 'value': 19, 'alias': "PBX" },
        { 'value': 20, 'alias': "Singapore PBX With Priority" },
        { 'value': 21, 'alias': "City Wide Centrex Subscriber" },
        { 'value': 22, 'alias': "Basic Business Group Subscriber" },
        { 'value': 23, 'alias': "PABX With Private Metering" },
        { 'value': 24, 'alias': "Hotel Subscriber" },
        { 'value': 25, 'alias': "Hotel Subscriber With PRI Metering" },
        { 'value': 26, 'alias': "Denmark 249" },
        { 'value': 27, 'alias': "Denmark 254" },
        { 'value': 28, 'alias': "UK Ord Sub Residential" },
        { 'value': 29, 'alias': "UK Ord Sub Business" },
        { 'value': 30, 'alias': "UK Admin Div Ord" },
        { 'value': 31, 'alias': "UK Admin Div Payphone" },
        { 'value': 32, 'alias': "UK ISDN Residential" },
        { 'value': 33, 'alias': "UK ISDN Business" },
        { 'value': 34, 'alias': "UK Payphone Public" },
        { 'value': 35, 'alias': "UK Payphone Rent Residential" },
        { 'value': 36, 'alias': "UK Payphone Rent Business" },
        { 'value': 37, 'alias': "UK Service Line" },
        { 'value': 38, 'alias': "UK Centrex" },
        { 'value': 39, 'alias': "UK OSS Operator" },
        { 'value': 40, 'alias': "UK AMC OP NND IND" },
        { 'value': 41, 'alias': "UK AMC OP NND" },
        { 'value': 42, 'alias': "UK DPNSS" },
        { 'value': 43, 'alias': "UK RSVD ITU LANG1" },
        { 'value': 44, 'alias': "UK RSVD ITU LANG2" },
        { 'value': 45, 'alias': "UK RSVD ITU LANG3" },
        { 'value': 46, 'alias': "Special Line 2" },
        { 'value': 47, 'alias': "Slow Ordinary" },
        { 'value': 48, 'alias': "Teletasa" },
        { 'value': 49, 'alias': "Mobile" },
        { 'value': 50, 'alias': "Virtual Private Network" },
        { 'value': 51, 'alias': "Special Line 1" },
        { 'value': 52, 'alias': "National Operator With Intervention Capability" },
        { 'value': 53, 'alias': "Immediate Charge Rate Service" },
        { 'value': 54, 'alias': "National Operator With Trunk Offer" },
        { 'value': 55, 'alias': "National Operator Without Trunk Offer" },
        { 'value': 56, 'alias': "CCB Subscriber" },
        { 'value': 57, 'alias': "Subscriber With Home Meter" },
        { 'value': 58, 'alias': "Line Test Desk" },
        { 'value': 59, 'alias': "Non-International National Operator" },
        { 'value': 60, 'alias': "Prepaid Phone" },
        { 'value': 61, 'alias': "Phone Box" },
        { 'value': 62, 'alias': "Semi Public Phone" },
        { 'value': 63, 'alias': "Subscriber Special Charge" },
        { 'value': 64, 'alias': "National Public Telephone" },
        { 'value': 65, 'alias': "ORD Free LS LS" },
        { 'value': 66, 'alias': "ORD Periodic LS LS" },
        { 'value': 67, 'alias': "ORD Metering LS LS" },
        { 'value': 68, 'alias': "ORD Printer LS LS" },
        { 'value': 69, 'alias': "Priority Free LS TS" },
        { 'value': 70, 'alias': "Priority Periodic LS TS" },
        { 'value': 71, 'alias': "ORD Subscriber LS LS" },
        { 'value': 72, 'alias': "Message In Mailbox" },
        { 'value': 73, 'alias': "Cancel Message" },
        { 'value': 74, 'alias': "Coin Phone" },
        { 'value': 76, 'alias': "UK Admin Div Payphone Priority" },
        { 'value': 77, 'alias': "UK Admin Div Ord Priority" },
        { 'value': 81, 'alias': "Russia Subscriber Category0" },
        { 'value': 82, 'alias': "Russia Subscriber Category1" },
        { 'value': 83, 'alias': "Russia Subscriber Category2" },
        { 'value': 84, 'alias': "Russia Subscriber Category3" },
        { 'value': 85, 'alias': "Russia Subscriber Category4" },
        { 'value': 86, 'alias': "Russia Subscriber Category5" },
        { 'value': 87, 'alias': "Russia Subscriber Category6" },
        { 'value': 88, 'alias': "Russia Subscriber Category7" },
        { 'value': 89, 'alias': "Russia Subscriber Category8" },
        { 'value': 90, 'alias': "Russia Subscriber Category9" },
        { 'value': 91, 'alias': "Auto Call Category1" },
        { 'value': 92, 'alias': "Auto Call Category2" },
        { 'value': 93, 'alias': "Auto Call Category3" },
        { 'value': 94, 'alias': "Auto Call Category4" },
        { 'value': 95, 'alias': "Semi Auto Call Category1" },
        { 'value': 96, 'alias': "Semi Auto Call Category2" },
        { 'value': 97, 'alias': "Semi Auto Call Category3" },
        { 'value': 98, 'alias': "Semi Auto Call Category4" },
        { 'value': 223, 'alias': "CCITT Spare 2" },
        { 'value': 224, 'alias': "Emergency" },
        { 'value': 225, 'alias': "Priority Emergency" },
        { 'value': 226, 'alias': "NSEP Call (Government Emergency)" },
        { 'value': 227, 'alias': "ANSI Spare 1" },
        { 'value': 228, 'alias': "IEPS Call" },
        { 'value': 229, 'alias': "Cellular" },
        { 'value': 230, 'alias': "Cellular Roaming" },
        { 'value': 239, 'alias': "ANSI Spare 2" },
        { 'value': 240, 'alias': "Network Specific 1" },
        { 'value': 254, 'alias': "Network Specific 2" }
    ]

    maximumSatelliteHops = [
        { 'value': 0, 'alias': "No Satellite Hops" },
        { 'value': 1, 'alias': "One Satellite Hop" },
        { 'value': 2, 'alias': "Two Satellite Hops" },
        { 'value': 3, 'alias': "Three or More Satellite Hops" }
    ]

    remoteSipPeerType = [
        { 'value': 0, 'alias': "None" },
        { 'value': 1, 'alias': "Voice Mail Server" }       
    ]    


###############################################################################
#
#  Method:  getAlias
#
#  Utility method to return the alias for the corresponding value from the 
# list of enum dictionaries.
#
###############################################################################
def getAlias(enums, value):
    result = next((e for e in enums if e['value'] == int(value)), None)
    return result['alias'] if result != None else 'No alias found'

####################################################################
#
#  Method:  getBitSetting
#
#  Utility method to return ON or OFF based on bit setting
#
####################################################################
def getBitSetting(value, bitmask):
    theMask = bitmask & 0x0fffffff
    bitSetting = int(value) & theMask
    return 'ON' if bitSetting == theMask else 'OFF'

########################################################################
#
#  Method:  decodeAttribute
#
#  Decode the bitmask for the attribute by printing the mask and alias
#  for each bitmask that is enabled.
#
########################################################################
def decodeAttribute(value, attributes):
    intValue = int(value)
    for m in attributes:
        print('\t{: >60} (Mask={:08x}) : {}'.format(m['alias'], m['mask'], getBitSetting(value, m['mask'])))


########################################################################
#
#  Method:  decodeBothAttributes
#
#  Decode the bitmask for the attribute by printing the mask and alias
#  for each bitmask.
#
########################################################################
def decodeBothAttributes(ID1, value1, ID2, value2, attributes):

    ##  Some of the enumerations use the most significant nibble (MSB) for purposes
    ##  other than to define a setting. For example, there are 5 sets of featureList enumerations.
    ##  Each set uses the MSB as part of the mask to indicate which set.
    ##  Therefore, we mask off this nibble before using the mask. 

    for m in attributes:
        print('\t{: >60} (Mask={:08x}) : {}: {}, {}: {} '.format(m['alias'], m['mask'], \
                                        ID1, getBitSetting(value1, m['mask']),  \
                                        ID2, getBitSetting(value2, m['mask'])))


#############################################################
#
#  Method: printProfiles
#
#  Print all attributes in each profile.
#
#############################################################
def printProfiles(profObj1, profObj2):

    for p in [profObj1, profObj2]:
        theDict = p.theDict
        try:
            ##  Get the name of the profile

            idx = next((index for (index, theDict) in enumerate(theDict) if theDict["key"] == p.profileID), None)
            profID = theDict[idx]['value']

            print("\nProfile: {}\n".format(profID))

            ##  Print all attributes

            for i in range(len(theDict)):

                ##  Don't print keys we are ignoring.

                if theDict[i]['key'] in p.keysToIgnore:
                    continue
                
                ##  Print the attributes

                ##  If enumerations exist for this attribute... 
                try:
                    theEnums = eval('p.{}'.format(theDict[i]['key']))
                except:
                    theEnums = None

                if hasattr(p, theDict[i]['key']): 

                    ##  If enumerations have a mask, then decode the mask and print 

                    if theEnums is not None and 'mask' in theEnums[0]:                        
                        print("\n{: >3}. {}: {}".format(i, theDict[i]['key'], hex(int((theDict[i]['value'])))))
                        decodeAttribute(theDict[i]['value'], theEnums)

                    ## Else just print the values.

                    else:
                        alias = getAlias(theEnums, theDict[i]['value'])
                        print("\n{: >3}. {} Value: {}: Alias: {}".format(i, theDict[i]['key'], theDict[i]['value'], alias))

                ## No enumerations for the key, just print the value
                else:
                    print("\n{: >3}. {}: {}".format(i, theDict[i]['key'], theDict[i]['value']))

        except Exception as e:
            print("Error when printing profile {}, error={}".format(profID, e))

    return

#############################################################
#
#  Method: compareProfiles
#
#  Compare two profiles
#
#############################################################
def compareProfiles(profObj1, profObj2, bPrintDiffs):

    theDict1 = profObj1.theDict
    theDict2 = profObj2.theDict
    try:
        ##  Get the name of each profile

        idx = next((index for (index, d) in enumerate(theDict1) if d["key"] == profObj1.profileID), None)
        prof1ID = theDict1[idx]['value']
        idx = next((index for (index, d) in enumerate(theDict2) if d["key"] == profObj2.profileID), None)
        prof2ID = theDict2[idx]['value']

        ##  Now compare each attribute in the profile

        counter=1
        for i in range(len(theDict1)):

            ##  Don't compare keys we are ignoring.
            if theDict1[i]['key'] in profObj1.keysToIgnore:
                continue

            ##  If printing the differences in the attribute ....

            try:
                theEnums = eval('profObj1.{}'.format(theDict1[i]['key']))
            except:
                theEnums = None

            if bPrintDiffs == True:

                ##  Print values if different

                if theDict1[i]['value'] != theDict2[i]['value']:

                    ##  If enumerations exist for this attribute....

                    if hasattr(profObj1, theDict1[i]['key']):

                        ##  If enumerations have a mask, then decode the bit setting and print

                        if theEnums is not None and 'mask' in theEnums[0]:
                            print("\n{: >3}. {}: {}:{}, {}:{}".format(counter, theDict1[i]['key'], prof1ID, hex(int((theDict1[i]['value']))), prof2ID, hex(int((theDict2[i]['value'])))))
                            decodeBothAttributes(prof1ID, theDict1[i]['value'], prof2ID, theDict2[i]['value'], theEnums)

                        ## Else just print the values.  
                        else:
                            print("\n{: >3}. {}: {}:{}, {}:{}".format(counter, theDict1[i]['key'], \
                                    prof1ID, getAlias(theEnums, theDict1[i]['value']), prof2ID, getAlias(theEnums, theDict2[i]['value'])))
                    else:
                        print("\n{: >3}. {}: {}:{}, {}:{}".format(counter, theDict1[i]['key'], prof1ID, theDict1[i]['value'], prof2ID, theDict2[i]['value']))
                    counter += 1

            ##  Not printing the differences 
            else:
                diffStatus = 'Different' if theDict1[i]['value'] != theDict2[i]['value'] else ('Same' if theDict1[i]['value'] else 'No value')
                ##  If mask attribute, print as hex
                if theEnums is not None:
                    if 'mask' in theEnums[0]:
                        print("\n{: >3}. {: <30} {}:{}, {}:{} -----> {}".format(i, theDict1[i]['key'], prof1ID, hex(int(theDict1[i]['value'])), prof2ID, hex(int(theDict2[i]['value'])), diffStatus))

                    ##  Not a mask, print the value and alias
                    else:
                        print("\n{: >3}. {: <30} {}:{}, {}:{} -----> {}".format(i, theDict1[i]['key'], \
                                prof1ID, theDict1[i]['value'], prof2ID, theDict2[i]['value'], diffStatus))
                ##  No enums for the attribute, just print the key and value
                else:
                    print("\n{: >3}. {: <30} {}:{}, {}:{} -----> {}".format(i, theDict1[i]['key'], \
                            prof1ID, theDict1[i]['value'], prof2ID, theDict2[i]['value'], diffStatus))

    except Exception as e:
        print("Error when comparing profiles {} and {}, error={}".format(prof1ID, prof2ID, e))

    return

##########################################################################
#
#  Method: printList
#
#  Common method to print a list of retrieved entities.
#
##########################################################################
def printList(listEntities):

    if len(listEntities):
        theKeys = [*listEntities[0]]

        ##  Print the keys
        print("\n{}\n".format(", ".join(f"{key}" for key, value in listEntities[0].items())))

        ##  Now print all the entities
        for e in listEntities:
            print(", ".join(f"{value}" for key, value in e.items()))

    else:
        print('No entities to print')
    return


##########################################################################
#
#  Method: retrieveList
#
#  Common method to make SOAP request to retrieve the list of entities.
#
##########################################################################
def retrieveList(psxCfg, entityType):

    chunkID=0
    numItems=100
    listEntities = []

    types = {
        'dest':'DestinationKey',
        'erp':'ElementRoutingPriorityKey',
        'feat':'FeatureControlProfileKey',
        'ipsp':'IpSignalingProfileKey',
        'psp':'PacketServiceProfileKey',
        'rc':'RoutingCriteriaKey',
        'sig':'SignalingProfileKey',
        'tg':'TrunkgroupKey'
        }

    entityKey = types[entityType]

    ##  Build the SOAP request to retrieve the entity list

    headers = {"Content-Type": "text/xml;charset=UTF-8", 'SOAPAction': ''}
    url = "https://{}/emsapi/services/PSXAPI/{}".format(psxCfg.getIP(), psxCfg.getVersion())

    listRequest = """
    <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:intf="http://www.sonusnet.com/ems/emsapi/psx/{psxVersion}/intf">
       <soapenv:Header>
             <USER soapenv:mustUnderstand="0" xsi:type="xsd:string">{user}</USER><PASSWORD soapenv:mustUnderstand="0" xsi:type="xsd:string">{password}</PASSWORD>
       </soapenv:Header>
       <soapenv:Body>
          <intf:getNextItems soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
             <in0 xsi:type="xsd:string">{psxName}</in0>
             <in1 xsi:type="mod:{entityKey}" xmlns:mod="http://www.sonusnet.com/ems/emsapi/psx/{psxVersion}/model"></in1>
                 <numItems xsi:type="xsd:int">{numItems}</numItems>
                 <chunkId xsi:type="xsd:int">{chunkID}</chunkId>
          </intf:getNextItems>
       </soapenv:Body>
    </soapenv:Envelope>
    """

    ##  Retrieve all the entities
    print('Retrieving chunkID ..', end='')
    while 1 == 1:
        theRequest = listRequest.format(user=psxCfg.getUser(), password=psxCfg.getPassword(), psxName=psxCfg.getName(), entityKey=entityKey, psxVersion=psxCfg.getVersion(), numItems=numItems, chunkID=chunkID)
        try:
            response = requests.post(url, data=theRequest, headers=headers, verify=False)

            if 'soapenv:Fault' in xmltodict.parse(response.text)['soapenv:Envelope']['soapenv:Body']:
                soapEnvBody = xmltodict.parse(response.text)['soapenv:Envelope']['soapenv:Body']
                soapEnvFault = soapEnvBody['soapenv:Fault']

                ##  Check for various errors (Authentication failure, invalid PSX name)

                if 'faultstring' in soapEnvFault:
                    if soapEnvFault['faultstring'] is not None:
                        print('Error retrieving {} entities: Error: EMS {},  message {}'.format(entityType, soapEnvFault['faultstring'], soapEnvFault['detail']['ns1:hostname']['#text']))
                    elif 'multiRef' in soapEnvBody:
                        if 'errorCode' in soapEnvBody['multiRef']:
                            print("Could not find profile {}".format(profObj.profileName))
                        else:
                            print("Could not find PSX {}".format(soapEnvBody['multiRef']['message']['#text']))
                elif 'multiRef' in soapEnvFault:
                    print('Error retrieving {} entities: Error: {}'.format(entityType, soapEnvFault['multiRef']['rootCause']['#text']))

            else:
                json = xmltodict.parse(response.text, xml_attribs=False)['soapenv:Envelope']['soapenv:Body']['multiRef']
                if 'errorCode' in json:
                    print("Could not retrieve the {} entities, error={}, msg={}".format(entityType, json['errorCode']['#text'], json['message']['#text']))
                else:
                    ##  Append all the retrieved entities to the list
                    for i in range(len(json)):
                        listEntities.append(json[i])
                    ##  If retrieved max number of entities, get next chunk
                    if len(json) == numItems:
                        chunkID += 1
                        print('{}..'.format(chunkID),end='',flush=True)
                    ##  Retrieved fewer than max, all done
                    else:
                        break

        except Exception as e:
            print("Request failed, message: {}".format(str(e)))

    return listEntities

##########################################################################
#
#  Method: retrieveProfile
#
#  Common method to make SOAP request to retrieve the profile and return
#  the list of attributes.
#
##########################################################################
def retrieveProfile(psxCfg, profObj):

    listAttributes = None

    ##  Build the SOAP request to retrieve the profile

    headers = {"Content-Type": "text/xml;charset=UTF-8", 'SOAPAction': ''}
    url = "https://{}/emsapi/services/PSXAPI/{}".format(psxCfg.getIP(), psxCfg.getVersion())

    try:
        response = requests.post(url, data=profObj.genRequest(psxCfg), headers=headers, verify=False)

        if 'soapenv:Fault' in xmltodict.parse(response.text)['soapenv:Envelope']['soapenv:Body']:
            soapEnvBody = xmltodict.parse(response.text)['soapenv:Envelope']['soapenv:Body']
            soapEnvFault = soapEnvBody['soapenv:Fault']

            ##  Check for various errors (Authentication failure, invalid PSX name)

            if 'faultstring' in soapEnvFault:
                if soapEnvFault['faultstring'] is not None:
                    print('Error retrieving profile {}: Error: EMS {},  message {}'.format(profObj.profileName, soapEnvFault['faultstring'], soapEnvFault['detail']['ns1:hostname']['#text']))
                elif 'multiRef' in soapEnvBody:
                    if 'errorCode' in soapEnvBody['multiRef']:
                        print("Could not find profile {}".format(profObj.profileName))
                    else:
                        print("Could not find PSX {}".format(soapEnvBody['multiRef']['message']['#text']))
            elif 'multiRef' in soapEnvFault:
                print('Error retrieving profile {}: Error: {}'.format(profObj.profileName, soapEnvFault['multiRef']['rootCause']['#text']))

        else:
            json = xmltodict.parse(response.text)['soapenv:Envelope']['soapenv:Body']['multiRef']
            if 'errorCode' in json:
                print("Could not find the profile {}, error={}, msg={}".format(profObj.profileName, json['errorCode']['#text'], json['message']['#text']))
            else:
                listAttributes = copy.deepcopy(profObj.profAttributes)
                for i in range(len(listAttributes)):
                    try:
                        json[listAttributes[i]['key']]
                        listAttributes[i]['value'] = 'None' if '#text' not in json[listAttributes[i]['key']] else json[listAttributes[i]['key']]['#text']
                    except:
                        print("{}:  Response does not contain the key {}".format(profObj.profileName, listAttributes[i]['key']))
                        continue
    except Exception as e:
        print("Request failed, message: {}".format(str(e)))

    return listAttributes


########################################################
#
#  Method: main
#
########################################################
def main():

    sbcEps = []
    status = 0

    pgmName = Path(sys.argv[0]).stem

    #  Setup to parse the command line arguments

    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter, description='Compare two PSX profiles',
        epilog="\
            Example to compare 2 IPSP profiles AUTO_CARRIER_IPSP and AUTO_CARRIER_IPSP_302: \n \
                ./PSXprofileTool.py -u psx-cli-user -p psx-cli-password -psx SCLC004PSX03 -a cmp -t ipsp -ip 10.7.17.101 -profs AUTO_CARRIER_IPSP,AUTO_CARRIER_IPSP_302 \n\
            Example to compare 2 signaling profiles AUTO_SIG_PROF_PROXY and FIVN_SIG_PROF_PROXY: \n \
                ./PSXprofileTool.py -u psx-cli-user -p psx-cli-password -psx SCLC004PSX03 -a cmp -t sig -ip 10.7.17.101 -profs AUTO_SIG_PROF_PROXY,FIVN_SIG_PROF_PROXY \n\
            Example to print 2 signaling profiles AUTO_SIG_PROF_PROXY and FIVN_SIG_PROF_PROXY: \n \
                ./PSXprofileTool.py -u psx-cli-user -p psx-cli-password -psx SCLC004PSX03 -a cmp -t sig -ip 10.7.17.101 -prt -profs AUTO_SIG_PROF_PROXY,FIVN_SIG_PROF_PROXY \n\
            Example to compare 2 trunk group profiles AUTO_SIG_PROF_PROXY and FIVN_SIG_PROF_PROXY: \n \
                ./PSXprofileTool.py -u psx-cli-user -p psx-cli-password -psx SCLC004PSX03 -a cmp -tgs SCLC007SBC11:AGENT_ACCESS,SCLC007SBC11:VERINT_CORE -t tg -ip 10.7.17.101  \n \
            Example to list all the trunk groups: \n \
                ./PSXprofileTool.py -u psx-cli-user -p psx-cli-password -psx SCLC004PSX03 -t tg -ip 10.7.17.101 -a list  \n ")
                
    
    required= parser.add_argument_group(title='Required arguments')
    optional= parser.add_argument_group(title='Optional arguments')

    optional.add_argument('-d',  action='store_true', default=False, dest='bDebug', help='Enable debug logging (optional)')
    optional.add_argument('-diffs',  action='store_true', default=False, dest='bPrintJustDiffs', help='Only print the differences (optional)')
    optional.add_argument('-prt',  action='store_true', dest='bPrtProfiles', help='Print the profiles. Overrides comparing two profiles.')

    optional.add_argument('-profs',  action='store', dest='profiles', required=False, help='The 2 profiles, comma separated, to be compared')
    optional.add_argument('-tgs',  action='store', dest='tgs', required=False, help='The 2 TGs (GW:TG), comma separated, for comparing TGs')
    optional.add_argument('-gws',  action='store', dest='gateways', required=False, help="The 2 gateways, comma separated, for comparing TGs")

    required.add_argument('-a',  action='store', dest='action', required=True, choices=['list', 'cmp'], help='Action to perform')
    required.add_argument('-u',  action='store', dest='psxUser', required=True, help='PSX CLI user')
    required.add_argument('-p',  action='store', dest='psxPassword', required=True, help='PSX CLI password')
    required.add_argument('-v',  action='store', dest='psxVersion', required=False, default='r14_01_00', help='PSX version')
    required.add_argument('-psx',  action='store', dest='psx', required=True, help='EMS server name')
    required.add_argument('-ip',  action='store', dest='ip', required=True, help='EMS IP')

    required.add_argument('-t',  action='store', dest='profType', required=True, choices=['dest', 'erp', 'feat', 'ipsp', 'psp', 'rc', 'sig', 'tg'], help='Profile type to retrieve)')

    #  Print help if no arguments

    if len(sys.argv)==1:
        parser.print_help(sys.stderr)
        sys.exit(1)

    #  Parse the arguments

    args = parser.parse_args()  

    if args.bDebug == True:
        pdb.set_trace()

    ##  Create the PSX configuration object
    
    psxCfg = PsxConfig(args.psxUser, args.psxPassword, args.psx, args.psxVersion, args.ip)

    ##  If listing an entity....
    if args.action == 'list':
        listEntities = retrieveList(psxCfg, args.profType)
        printList(listEntities)
        sys.exit(0)

    ##  If comparing TG profiles ....
    ##     Retrieving the TG profile requires the gateway and the TG profile name

    if args.profType == 'tg':
        if args.tgs is None:
            print("Need to specify 'tg' for Trunk Group comparison")
            sys.exit(-1)

        ## Verify 2 gateways and trunks are provided
        
        if len(args.tgs.split(',')) != 2:
            print("Need to specify 2 TGs (-tgs), comma separated. Format is gw1:tg1,gw2:tg2.")
            sys.exit(-1)

        ##  Get the gateways and TGs.  Format of the TG argument is GW1:TG1,GW2:TG2

        if len(args.tgs.split(',')[0].split(':')) != 2 or len(args.tgs.split(',')[1].split(':')) != 2:
            print("Incomplete TGs. Format is gw1:tg1,gw2:tg2.")
            sys.exit(-1)

        [gw1,tg1] = args.tgs.split(',')[0].split(':')
        [gw2,tg2] = args.tgs.split(',')[1].split(':')

        ## Verify both gateways and TGs are not empty

        if len(tg1) == 0  or len(tg2) == 0:
            print("Need to specify both TGs (-tgs). Format is gw1:tg1,gw2:tg2.")
            sys.exit(-1)

        ##  Create the TG objects

        profObj1 = TrunkGroup(tg1, gw1)
        profObj2 = TrunkGroup(tg2, gw2)

    #  Else if comparing other profiles ...

    elif args.profType in ['erp', 'feat', 'ipsp', 'psp', 'rc', 'sig']:
        if args.profiles is None:
            print("Need to specify 2 profiles (-profs) to compare profiles")
            sys.exit(-1)
        else:
            if len(args.profiles.split(',')) != 2:
                print("Need to specify 2 profiles (-profs), comma separated")
                sys.exit(-1)

        [profile1, profile2] = args.profiles.split(',')
        if len(profile1) == 0  or len(profile2) == 0:
            print("Need to specify both profiles ")
            sys.exit(-1)

        ##  Map between the profile type and its class name

        profileClasses = {
            "erp": "ElementRoutingPriority",
            "feat": "FeatureControl",
            "ipsp": "IPSPsignaling",
            "psp": "PSP",
            "rc": "RoutingCriteria",
            "sig": "Signaling"
        }

        ##  Get the profile class name

        profClassName = profileClasses.get(args.profType) 

        ##  Create the object for the 2 profiles to be compared

        profObj1 = eval(profClassName)(profile1)
        profObj2 = eval(profClassName)(profile2)

    ##  Retrieve the profiles

    profObj1.theDict = retrieveProfile(psxCfg, profObj1)
    if profObj1.theDict != None:
        profObj2.theDict = retrieveProfile(psxCfg, profObj2)
        if profObj2.theDict != None:

            ##  Both profiles retrieved.

            ##  If printing the profiles ...
            if args.bPrtProfiles:
                printProfiles(profObj1, profObj2)

            ##  Compare the profiles
            else:
                compareProfiles(profObj1, profObj2, args.bPrintJustDiffs)
        else:
            status = -1
    else:
        status = -1

    sys.exit(status)

if __name__ == '__main__':
    main()

